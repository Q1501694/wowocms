<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model\ViewModel;
//视图模型
class MemberViewModel extends ViewModel {
    
    protected $viewFields = array(
        'member' => array('id', 'email', 'nickname', 'amount', 'score', 'face', 'regtime', 'logintime', 'loginip', 'loginnum', 'groupid', 'status', 'islock',
        '_type' => 'LEFT'
        ),
       /* 'membergroup' => array(
        'name' => 'groupname',
        '_on' => 'member.groupid = membergroup.id',//_on 对应上面LEFT关联条件
        //'_type' => 'LEFT'
        ),*/
        'memberdetail'=>array(
            'realname'=>'realname',
            'sex'=>'sex',
            'qq'=>'qq',
            'phone'=>'phone',
            '_on'=>'member.id = memberdetail.userid'
        ),
        /*
        'model' => array(
        'tablename' => 'tablename',//显示字段name as model
        '_on' => 'category.modelid = model.id',//_on 对应上面LEFT关联条件
        ),
        */

    );
}

?>