<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model\ViewModel;
class RuleViewModel extends ViewModel{
    public $viewFields=array(       
        'rule'=>array('_table'=>'__AUTH_RULE__','id','name','title','type','condition'=>'term','status','mid','sort'),
        //condition必须别名,否则出错
        'modules'=>array('_table'=>'__AUTH_MODULES__','moduleName','sort'=>'s','_on'=>'rule.mid=modules.id')
        );
}
 ?>