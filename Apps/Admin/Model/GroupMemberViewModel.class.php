<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model\ViewModel;
class GroupMemberViewModel extends ViewModel{
    public $viewFields=array(       
        'member'=>array('_table'=>'__MEMBERS__','Id'=>'mid','username'),        
        'groups'=>array('_table'=>'__AUTH_GROUP_ACCESS__','uid','group_id','_on'=>'member.Id=groups.uid'),
        'groupname'=>array('_table'=>'__AUTH_GROUP__','title','_on'=>'group_id=groupname.id')
        );
}
 ?>