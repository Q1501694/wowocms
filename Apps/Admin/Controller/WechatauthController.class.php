<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;

use Think\Controller;
use Com\WechatAuth;

class WechatauthController extends CommonController{

    public function index(){
        $count=M('chatmenu')->count();
        $page = new \Think\Page($count,10);
        $limit = $page->firstRow. ',' .$page->listRows;
        $list = M('chatmenu')->order('sort desc')->limit($limit)->select();
        $Category = new \Think\Category();
        $list=$Category::unlimitedForLevel($list, '&nbsp;&nbsp;&nbsp;&nbsp;', 0);
        $this->page = $page->show();


        $this->menus=$list;
        $this->type='接口配置';
        $this->display();       
    }
    //信息配置
    public function editHuiType(){       
        $data['cfg_wechat_appid']=I('cfg_wechat_appid');
        $data['cfg_wechat_appsecret']=I('cfg_wechat_appsecret');
        if (IS_POST) {
            if (writeArr($data,APP_PATH.'Home/Conf/'.'config.wechatauth.php') && writeArr($data,APP_PATH.'Admin/Conf/'.'config.wechatauth.php')) {
                yes('保存成功！');
            }else{
                error('保存失败！');
            }
        }else{
            exit;
        }       
    }
    //add menu to db
    public function addMenu(){

        $action=I('action');
        $pid=I('pid');
        $id=I('id');

        if (IS_POST) {
            if ($action=='edit') {
                $this->editMenuHandler();
            }else if($action=='addchild'){
                $this->addChildHandler();
            }else{
                $this->addMenuHandler();
            }
        }

        if ($action=='edit') {
            $db=M('chatmenu');
            $this->menu=$db->where(array('id'=>$id))->find();
            $this->menus=$db->where(array('pid'=>'0'))->select();
            $action='edit';
        }
        if ($action=='addchlid') {
            $action='addchlid';
        }
        if ($action=='add') {
            $action='add';
        }
        
        $this->action=$action;
        $this->pid=$pid;
        $this->display('addmenu');

    }
    //
    public function editMenuHandler(){
        $validate = array(
            array('title','require','名称必须填写！'), 
            array('url','require','链接必须填写！'), 
        );
        $db = M('chatmenu');
        if (!$db->validate($validate)->create()) {
            error($db->getError());
        }
        $data=array(
                'title'=>I('title'),
                'url'=>I('url'),
                'sort'=>I('sort'),
                'pid'=>I('pid'),
                'type'=>I('post.type',0,'intval'),
            );
        if (M('chatmenu')->where(array('id'=>I('id')))->save($data)) {
            yes('修改成功！',true,'Wechatauth-index');
        }else{
            error('修改失败！');
        }

    }
    //
    public function addChildHandler(){
        $pid=I('post.pid',0,'intval');
        $isone=M('chatmenu')->where(array('id'=>$pid))->getField('pid');
        if ($isone!=0) {
            error('只能一级菜单添加二级菜单');
        }
        $count=M('chatmenu')->where(array('pid'=>I('pid')))->count();
        if ($count>=5) {
            error('二级栏目最多包含5个！');
        }

        $validate = array(
            array('title','require','名称必须填写！'), 
            array('url','require','链接必须填写！'), 
        );
        $db = M('chatmenu');
        if (!$db->validate($validate)->create()) {
            error($db->getError());
        }
        
        $data = array(
            'title' => I('title'), 
            'url' => I('url'),
            'sort' => I('sort'),
            'pid' => I('pid'),
            'type'=>I('post.type',0,'intval'),
        );
        if ($data['type']==1|| $data['type']==2) {
            $data['key']=build_order_no();
        }

        if ($db->add($data)) {
            yes('添加成功！',true,'Wechatauth-index');
        }else{
            error('添加失败！');
        }
    }

    public function addMenuHandler(){
        $count=M('chatmenu')->where(array('pid'=>0))->count();
        if ($count>=3) {
            error('一级栏目最多3个！');
        }
        $validate = array(
            array('title','require','名称必须填写！'), 
            array('url','require','链接必须填写！'), 
        );
        $db = M('chatmenu');
        if (!$db->validate($validate)->create()) {
            error($db->getError());
        }
        $data = array(
            'title' => I('title'), 
            'url' => I('url'),
            'sort' => I('sort'),
            'pid' => I('pid'),
            'type'=>I('post.type',0,'intval'),
        );
        if ($data['type']==1|| $data['type']==2) {
            $data['key']=build_order_no();
        }
        if ($db->add($data)) {
            yes('添加成功！',true,'Wechatauth-index');
        }else{
            error('添加失败！');
        }
    }

    public function del(){
        $id=I('id');
        if (M('chatmenu')->where(array('id'=>$id))->delete()) {
            yes('删除成功！');
        }else{
            error('删除失败！');
        }
    }

    public function reloadMenu(){
        header("Content-Type:text/html; charset=utf-8");
        $appid=C('cfg_wechat_appid');
        $appsecret=C('cfg_wechat_appsecret');
        $wechatauth = new WechatAuth($appid,$appsecret);
        $menus=M('chatmenu')->field('id,pid,title as name,url,type,key')->select();
        $newmenu=getMenus($menus);
        $newmenu=unserialize($newmenu);
        $access_token=$wechatauth->getAccessToken();
        $result=$wechatauth->menuCreate($newmenu);
        if ($result['errmsg'] == 'ok') {
            yes('成功发布到微信！');
        }else{
            error('发布失败！');
        }
    }
}