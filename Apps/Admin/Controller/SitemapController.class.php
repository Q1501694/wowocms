<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class SitemapController extends CommonController {

    public function index(){
        $this->type="网站XML——可提交到百度。谷歌。360。SiteMap";
        $where['tablename']=array('neq','page');
        $this->models=M('model')->field('name,tablename')->where($where)->select();
        $this->display();
    }
    public function makeMap(){
           header("Content-Type: text/html; charset=utf-8");
           $md=I('post.md');//所属于模型
           if (empty($md)) {
               $md='article';
           }
            $data_array=M($md)->field('id,cid,title,publishtime,jumpurl,flag')->limit(80)->order('publishtime DESC')->select();
            //print_r($data_array);
            $title_size = 1;
            $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset
    xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
    xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9
       http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n";
            foreach ($data_array as $data) {
              $_jumpflag = ($data['flag'] & B_JUMP) == B_JUMP? true : false;
              $cate=getCateNameAndEnameById($data['cid']);
              $data['ename']=$cate['ename'];
              $data['url']=getContentUrl($data['id'],$data['cid'],$data['ename'],$_jumpflag,$data['jumpurl']);
              switch (C('URL_MODEL')) {
                  case 0://普通模式 
                      $data['url']=str_replace(C('cfg_hou_tai'), C('DEFAULT_MODULE'), $data['url']);
                      break;
                  case 1://PATHINFO模式 
                      $data['url']=str_replace(C('cfg_hou_tai').'/', '', $data['url']);
                      break;
                  case 2://REWRITE模式(需要URL_REWRITE支持) 
                      $data['url']=str_replace(C('cfg_hou_tai').'/', '', $data['url']);
                      break;
                  case 3://兼容模式 
                      $data['url']=str_replace('/'.C('cfg_hou_tai'), '', $data['url']);
                      break;
              }
              $data['lastmod']=date('Y-m-d',$data['publishtime']);
              $xml .= $this->create_item(C('cfg_weburl').$data['url'], $data['lastmod']);
            }
            $xml .= "</urlset>\n";
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/'.$md.'_sitemap.xml','w+') or die("Unable to open file!");
            if(fwrite($file,$xml))
            {   
                yes('生成成功！');
            }
            fclose($fp);
    }

    function create_item($loc, $lastmod, $changefreq="daily", $priority="1.0")
    {
        $item = "<url>\n";
        $item .= "<loc>" . $loc . "</loc>\n";
        $item .= "<lastmod>" . $lastmod . "</lastmod>\n";
        $item .= "<changefreq>" . $changefreq . "</changefreq>\n";
        $item .= " <priority>" . $priority . "</priority>\n";
        $item .= "</url>\n";
        return $item;
    }
}