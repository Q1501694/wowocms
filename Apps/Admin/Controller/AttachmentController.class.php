<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;

use Think\Controller;

class AttachmentController extends CommonController
{

    //B-JUI上传组件专用  暂不入库附件表
    public function uploadPlus()
    {
        /*$uid = I('post.aid'); //上传标识
        if (empty($uid)) {
          $return['error'] = 1;
          $return['message'] = '没有登录不允许上传';
          exit(json_encode($return));
        }*/
        /* 返回标准数据 */
        $dir = I('get.dir'); //上传类型image、flash、media、file
        //上传配置
        $ext_arr = array(
            'image' => array('gif', 'jpg', 'jpeg', 'png', 'bmp'),
            'flash' => array('swf', 'flv'),
            'media' => array('swf', 'flv', 'mp3', 'wav', 'wma', 'wmv', 'mid', 'avi', 'mpg', 'asf', 'rm', 'rmvb'),
            'file' => array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'htm', 'html', 'txt', 'zip', 'rar', 'gz', 'bz2'),
        );
        $upload = new \Think\Upload(); 
        $upload->maxSize = 3145728; 
        $upload->exts = $ext_arr[$dir]; 
        $upload->savePath = $dir . '/';
        $info = $upload->upload();
        if ($info) {
                foreach ($info as &$file) {
                    $return['filename']=C('ROOTPATH') . $file['savepath'] . $file['savename'];
                    $return['statusCode'] = 200;
                }
        } else {
            $return['statusCode'] = 300;
            $return['message'] = $upload->getError();
        }
        exit(json_encode($return));
    }
    //产品发布页面删除图片
    public function delimg(){
        $img=I('post.imagename');
        @unlink(".".$img);
    }


    //编辑器浏览器设置
    public function filemanager()
    {
        //根目录路径，可以指定绝对路径，比如 /var/www/attached/
        $root_path = "./" . C('ROOTPATH');
        //根目录URL，可以指定绝对路径，比如 http://www.yoursite.com/attached/
        $root_url = C('ROOTPATH');
        //图片扩展名
        $ext_arr = array('gif', 'jpg', 'jpeg', 'png', 'bmp');
        //目录名
        $dir_name = empty($_GET['dir']) ? '' : trim($_GET['dir']);
        //不在上传目录退出
        if (!in_array($dir_name, array('', 'image', 'flash', 'media', 'file'))) {
            echo "Invalid Directory name.";
            exit;
        }
        if ($dir_name !== '') {
            $root_path .= $dir_name . "/";
            $root_url .= $dir_name . "/";
            if (!file_exists($root_path)) {
                mkdir($root_path);
            }
        }
        //根据path参数，设置各路径和URL
        if (empty($_GET['path'])) {
            $current_path = realpath($root_path) . '/';
            $current_url = $root_url;
            $current_dir_path = '';
            $moveup_dir_path = '';
        } else {
            $current_path = realpath($root_path) . '/' . $_GET['path'];
            $current_url = $root_url . $_GET['path'];
            $current_dir_path = $_GET['path'];
            $moveup_dir_path = preg_replace('/(.*?)[^\/]+\/$/', '$1', $current_dir_path);
        }
        //echo realpath($root_path);
        //不允许使用..移动到上一级目录
        if (preg_match('/\.\./', $current_path)) {
            echo 'Access is not allowed.';
            exit;
        }
        //最后一个字符不是/
        if (!preg_match('/\/$/', $current_path)) {
            echo 'Parameter is not valid.';
            exit;
        }
        //目录不存在或不是目录
        if (!file_exists($current_path) || !is_dir($current_path)) {
            echo 'Directory does not exist.';
            exit;
        }
        //遍历目录取得文件信息
        $file_list = array();
        if ($handle = opendir($current_path)) {
            $i = 0;
            while (false !== ($filename = readdir($handle))) {
                if ($filename{0} == '.')
                    continue;
                $file = $current_path . $filename;
                if (is_dir($file)) {
                    $file_list[$i]['is_dir'] = true; //是否文件夹
                    $file_list[$i]['has_file'] = (count(scandir($file)) > 2); //文件夹是否包含文件
                    $file_list[$i]['filesize'] = 0; //文件大小
                    $file_list[$i]['is_photo'] = false; //是否图片
                    $file_list[$i]['filetype'] = ''; //文件类别，用扩展名判断
                } else {
                    $file_list[$i]['is_dir'] = false;
                    $file_list[$i]['has_file'] = false;
                    $file_list[$i]['filesize'] = filesize($file);
                    $file_list[$i]['dir_path'] = '';
                    $file_ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    $file_list[$i]['is_photo'] = in_array($file_ext, $ext_arr);
                    $file_list[$i]['filetype'] = $file_ext;
                }
                $file_list[$i]['filename'] = $filename; //文件名，包含扩展名
                $file_list[$i]['datetime'] = date('Y-m-d H:i:s', filemtime($file)); //文件最后修改时间
                $i++;
            }
            closedir($handle);
        }
        //排序形式，name or size or type
        $order = empty($_GET['order']) ? 'name' : strtolower($_GET['order']);
        $sorts = array();
        foreach ($file_list as $row) {
            $sorts['size'][] = $row['filesize'];
            $sorts['type'][] = $row['filetype'];
            $sorts['name'][] = $row['filename'];
            //$sorts['datetime'][] = $row['datetime']; //时间排序
        }
        /*
        if($order=='datetime'){
           array_multisort($sorts['datetime'], SORT_ASC, $file_list);
        }
         *
         */
        if ($order == 'name') {
            array_multisort($sorts['name'], SORT_ASC, $file_list);
        }
        if ($order == 'size') {
            array_multisort($sorts['size'], SORT_DESC, $file_list);
        }
        if ($order == 'type') {
            array_multisort($sorts['type'], SORT_ASC, $file_list);
        }

        $result = array();
        //相对于根目录的上一级目录
        $result['moveup_dir_path'] = $moveup_dir_path;
        //相对于根目录的当前目录
        $result['current_dir_path'] = $current_dir_path;
        //当前目录的URL
        $result['current_url'] = $current_url;
        //文件数
        $result['total_count'] = count($file_list);
        //文件列表数组
        $result['file_list'] = $file_list;
        //输出JSON字符串
        header('Content-type: application/json; charset=UTF-8');
        exit(json_encode($result));
    }


    public function uploads()
    {
        $return = array('error' => 0);
        $dir = I('get.dir'); //上传类型image、flash、media、file
        //上传配置
        $ext_arr = array(
            'image' => array('gif', 'jpg', 'jpeg', 'png', 'bmp'),
            'flash' => array('swf', 'flv'),
            'media' => array('swf', 'flv', 'mp3', 'wav', 'wma', 'wmv', 'mid', 'avi', 'mpg', 'asf', 'rm', 'rmvb'),
            'file' => array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'htm', 'html', 'txt', 'zip', 'rar', 'gz', 'bz2'),
        );

        $upload = new \Think\Upload();
        // 实例化上传类
        $upload->maxSize = 3145728;
        // 设置附件上传大小
        //$upload -> exts = array('jpg', 'gif', 'png', 'jpeg');
        $upload->exts = $ext_arr[$dir]; // 设置附件上传类型
        // 设置附件上传类型
        //$upload -> rootPath = C('ROOTPATH');
        // 设置附件上传根目录
        $upload->savePath = $dir . '/';
        // 设置附件上传目录
        // 上传文件
        $info = $upload->upload();
        //这里判断是否上传成功
        if ($info) {
            //// 上传成功 获取上传文件信息
            $db = M('attachment');
            foreach ($info as &$file) {

                //入库附件表格
                $savepath = $file['savepath'];
                $data['filepath'] = ltrim(C('ROOTPATH'), ".") . $savepath . $file['savename'];
                $data['title'] = $file['name'];
                $data['haslitpic'] = empty($file['haslitpic']) ? 0 : 1;
                $filetype = 1;
                //后缀
                switch ($file['ext']) {
                    case 'gif' :
                        $filetype = 1;
                        break;
                    case 'jpg' :
                        $filetype = 1;
                        break;
                    case 'png' :
                        $filetype = 1;
                        break;
                    case 'bmp' :
                        $filetype = 1;
                        break;
                    case 'swf' :
                        //flash
                        $filetype = 2;
                        break;
                    case 'mp3' :
                        //音乐
                        $filetype = 3;
                        break;
                    case 'wav' :
                        $filetype = 3;
                        break;
                    case 'rm' :
                        //电影
                        $filetype = 4;
                        break;

                    case 'doc' :
                        //
                        $filetype = 5;
                        break;
                    case 'docx' :
                        //
                        $filetype = 5;
                        break;
                    case 'xls' :
                        //
                        $filetype = 5;
                        break;
                    case 'ppt' :
                        //
                        $filetype = 5;
                        break;
                    case 'zip' :
                        //
                        $filetype = 6;
                        break;
                    case 'rar' :
                        //
                        $filetype = 6;
                        break;
                    case '7z' :
                        //
                        $filetype = 6;
                        break;

                    default :
                        //其他
                        $filetype = 0;
                        break;
                }
                $data['filetype'] = $filetype;
                $data['filesize'] = $file['size'];
                $data['uploadtime'] = time();
                $data['aid'] = $_SESSION['aid'];
                //管理员ID
                $db->add($data);
                //入库附件表结束

                //拼接出上传目录
                // $file['rootpath'] =ltrim($setting['rootPath'], ".");
                //拼接出文件相对路径
                $file['filepath'] = C('ROOTPATH') . $file['savepath'] . $file['savename'];
                echo json_encode(array('error' => 0, 'url' => $file['filepath']));
            }
            exit();
        } else {
            //输出错误信息
            exit($this->uploader->getError());
        }
    }

}