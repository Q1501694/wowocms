<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;

use Think\Controller;

class ArticleController extends CommonController
{
    public function index()
    {
        $pid = I('get.pid', 0, 'intval');

        $db=I('model');
        if (empty($db)) {
            $db=I('get.model');
        }
        //print_r($db);
        $keyword = I('keyword', '', 'htmlspecialchars,trim');
        $Category = new \Think\Category();
        $cate = getCategory();
        $this->subcate = $Category::getChilds($cate, $pid);
        $this->poscate = $Category::getParents($cate, $pid);
        if ($pid) {
            $idarr = $Category::getChildsId($cate, $pid, 1);
            $where = array('status' => 0, 'cid' => array('in', $idarr));
        } else {
            //$where = array('status' => 0); 临时取消 因为添加自定义模型之后，只允许带pid过来
            error('参数错误！');
        }
        if (!empty($keyword)) {
            $where['title'] = array('LIKE', "%{$keyword}%");
        }
        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count = M($db)->where($where)->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $art = M($db)->where($where)->order('id DESC')->limit($limit)->select();
        $this->model=$db;
        $this->pid = $pid;
        $this->keyword = $keyword;
        $this->page = $page->show();
        $this->vlist = $art;
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->display();
    }

    //添加文章
    public function add()
    {
        $model_name=I('get.model');
        $model_name=strtolower($model_name);
        $this->model_name=$model_name;
        $this->pid = I('pid', 0, 'intval');
        $db=I('get.model');
        $db=strtolower($db);
        $model_id=M('model')->where(array('tablename'=>$db))->getField('id');
        $fields=M('model_field')->where(array('model_id'=>$model_id))->order('sort')->select();
        //p($fields);
        $rs_html=SendDataByCurl($fields);

        
        $cate = getCategory(2);
        $Category = new \Think\Category();
        $cate = $Category::unlimitedForLevel($cate);
        $this->flagtypelist = getArrayOfItem('flagtype');
        $this->cate = $Category::getLevelOfModel($cate, $model_name);
        $this->field_html=$rs_html;
        $this->display();
    }

    //
    public function addPost()
    {
        if (!IS_POST) {
            exit();
        }
        $pid = I('pid', 0, 'intval');
        $cid = I('cid', 0, 'intval');
        $title = I('title');
        $copyfrom = I('copyfrom');
        $author = I('author');
        $flags = I('flags', array(), 'intval');
        $jumpurl = I('jumpurl');
        $description = I('description');
        $content = I('content');
        $pic = I('litpic');
        $add_table=I('post.model_name');
        if (empty($add_table)) {
            error('参数错误！');
        }
        if (empty($title)) {
            error('标题不能为空');
        }
        if (!$cid) {
            error('请选择栏目');
        }
        $pid = $cid;
        
        //图片标志
        if (!empty($pic) && !in_array(B_PIC, $flags)) {
            $flags[] = B_PIC;
        }
        $flag = implode(',', $flags); //拼装属性
        //获取属于分类信息,得到modelid
        $Category = new \Think\Category();
        $selfCate = $Category::getSelf(getCategory(0), $cid);
        //当前栏目信息
        $modelid = $selfCate['modelid'];

        //userid暂时不做插入。因为这是后台操作
        $data = array(
            'title' => $title, 
            'copyfrom' => $copyfrom, 
            'author' => $author, 
            'shorttitle' => I('shorttitle'), 
            'color' => I('color'), 
            'cid' => $cid, 
            'litpic' => $pic, 
            'keywords' => I('keywords'), 
            'description' => $description, 
            'publishtime' => time(), 
            'updatetime' => time(), 
            'click' => rand(10, 95), 
            'status' => 0, 
            'commentflag' => I('commentflag', 0, 'intval'), 
            'flag' => $flag, 
            'jumpurl' => $jumpurl, 
            'aid' => $_SESSION['aid'],
            'addtable'=>"addon".$add_table
        );
        if ($aid = M($add_table)->add($data)) {
            //更新上传附件表
            if (!empty($pic)) {
                //上传缩略图操作的时候已经插入附件表
                $attid = M('attachment')->where(array('filepath' => $pic))->getField('id');
                if ($attid) {
                    M('attachmentindex')->add(array('attid' => $attid, 'arcid' => $aid, 'modelid' => $modelid));
                }
            }
            //加入附加表的数据
            $addon_data=array('aid'=>$aid);
            $post_data=I('post.');
            //剔除部分数据，只保留给附加表字段的数据
            foreach ($post_data as $key => $value) {
                foreach ($data as $k => $v) {
                    if ($key==$k) {
                        unset($post_data[$k]);
                    }
                }
                unset($post_data['pid']);
                unset($post_data['flags']);
                unset($post_data['model_name']);
            }

            //检查出是否是多图片上传字段
            $imgfiles=M('model_field')->where(array('model_id'=>$modelid,'type'=>'imgfile'))->field('field')->select();

            for ($i=0; $i <count($imgfiles) ; $i++) { 
                $pictureurls_arr=array();
                $key=$imgfiles[$i]['field'];
                if (array_key_exists($key,$post_data)) {
                    foreach ($post_data[$key] as $k => $v) {
                        $pictureurls_arr[] = $v . '$$$' . '$$$'; //array('url'=> $v ,'alt'=> '');
                            if ($k == 0) {
                                $pic = $v;
                            }
                    }
                    $post_data[$key] = join('|||', $pictureurls_arr);
                }
            }
            //检查出是否是多选字段
            $checkboxs=M('model_field')->where(array('model_id'=>$modelid,'type'=>'checkbox'))->field('field')->select();
            for ($i=0; $i <count($checkboxs) ; $i++) { 
                $checkboxs_arr=array();
                $key=$checkboxs[$i]['field'];
                if (array_key_exists($key,$post_data)) {
                    $checkboxs_arr=$post_data[$key];
                    $post_data[$key] = join(',', $checkboxs_arr);//因为存储checkbox数据是以SET格式存储
                }
            }
            //检查出是否是日期类型，转成时间戳进行储蓄
            //检查出是否是多选字段
            $datetimes=M('model_field')->where(array('model_id'=>$modelid,'type'=>'datetime'))->field('field')->select();
            for ($i=0; $i <count($datetimes) ; $i++) { 
                $tims="";
                $key=$datetimes[$i]['field'];
                if (array_key_exists($key,$post_data)) {
                    $datetimes_str=strtotime($post_data[$key]);//转成时间戳
                    $post_data[$key] = $datetimes_str;
                }
            }
            $addon_data=array_merge($addon_data,$post_data);
            M("addon".$add_table)->add($addon_data);

            delCacheHtml('List/index_' . $cid, false, 'list:index');
            delCacheHtml('Index_index', false, 'index:index');
            yes('添加成功！', true, 'article-index,sub-article-index');
        } else {
            error('添加文章失败');
        }
    }

    //编辑文章
    public function edit()
    {
        if (IS_POST) {
            $this->editPost();
            exit();
        }
        //当前控制器名称
        $id = I('id', 0, 'intval');
        $model_name=I('get.model');
        $model_name=strtolower($model_name);

        $addon_table="addon".$model_name;
        $data=M($addon_table)->where(array('aid'=>$id))->find();
        unset($data['aid']);
        $model_id=M('model')->where(array('tablename'=>$model_name))->getField('id');
        $fields=M('model_field')->where(array('model_id'=>$model_id))->field('default_value,itemname,field,type')->order('sort')->select();
        $rs_html=SendDataByCurls($fields,$data);
        $this->pid = I('pid', 0, 'intval');
        
        $cate = getCategory(2);
        $Category = new \Think\Category();
        $cate = $Category::unlimitedForLevel($cate);
        $this->cate = $Category::getLevelOfModel($cate, $model_name);
        $vo = M($model_name)->find($id);
        $vo['flag'] = explode(',', $vo['flag']);
        $this->field_html=$rs_html;
        $this->vo = $vo;
        $this->model_name=$model_name;
        $this->flagtypelist = getArrayOfItem('flagtype');
        $this->display();
    }

    public function editPost(){
        $pid = I('pid', 0, 'intval');
        $cid = I('cid', 0, 'intval');
        $title = I('title');
        $copyfrom = I('copyfrom');
        $author = I('author');
        $flags = I('flags', array(), 'intval');
        $jumpurl = I('jumpurl');
        $description = I('description');
        $content = I('content');
        $pic = I('litpic');
        $add_table=I('post.model_name');
        $aid =I('post.id',0,'intval');
        if (empty($add_table)||$aid==0) {
            error('参数错误！');
        }
        if (empty($title)) {
            error('标题不能为空');
        }
        if (!$cid) {
            error('请选择栏目');
        }
        $pid = $cid;
        
        //图片标志
        if (!empty($pic) && !in_array(B_PIC, $flags)) {
            $flags[] = B_PIC;
        }
        $flag = implode(',', $flags); //拼装属性
        //获取属于分类信息,得到modelid
        $Category = new \Think\Category();
        $selfCate = $Category::getSelf(getCategory(0), $cid);
        //当前栏目信息
        $modelid = $selfCate['modelid'];

        //userid暂时不做插入。因为这是后台操作
        $data = array(
            'title' => $title, 
            'copyfrom' => $copyfrom, 
            'author' => $author, 
            'shorttitle' => I('shorttitle'), 
            'color' => I('color'), 
            'cid' => $cid, 
            'litpic' => $pic, 
            'keywords' => I('keywords'), 
            'description' => $description, 
            'updatetime' => time(), 
            'status' => 0, 
            'commentflag' => I('commentflag', 0, 'intval'), 
            'flag' => $flag, 
            'jumpurl' => $jumpurl, 
            'aid' => $_SESSION['aid'],
            'addtable'=>"addon".$add_table
        );
        if (M($add_table)->where(array('id'=>$aid))->save($data)) {
            //更新上传附件表
            if (!empty($pic)) {
                //上传缩略图操作的时候已经插入附件表
                $attid = M('attachment')->where(array('filepath' => $pic))->getField('id');
                if ($attid) {
                    M('attachmentindex')->add(array('attid' => $attid, 'arcid' => $aid, 'modelid' => $modelid));
                }
            }
            //加入附加表的数据
            //$addon_data=array('aid'=>$aid);
            $post_data=I('post.');
            //剔除部分数据，只保留给附加表字段的数据
            foreach ($post_data as $key => $value) {
                foreach ($data as $k => $v) {
                    if ($key==$k) {
                        unset($post_data[$k]);
                    }
                }
                unset($post_data['id']);
                unset($post_data['pid']);
                unset($post_data['flags']);
                unset($post_data['model_name']);
            }

            //检查出是否是多图片上传字段
            $imgfiles=M('model_field')->where(array('model_id'=>$modelid,'type'=>'imgfile'))->field('field')->select();

            for ($i=0; $i <count($imgfiles) ; $i++) { 
                $pictureurls_arr=array();
                $key=$imgfiles[$i]['field'];
                if (array_key_exists($key,$post_data)) {
                    foreach ($post_data[$key] as $k => $v) {
                        $pictureurls_arr[] = $v . '$$$' . '$$$'; //array('url'=> $v ,'alt'=> '');
                            if ($k == 0) {
                                $pic = $v;
                            }
                    }
                    $post_data[$key] = join('|||', $pictureurls_arr);
                }
            }
            //检查出是否是多选字段
            $checkboxs=M('model_field')->where(array('model_id'=>$modelid,'type'=>'checkbox'))->field('field')->select();
            for ($i=0; $i <count($checkboxs) ; $i++) { 
                $checkboxs_arr=array();
                $key=$checkboxs[$i]['field'];
                if (array_key_exists($key,$post_data)) {
                    $checkboxs_arr=$post_data[$key];
                    $post_data[$key] = join(',', $checkboxs_arr);//因为存储checkbox数据是以SET格式存储
                }
            }
            //检查出是否是日期类型，转成时间戳进行储蓄
            //检查出是否是多选字段
            $datetimes=M('model_field')->where(array('model_id'=>$modelid,'type'=>'datetime'))->field('field')->select();
            for ($i=0; $i <count($datetimes) ; $i++) { 
                $tims="";
                $key=$datetimes[$i]['field'];
                if (array_key_exists($key,$post_data)) {
                    $datetimes_str=strtotime($post_data[$key]);//转成时间戳
                    $post_data[$key] = $datetimes_str;
                }
            }
            //$addon_data=array_merge($addon_data,$post_data);
            //print_r($post_data);
            M("addon".$add_table)->where(array('aid'=>$aid))->save($post_data);

            delCacheHtml('List/index_' . $cid, false, 'list:index');
            delCacheHtml('Index_index', false, 'index:index');
            yes('修改成功！', true, 'article-index,,sub-article-index');
        } else {
            error('修改失败');
        }
    }

    //修改文章处理

    //回收站文章列表
    public function trach()
    {
        $db=I('model');
        $where = array('status' => 1);
        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count = M($db)->where($where)->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $art = M($db)->where($where)->order('id desc')->limit($limit)->select();
        $this->pid = I('pid', 0, 'intval');
        $this->page = $page->show();
        $this->vlist = $art;
        $this->subcate = '';
        $this->model=$db;
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->display('index');
    }

    //删除文章到回收站  兼容三个地方删除操作
    public function del()
    {
        $pid = I('get.pid', 0, 'intval');
        $db=I('get.model');
        if (IS_POST) {
            $id = I('get.id'); //顶部删除
            if (empty($id)) {
                $idArr = I('post.id'); //右下角删除
            } else {
                $idArr = explode(',', $id);
            }
        } else { //点击删除
            $idArr = I('id', 0, 'intval');
            $idArr = array($idArr);
        }
        if ($idArr[0] == 0 || $idArr == 0) {
            error('没有选择删除项！');
        }
        if (M($db)->where(array('id' => array('in', $idArr)))->setField('status', 1)) {
            foreach ($idArr as $v) {
                delCacheHtml('Show/index_*_' . $v . '.', false, 'show:index');
            }
            yes('删除成功');
        } else {
            error('删除失败');
        }
    }

    //还原文章
    public function restore()
    {
        $pid = I('get.pid', 0, 'intval');
        $db=I('get.model');
        if (IS_POST) {
            $id = I('get.id');
            $idArr = explode(',', $id);
        }else{
            $idArr = I('id', 0, 'intval');
            $idArr = array($idArr);
        }
        if (M($db)->where(array('id' => array('in', $idArr)))->setField('status', 0)) {
            yes('还原成功！',false,'article-index');
        } else {
            error('还原失败');
        }
    }


    //彻底删除文章
    public function clear()
    {
        $pid = I('get.pid', 0, 'intval');
        $db=I('get.model');
        if (IS_POST) {
            $id = I('get.id');
            $idArr = explode(',', $id);
        }else{
            $idArr = I('id', 0, 'intval');
            $idArr = array($idArr);
        }
        $where = array('id' => array('in', $idArr));
        $modelid = M($db)->where(array('id' => $idArr[0]))->getField('modelid');

        if (M($db)->where($where)->delete()) {
            if ($modelid) {
                $where = array('arcid' => array('in', $idArr), 'modelid' => $modelid);
                $attid = M('attachmentindex')->where($where)->Field('attid')->select();
                $uid = arrToStr($attid);
                $map['id'] = array('exp', ' IN (' . $uid . ') ');
                /*$upath = M('attachment')->where($map)->Field('filepath')->select();
                foreach ($upath as $key => $value) {
                    @unlink('.' . $value['filepath']);
                }*/

                M('attachment')->where($map)->delete();
                M('attachmentindex')->where($where)->delete();
            }
            $addon_where=array('aid' => array('in', $idArr));
            M("addon".$db)->where($addon_where)->delete();
            yes('彻底删除成功！');
        } else {
            error('彻底删除失败');
        }
    }

    /*
     上传图片
     */
    public function upload()
    {
        $upload = new \Think\Upload();
        // 实例化上传类
        $upload->maxSize = 3145728;
        // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');
        // 设置附件上传类型
        $upload->rootPath = C('ROOTPATH');
        // 设置附件上传根目录
        $upload->savePath = '';
        // 设置附件上传目录
        // 上传文件
        $info = $upload->upload();
        //这里判断是否上传成功
        if ($info) {
            //// 上传成功 获取上传文件信息
            $db = M('attachment');
            foreach ($info as &$file) {

                //入库附件表格
                $savepath = $file['savepath'];
                $data['filepath'] = ltrim(C('ROOTPATH'), ".") . $savepath . $file['savename'];
                $data['title'] = $file['name'];
                $data['haslitpic'] = empty($file['haslitpic']) ? 0 : 1;
                $filetype = 1;
                //后缀
                switch ($file['ext']) {
                    case 'gif' :
                        $filetype = 1;
                        break;
                    case 'jpg' :
                        $filetype = 1;
                        break;
                    case 'png' :
                        $filetype = 1;
                        break;
                    case 'bmp' :
                        $filetype = 1;
                        break;
                    case 'swf' :
                        //flash
                        $filetype = 2;
                        break;
                    case 'mp3' :
                        //音乐
                        $filetype = 3;
                        break;
                    case 'wav' :
                        $filetype = 3;
                        break;
                    case 'rm' :
                        //电影
                        $filetype = 4;
                        break;

                    case 'doc' :
                        //
                        $filetype = 5;
                        break;
                    case 'docx' :
                        //
                        $filetype = 5;
                        break;
                    case 'xls' :
                        //
                        $filetype = 5;
                        break;
                    case 'ppt' :
                        //
                        $filetype = 5;
                        break;
                    case 'zip' :
                        //
                        $filetype = 6;
                        break;
                    case 'rar' :
                        //
                        $filetype = 6;
                        break;
                    case '7z' :
                        //
                        $filetype = 6;
                        break;

                    default :
                        //其他
                        $filetype = 0;
                        break;
                }
                $data['filetype'] = $filetype;
                $data['filesize'] = $file['size'];
                $data['uploadtime'] = time();
                $data['aid'] = $_SESSION['aid'];
                //管理员ID
                $db->add($data);
                //入库附件表结束

                //拼接出上传目录
                $file['rootpath'] = __ROOT__ . ltrim($setting['rootPath'], ".");
                //拼接出文件相对路径
                $file['filepath'] = $file['rootpath'] . $file['savepath'] . $file['savename'];

                echo json_encode(array('url' => $file['filepath'], 'title' => htmlspecialchars($_POST['pictitle'], ENT_QUOTES), 'original' => $file['savename'], 'state' => 'SUCCESS'));
            }

            exit();
        } else {
            //输出错误信息
            exit($this->uploader->getError());
        }
    }

    //文件夹管理
    function browseFile($spath = '', $stype = 'file')
    {
        $base_path = '/Uploads';
        $enocdeflag = I('encodeflag', 0, 'intval');
        switch ($stype) {
            case 'picture' : //图片文件夹
                $base_path = ltrim(C('ROOTPATH'), ".");
                break;
            case 'file' : //文档文件夹
                $base_path = ltrim(C('DOC_SAVE_PATH'), ".");
                break;
            case 'ad' :
                $base_path = '/uploads/abc1';
                break;
            default :
                exit('参数错误');
                break;
        }

        if ($enocdeflag) {
            $spath = base64_decode($spath);
        }

        $spath = str_replace('.', '', ltrim($spath, $base_path));

        $path = $base_path . '/' . $spath;


        $dir = new \Think\Dir('.' . $path);
        //加上.
        $list = $dir->toArray();
        for ($i = 0; $i < count($list); $i++) {

            $list[$i]['isImg'] = 0;
            if ($list[$i]['isFile']) {
                $url = __ROOT__ . rtrim($path, '/') . '/' . $list[$i]['filename'];
                $ext = explode('.', $list[$i]['filename']);
                $ext = end($ext);
                if (in_array($ext, array('jpg', 'png', 'gif'))) {
                    $list[$i]['isImg'] = 1;
                }
            } else {
                //为了兼容URL_MODEL(1、2)
                if (C('URL_MODEL') == 1 || C('URL_MODEL') == 2) {
                    $url = U('Article/browseFile', array('stype' => $stype, 'encodeflag' => 1, 'spath' => base64_encode(rtrim($path, '/') . '/' . $list[$i]['filename'])));
                } else {
                    $url = U('Article/browseFile', array('stype' => $stype, 'spath' => rtrim($path, '/') . '/' . $list[$i]['filename']));
                }

            }
            $list[$i]['url'] = $url;
            $list[$i]['size'] = get_byte($list[$i]['size']);
        }
        //p($list);
        $parentpath = substr($path, 0, strrpos($path, '/'));
        //为了兼容URL_MODEL(1、2)
        if (C('URL_MODEL') == 1 || C('URL_MODEL') == 2) {
            $this->purl = U('Article/browseFile', array('spath' => base64_encode($parentpath), 'encodeflag' => 1, 'stype' => $stype));
        } else {
            $this->purl = U('Article/browseFile', array('spath' => $parentpath, 'stype' => $stype));
        }
        $this->type = '浏览文件';
        $this->vlist = $list;
        $this->stype = $stype;
        $this->display('browseFile');

    }

    //百度编辑器浏览本地图片
    public function getFileOfImg()
    {

        header("Content-Type: text/html; charset=utf-8");

        $action = I('action', '', 'trim');
        if (IS_POST && $action != 'get') {
            exit();
        }


        //需要遍历的目录列表，最好使用缩略图地址，否则当网速慢时可能会造成严重的延时
        //$paths = './uploads/img1';

        //显示有缩略图　文件
        $files = M('attachment')->where(array('filetype' => 1, 'haslitpic' => 0))->order('uploadtime DESC')->getField('filepath', 50); //最新50条

        if (!count($files)) return;
        rsort($files, SORT_STRING);
        $str = "";
        foreach ($files as $file) {
            $str .= $file . "ue_separate_ue";
            $file = preg_replace('/\.(.+)$/', '_m.$1', $file); //缩略图
            $str .= $file . "ue_separate_ue";
        }
        echo $str;

    }


    //ueditor抓取远程图片
    public function getRemoteImage()
    {
        //远程抓取图片配置
        $config = array(
            "savePath" => C('ROOTPATH') . date('Y-m-d') . '/', //保存路径
            "allowFiles" => array(".gif", ".png", ".jpg", ".jpeg", ".bmp"), //文件允许格式
            "maxSize" => 3000 //文件大小限制，单位KB
        );
        $uri = htmlspecialchars($_POST['upfile']);
        $uri = str_replace("&amp;", "&", $uri);

        //忽略抓取时间限制
        set_time_limit(0);
        //ue_separate_ue  ue用于传递数据分割符号
        $imgUrls = explode("ue_separate_ue", $uri);
        $tmpNames = array();
        foreach ($imgUrls as $imgUrl) {
            //http开头验证
            if (strpos($imgUrl, "http") !== 0) {
                array_push($tmpNames, "error");
                continue;
            }
            //获取请求头
            $heads = get_headers($imgUrl);
            //死链检测
            if (!(stristr($heads[0], "200") && stristr($heads[0], "OK"))) {
                array_push($tmpNames, "error");
                continue;
            }

            //格式验证(扩展名验证和Content-Type验证)
            $fileType = strtolower(strrchr($imgUrl, '.'));
            if (!in_array($fileType, $config['allowFiles']) || stristr($heads['Content-Type'], "image")) {
                array_push($tmpNames, "error");
                continue;
            }

            //打开输出缓冲区并获取远程图片
            ob_start();
            $context = stream_context_create(
                array(
                    'http' => array(
                        'follow_location' => false // don't follow redirects
                    )
                )
            );
            //请确保php.ini中的fopen wrappers已经激活
            readfile($imgUrl, false, $context);
            $img = ob_get_contents();
            ob_end_clean();

            //大小验证
            $uriSize = strlen($img); //得到图片大小
            $allowSize = 1024 * $config['maxSize'];
            if ($uriSize > $allowSize) {
                array_push($tmpNames, "error");
                continue;
            }
            //创建保存位置
            $savePath = $config['savePath'];
            if (!file_exists($savePath)) {
                mkdir("$savePath", 0777);
            }
            //写入文件
            $tmpName = $savePath . rand(1, 10000) . time() . strrchr($imgUrl, '.');
            try {
                $fp2 = @fopen($tmpName, "a");
                fwrite($fp2, $img);
                fclose($fp2);
                $tmpName = ltrim($tmpName, ".");
                array_push($tmpNames, $tmpName);
            } catch (Exception $e) {
                array_push($tmpNames, "error");
            }
        }

        //ltrim(implode( "ue_separate_ue" , $tmpNames ), ".")
        /**
         * 返回数据格式
         * {
         *   'url'   : '新地址一ue_separate_ue新地址二ue_separate_ue新地址三',
         *   'srcUrl': '原始地址一ue_separate_ue原始地址二ue_separate_ue原始地址三'，
         *   'tip'   : '状态提示'
         * }
         */

        echo "{'url':'" . implode("ue_separate_ue", $tmpNames) . "','tip':'远程图片抓取成功！','srcUrl':'" . $uri . "'}";
    }


}

?>