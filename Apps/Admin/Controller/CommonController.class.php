<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class CommonController extends Controller {

    public function _initialize(){

        $user=M('members')->where(array('Id'=>$_SESSION['aid']))->find();

        if (!isset($_SESSION['aid']) || $user==NULL || $user['status']==1) {
            session('aid',NULL);
            session('uname',NULL);
            redirect(U('Index/index'));
        }
       
		$rule=strtolower(MODULE_NAME."/".CONTROLLER_NAME."/".ACTION_NAME);
		$allow_url=C('AUTH_ISALLOW_URL');
        //print_r(MODULE_NAME."/".CONTROLLER_NAME."/".ACTION_NAME);
        //权限验证
        if(!in_array_case($rule, $allow_url)){
        	if(!authCheck($rule,$_SESSION['aid'],array('in','1,2'))){
	            error('你没有权限!');
	        }
        }
    }

      
}