<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class PersonController extends CommonController {

    //修改密码
    public function pwd() {
        if (!IS_POST) {
            $this->display();
            exit();
        }
        
        $id = $_SESSION['aid'];
        $oldpassword = I('oldpassword', '');
        $password = I('password', '');
        $rpassword = I('rpassword', '');
        if (empty($oldpassword)) {
            error('请填写旧密码！');
        }
        if (empty($password)) {
            error('请填写新密码！');
        }

        if ($password != $rpassword) {
            error('两次密码不一样，请重新填写！');
        }
        
        $self = M('members')->field(array('password','encrypt'))->where(array('Id' => $id))->find();
        if (!$self) {
            error('用户不存在，请重新登录');
        }

        if (get_password($oldpassword, $self['encrypt']) != $self['password']) {
            
            error('旧密码错误');
        }


        $passwordinfo = get_password($password);

        $data = array(
            'Id'        => $id,
            'password'      => $passwordinfo['password'],       
            'encrypt'       => $passwordinfo['encrypt']
            );

        if (false !== M('members')->save($data)) {

            session('aid',NULL);
            session('aname',NULL);
            session(NULL);
            //$this->success('修改密码成功,请重新登录', U('Index/index'));
            yes('修改密码成功！',true);
            //$this->redirect(U('Index/index'));
        }else {
            error('修改密码失败');
        }
        
    }
}
?>