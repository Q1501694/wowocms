<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class ItemgroupController extends CommonController {
    
    public function index() {
                    
        //分页
        $count = M('itemgroup')->count();

        $page = $page = new \Think\Page($count, 10);
        $page->rollPage = 7;
        $page->setConfig('theme','%totalRow% %header%  %first% %upPage% %linkPage% %downPage% %end% %nowPage%/%totalPage% 页');
        $limit = $page->firstRow. ',' .$page->listRows;
        $list = M('itemgroup')->order('id')->limit($limit)->select();

        $this->page = $page->show();
        $this->vlist = $list;
        $this->type = '联动分组列表';

        $this->display();
    }
    public function add() {
        $actionName = strtolower($Think.CONTROLLER_NAME);
        if (IS_POST) {
            $validate = array(
                array('remark','require','组名必须填写！'), 
                array('name','require','英文组名必须填写！'), 
                array('name','','英文组名已经存在！',0,'unique',1), 
            );
            $db = M('itemgroup');
            if (!$db->validate($validate)->create()) {
                $this->error($db->getError());
            }
            if($id = M('itemgroup')->add()) {
                yes('添加成功',true,'model-index');
            }else {
                error();
            }
        }
        $this->display();
    }



    public function edit() {

        $id = intval($_GET['id']);
        $actionName = strtolower($Think.CONTROLLER_NAME);
        if (IS_POST) {
            $name = I('name', '', 'htmlspecialchars,trim');
            $remark = I('remark', '', 'htmlspecialchars,trim');
            $id=I('post.id');
            if (empty($remark)) {
                error('组名必须填写！');
            }
            if (empty($name)) {
                error('英文组名必须填写！');
            }
            $db=M('itemgroup');
            $count=$db->where(array('name'=>$name))->count();
            if ($count>=1) {
                error('英文组名已经存在！');
            }
            $data = array('name' =>$name ,'remark'=>$remark,'status'=>I('post.status',0,'intval'));
            if ($db->where(array('id'=>$id))->save($data)) {
                yes('修改成功！',true,'model-index');
            }else {
                error('修改失败');
            }
           
        }
        $this->vo = M($actionName)->find($id);
        $this->display();
    }

    //彻底删除文章
    public function del() {

        $id = I('id',0 , 'intval');
        $Model =M();        
        $batchFlag = intval($_GET['batchFlag']);
        //批量删除
        if ($batchFlag) {
            $this->delBatch();
            return;
        }

        //getField('id'),返回一个结果，getField('id',true),返回满足的所有(数组)
        $child= $Model->table(C('DB_PREFIX'). 'iteminfo I')->join('inner join '. C('DB_PREFIX').'itemgroup G on I.group = G.name')->where(array('G.id' => $id))->getField('I.id');
        if($child) {
            error('请先删除分组下的联动信息，再删除分组');
        }

        if (M('itemgroup')->delete($id)) {
            yes('彻底删除成功');
        }else {
            error('彻底删除失败');
        }
    }


    //批量彻底删除文章
    public function delBatch() {

        $idArr = I('key',0 , 'intval');     
        if (!is_array($idArr)) {
            $this->error('请选择要彻底删除的项');
        }
        $where = array('id' => array('in', $idArr));

        //getField('id'),返回一个结果，getField('id',true),返回满足的所有(数组)
        $Model =M();
        $child= $Model->table(C('DB_PREFIX'). 'iteminfo I')->join('inner join '. C('DB_PREFIX').'itemgroup G on I.group = G.name')->where(array('G.id' => array('in', $idArr)))->getField('I.id');
        if($child) {
            $this->error('请先删除分组下的联动信息，再删除分组');
        }


        if (M('itemgroup')->where($where)->delete()) {
            $this->success('彻底删除成功', U('Itemgroup/index'));
        }else {
            $this->error('彻底删除失败');
        }
    }




}



?>