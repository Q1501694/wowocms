<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class GuestbookController extends CommonController {

    public function index(){

        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count=M('guestbook')->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;

        $list = M('guestbook')->order('id desc')->limit($limit)->select();
        $this->page = $page->show();
        $this->vlist = $list;
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->type = '留言本管理';

        $this->display();

    }

    //回复处理
    public function reply() {
        $id = I('id', '', 'intval');
        $reply = I('reply', '', 'trim');
        if (!$id) {
            error('参数错误');
        }
        $data = array(
            'id' => $id,
            'reply' => $reply,
            'replytime' => time()
        );
        if (M('guestbook')->save($data)) {
            //$this->success('修改成功', U('Guestbook/index'));
            yes('回复成功！',true,'Guestbook-index');
        }else {
            error('回复失败！');
        }
    }

    public function look(){
        $id=I('get.id');
        $book=M('guestbook')->where(array('id'=>$id))->find();
        $this->book=$book;
        $this->display();
    }

    //彻底删除文章
    public function del() {
        $id = I('id',0 , 'intval');
        $batchFlag = intval($_GET['batchFlag']);
        //批量删除
        if ($batchFlag) {
            $this->delBatch();
            return;
        }
        
        if (M('guestbook')->delete($id)) {
            yes('删除成功！');
        }else {
            error('删除失败');
        }
    }


    //批量彻底删除文章
    public function delBatch() {

        $idArr = I('key',0 , 'intval');     
        if (!is_array($idArr)) {
            $this->error('请选择要彻底删除的项');
        }
        $where = array('id' => array('in', $idArr));

        if (M('guestbook')->where($where)->delete()) {
            $this->success('彻底删除成功', U('Guestbook/index'));
        }else {
            $this->error('彻底删除失败');
        }
    }

}

?>