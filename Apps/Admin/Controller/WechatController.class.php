<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;

use Think\Controller;
use Com\Wechat;

class WechatController extends CommonController{     

    public function index(){    

        $token=trim(C('cfg_wechat_token'));        
        if (empty($token)) {
            $token=get_wechat_token();
        }
        $this->wtype=C('cfg_wechat_type');
        $this->token=$token;
        $this->type='微信信息配置';
        $this->display();       
    }
    //信息配置
    public function editHuiType(){       
        $data['cfg_wechat_token']=I('cfg_wechat_token');
        $data['cfg_wechat_type']=I('cfg_wechat_type');
        if (IS_POST) {
            if (writeArr($data,APP_PATH.'Home/Conf/'.'config.wechat.php') && writeArr($data,APP_PATH.'Admin/Conf/'.'config.wechat.php')) {
                yes('保存成功！');
            }else{
                error('保存失败！');
            }
        }else{
            exit;
        }       
    }
    //文字信息
    public function words(){
        $keyword = I('keyword', '', 'htmlspecialchars,trim');
        if (!empty($keyword)) {
            $where['keywords'] = array('LIKE', "%{$keyword}%");
        }
        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count=M('chatwords')->where($where)->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $list = M('chatwords')->where($where)->order('id desc')->limit($limit)->select();
        $this->page = $page->show();
        $this->vlist = $list;
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->type = '关键字管理';
        $this->display();
    }

    public function awords(){
        $action=I('action');
        $id=I('id');
        if (IS_POST) {
            if ($action == 'edit') {
                $this->awordsEdit();
            }else{
                $this->awordsHandle();
            }
        }
        if ($action=='edit') {
            $this->words=$words=M('chatwords')->where(array('id'=> $id))->find();
            $this->action='edit';
        }
        $this->display();
    }
    public function awordsHandle(){
        $validate = array(
            array('title','require','标题必须填写！'), 
            array('keywords','require','关键字必须填写！'), 
            array('content','require','回复内容必须填写！'), 
        );
        $db = M('chatwords');
        if (!$db->validate($validate)->create()) {
            error($db->getError());
        }
        $data = array(
            'title' =>I('title','','trim,strip_tags') , 
            'keywords' =>I('keywords','','trim,strip_tags') , 
            'sort' =>I('sort',0,'trim,strip_tags') ,
            'content' =>I('content','','trim,strip_tags') , 
            'publictime' =>time() , 
            'isnews'=> 0,
            'iszidong'=>I('iszidong'),
            'aid' =>$_SESSION['aid']
        );
        if ($db->add($data)) {
            yes('添加成功！',true,'Wechat-words');
        }else{
            error('添加失败！');
        }
    }
    public function atuwen(){
        $action=I('action');
        $id=I('id');
        if (IS_POST) {
            if ($action == 'edit') {
                $this->atuwenEdit();
            }else{
                $this->atuwenHandle();
            }
        }
        if ($action=='edit') {
            $this->tuwen=$tuwen=M('chatwords')->where(array('id'=> $id))->find();
            $this->action='edit';
        }
        $this->display();
    }

    public function atuwenHandle(){
        $validate = array(
            array('title','require','标题必须填写！'), 
            array('keywords','require','关键字必须填写！'), 
            array('description','require','描述必须填写！'),
            array('url','require','URL必须填写！'), 
            array('picurl','require','缩略图必须填写！'), 
        );
        $db = M('chatwords');
        if (!$db->validate($validate)->create()) {
            $this->error($db->getError());
        }

        $data = array(
            'title' =>I('title','','trim,strip_tags') , 
            'sort' =>I('sort',0,'trim,strip_tags') ,
            'keywords' =>I('keywords','','trim,strip_tags') , 
            'content' =>I('description','','trim,strip_tags') , 
            'url' =>I('url','','trim,strip_tags') , 
            'picurl' =>I('picurl') , 
            'publictime' =>time() , 
            'isnews'=> 1,
            'iszidong'=> I('iszidong'),
            'aid' =>$_SESSION['aid']
            );
        if ($db->add($data)) {
            yes('添加成功！',true,'Wechat-words');
        }else{
            error('添加失败！');
        }
    }

    public function awordsEdit(){
        $validate = array(
            array('title','require','关键字必须填写！'), 
            array('keywords','require','关键字必须填写！'), 
            array('content','require','回复内容必须填写！'), 
        );
        $db = M('chatwords');
        if (!$db->validate($validate)->create()) {
            error($db->getError());
        }
        $data = array(
            'title' =>I('title','','trim,strip_tags') , 
            'sort' =>I('sort',0,'trim,strip_tags') ,
            'keywords' =>I('keywords','','trim,strip_tags') , 
            'content' =>I('content','','trim,strip_tags') ,             
            'iszidong'=>I('iszidong'),
            'aid' =>$_SESSION['aid']
            );
        $where = array('id' =>I('id'));
        $num=M('chatwords')->where($where)->save($data);
        if ($num) {
            yes('修改成功！',true,'Wechat-words');
        }else{
            error('修改失败！');
        }
    }
    public function atuwenEdit(){
        $validate = array(
            array('title','require','标题必须填写！'), 
            array('keywords','require','关键字必须填写！'), 
            array('description','require','描述必须填写！'),
            array('url','require','URL必须填写！'), 
            array('picurl','require','缩略图必须填写！'), 
        );
        $db = M('chatwords');
        if (!$db->validate($validate)->create()) {
            error($db->getError());
        }
        $data = array(
            'title' =>I('title','','trim,strip_tags') , 
            'keywords' =>I('keywords','','trim,strip_tags') , 
            'sort' =>I('sort',0,'trim,strip_tags') ,
            'content' =>I('description','','trim,strip_tags') , 
            'url' =>I('url','','trim,strip_tags') , 
            'picurl' =>I('picurl') , 
            'isnews'=> 1,
            'iszidong'=> I('iszidong'),
            'aid' =>$_SESSION['aid']
            );
        $num=M('chatwords')->where(array('id'=>I('id')))->save($data);
        if ($num) {
            yes('修改成功！',true,'Wechat-words');
        }else{
            error('修改失败！');
        }
    }

    public function del(){
        $id=I('id');
        $batchFlag=I('get.batchFlag');
        if ($batchFlag) {
            $this->batchFlag();
        }
        $num=M('chatwords')->where(array('id'=>$id))->delete();
        if ($num) {
             yes('修改成功！');
        }else{
            error('删除失败!');
        }
    }

    public function batchFlag(){
        $id = I('get.id');
        $idArr = explode(',', $id);
        if (empty($idArr)||$idArr[0]==0) {
            error('请选择要删除的列');
        }
        if (M('chatwords') -> where(array('id' => array('in', $idArr))) ->delete()) {
            yes('删除成功!');
        } else {
            error('删除失败!');
        }
    }

    //批量更新排序
    public function sort() {
    
        foreach ($_POST as $k => $v) {
            if ($k == 'key') {
                continue;
            }
            M('chatwords')->where(array('id'=>$k))->setField('sort',$v);
     
        }
        $this->redirect('Wechat/words');
    }

}