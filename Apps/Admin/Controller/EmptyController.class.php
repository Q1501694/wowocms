<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class EmptyController extends Controller{
    public function _empty(){
        header("Content-Type: text/html; charset=utf-8");
        header("Location:/Public/error.html");
    }
    public function index(){
        header("Content-Type: text/html; charset=utf-8");
        header("Location:/Public/error.html");

    }
}
