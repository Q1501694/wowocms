<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class OrderController extends CommonController {

    /**
     * 订单列表
     * Author:寒战
     * QQ：211330266 201177205
     * Copyright:广西南宁市铭成龙毅网络科技有限公司
     */
    public function index(){
        $keyword = I('keyword', '', 'htmlspecialchars,trim');
        if (!empty($keyword)) {
            $where['ordid'] = array('LIKE', "%{$keyword}%");
        }
        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count=M('orderlist')-> where($where)->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $list=M('orderlist')->field('payment_type,payment_trade_no,payment_trade_status,payment_notify_id,payment_notify_time,payment_buyer_email,getname,shouhuo,youbian,phone,beizhu',true)-> where($where) ->order('id desc')->limit($limit)->select();
        $this->keyword = $keyword;
        $this -> page = $page -> show();
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->list=$list;
        $this->display();
    }

    /**
     * 查看收货信息
     * Author:寒战
     * QQ：211330266 201177205
     * Copyright:广西南宁市铭成龙毅网络科技有限公司
     */
    public function look(){
        $id=I('get.id','','intval');
        if (!$id) {
            $this->error('参数错误！');
        }
        $order=M('orderlist')->field('ordid,getname,shouhuo,youbian,phone,beizhu')->where(array('id'=>$id))->find();
        $this->order=$order;
        $this->display();
    }

    /**
     * 修改订单视图
     * Author:寒战
     * QQ：211330266 201177205
     * Copyright:广西南宁市铭成龙毅网络科技有限公司
     */
    public function edit(){
        $id=I('get.id','','intval');
        if (!$id) {
            $this->error('参数错误！');
        }
        if (IS_POST) {
            $this->editHandle();
        }
        $order=M('orderlist')->field('ordbuynum,ordprice,ordfee,ordid,getname,shouhuo,youbian,phone,beizhu')->where(array('id'=>$id))->find();
        $this->order=$order;
        $this->display();
    }

    /**
     * 处理修改订单
     * Author:寒战
     * QQ：211330266 201177205
     * Copyright:广西南宁市铭成龙毅网络科技有限公司
     */
    public function editHandle(){
        $orderid=I('post.ordid','');
        if (!$orderid) {
            $this->error('非法操作！');
        }
        $pre=substr($orderid,0,2);
        if ($pre=="cz") {//如果是充值订单
            $ordfee=I('post.ordfee','');
            if (!$ordfee) {
                error('充值金额不能为空！');
            }
            $datas['ordfee']=$ordfee;
            if (M('orderlist')->where(array('ordid'=>$orderid))->save($datas)) {
                yes('编辑成功！',true,'Order-index');
            }else{
                error('编辑失败！');
            }

        }else{
            $ordbuynum=I('post.ordbuynum','','intval');
            $getname=I('post.getname','');
            $shouhuo=I('post.shouhuo','');
            $youbian=I('post.youbian');
            $phone=I('post.phone');
            $beizhu=I('post.beizhu');
            if (empty($ordbuynum) || empty($getname) || empty($shouhuo) || empty($youbian) || empty($youbian) || empty($phone) ) {
                error('除备注其他选项都不能为空！');
            }
            $ordprice=M('orderlist')->where(array('ordid'=>$orderid))->getField('ordprice');
            $data['ordbuynum']=$ordbuynum;
            $data['getname']=$getname;
            $data['shouhuo']=$shouhuo;
            $data['youbian']=$youbian;
            $data['phone']=$phone;
            $data['beizhu']=$beizhu;
            $data['ordfee']=$ordprice*$data['ordbuynum'];
            if (M('orderlist')->where(array('ordid'=>$orderid))->save($data)) {
                yes('编辑成功！',true,'Order-index');
            }else{
               error('编辑失败！');
            }
        }
    }

    /**
     * 删除订单
     * Author:寒战
     * QQ：211330266 201177205
     * Copyright:广西南宁市铭成龙毅网络科技有限公司
     */
    public function del(){
        $id=I('get.id','','intval');
        if (!$id) {
            error('参数错误！');
        }
        if (M('orderlist')->where(array('id'=>$id))->delete()) {
            yes('删除成功！');
        }else{
            error('删除失败！');
        }
    }

    /**
     * 购物车列表
     * Author:寒战
     * QQ：211330266 201177205
     * Copyright:广西南宁市铭成龙毅网络科技有限公司
     */
    public function cart(){
        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count=M('shopcart')->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $carts=M('shopcart') ->order('id desc') ->limit($limit)->select();
        $this -> page = $page -> show();
        $this->carts=$carts;
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->display();

    }
    public function cartDel(){
        $id=I('get.id','','intval');
        if (!$id) {
            error('参数错误！');
        }
        if (M('shopcart')->where(array('id'=>$id))->delete()) {
            yes('删除成功！');
        }else{
            error('删除失败！');
        }
    }

    public function amlog(){
        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count=M('amlog')->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $logs=M('amlog') ->order('id desc') ->limit($limit)->select();

        $total_ru=M('amlog')->where(array('dtype'=>1))->sum('orderfee');
        $total_chu=M('amlog')->where(array('dtype'=>0))->sum('orderfee');

        $this->ru=$total_ru;
        $this->chu=$total_chu;
        $this -> page = $page -> show();
        $this->logs=$logs;
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->display();
    }
}