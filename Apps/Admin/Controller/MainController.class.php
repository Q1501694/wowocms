<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class MainController extends CommonController {

    public function index(){
        //$this->menudoclist = D('CategoryView')->where(array('pid' => 0 , 'type' => 0))->order('category.sort')->select();

        /*print_r($menudoclist);

        die;*/
        /*
		 * Array
(
    [id] => 1
    [name] => Admin/Auth/groupIndex
    [title] => 用户组设置页面
    [type] => 1
    [term] => 
    [status] => 1
    [mid] => 5
    [moduleName] => 权限管理
)
		 */
        $menu=array();
		$mid=array();
		$menus=D('RuleView')->where(array('type'=>2))->order('rule.sort asc,s asc')->select();
		//p(D('RuleView')->_sql());
		for($i=0;$i<count($menus);$i++){
			$mid[]=$menus[$i]['mid'];
			$mid=array_unique($mid);
		}
		$dd=array();
		foreach($mid as $m=>$mm){
			$dd[]=$mm;
		}
		for($k=0;$k<count($dd);$k++){
			foreach($menus as $key =>$value){
				$m_key=$value['moduleName'];
				if($value['mid']==$dd[$k]){
					$menu[$m_key][]=$value;
				}
			}
		}
		/*$is=authCheck('Admin/Article/index,Admin/Auth/access',$_SESSION['aid'],array('in','1,2'));
		var_dump($is);die;*/
		//p($menu);
		$this->menus=$menu;
        $this->display();
    }

    public function getParentCate(){
        header("Content-Type:text/html; charset=utf-8");//不然返回中文乱码
        $count = D('CategoryView')->where(array('pid' => 0 , 'type' => 0))->count();
        $list = D('CategoryView')->where(array('pid' => 0 , 'type' => 0))->order('category.sort')->select();
        $menudoclist = array('count' => $count);
        foreach ($list as $v) {
            $menudoclist['list'][] = array(
                'id' => $v['id'],               
                'name' => $v['name'],       
                'url' => U(ucfirst($v['tablename']) .'/index', array('pid'=>$v['id']))
            );
        }
        exit(json_encode($menudoclist));
    }



      
}