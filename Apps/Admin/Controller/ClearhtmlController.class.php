<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class ClearhtmlController extends CommonController {

    public function index() {

    }

    //一键更新静态缓存html
    public function all() {

        if (IS_POST) {
            delCacheHtml('', true);
            //$this -> success('更新成功!', U('ClearHtml/all'));
            yes('更新成功！');
            exit();
        }
        $this -> type = '一键更新';
        $this -> display();
    }

    //更新首页静态缓存html
    public function home() {

        if (IS_POST) {
            delCacheHtml('Index_index', false, 'index:index');
            yes('更新成功！');
            exit();
        }

        $this -> type = '更新首页|静态缓存';
        $this -> display('all');
    }

    //更新栏目静态缓存html
    public function lists() {

        if (IS_POST) {
            $isall = I('get.isall', 0, 'intval');

            if ($isall) {
                delCacheHtml('List', true, '');
            } else {
                $idArr = I('key', array(), '');
                if (empty($idArr)) {
                    error('请选择栏目！');
                }
                $cate = M('category') -> where(array('id' => array('IN', $idArr), 'type' => 0)) -> field(array('id', 'ename')) -> select();
                foreach ($cate as $v) {
                    //更新静态缓存
                    delCacheHtml('List/index_' . $v['id'] . '_', false, 'list:index');
                    delCacheHtml('List/index_' . $v['ename'], false, 'list:index');
                    //还有只有名称
                }

            }

            //$this -> success('更新成功!', U('ClearHtml/lists'));
            yes('更新成功！');
            exit();
        }

        $cate = D('CategoryView') -> where(array('category.type' => 0)) -> order('category.sort') -> select();
        //$cate = getCategory();
        $Category = new \Think\Category();
        $this -> cate = $Category::unlimitedForLevel($cate, '&nbsp;&nbsp;&nbsp;&nbsp;', 0);

        $this -> type = '更新栏目|静态缓存';
        $this -> display('all');
    }

    //更新内容页静态缓存html
    public function shows() {

        if (IS_POST) {
            $isall = I('get.isall', 0, 'intval');
            if ($isall) {
                delCacheHtml('Show', true, '');
            } else {
                $idArr = I('key', array(), '');
                if (empty($idArr)) {
                    error('请选择栏目！');
                }
                $cate = D('CategoryView') -> where(array('category.id' => array('IN', $idArr), 'type' => 0)) -> field(array('id', 'ename', 'tablename')) -> select();
                foreach ($cate as $v) {
                    //更新静态缓存
                    delCacheHtml('Show/index_' . $v['id'] . '_', false, 'show:index');
                    delCacheHtml('Show/index_' . $v['ename'], false, 'show:index');
                    //还有只有名称
                }

            }

            yes('更新成功!');
            exit();
        }

        $cate = D('CategoryView') -> where(array('category.type' => 0)) -> order('category.sort') -> select();
        //$cate = getCategory();
        $Category = new \Think\Category();
        $this -> cate = $Category::unlimitedForLevel($cate, '&nbsp;&nbsp;&nbsp;&nbsp;', 0);

        $this -> type = '更新内容页(文档)|静态缓存';
        $this -> display('all');
    }

    //更新专题静态缓存html
    public function special() {

        if (IS_POST) {
            $isall = I('get.isall', 0, 'intval');
            if ($isall) {
                delCacheHtml('Special', true, '');
            } else {
                delCacheHtml('Special/index', false, 'special:index');
            }

            $this -> success('更新成功!', U('ClearHtml/special'));
            exit();
        }

        $this -> type = '更新专题|静态缓存';
        $this -> display('all');
    }

}
?>