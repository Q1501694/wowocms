<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class AuthController extends CommonController {

	//管理员列表
	public function index() {
		$keyword = I('keyword', '', 'trim');
		$where = '';
		if (empty($keyword)) {
			$where = array('Id' => array('GT', '0'));
		} else {
			$where = array('username' => array('like', "%$keyword%"));
		}

		$pageSize = I('pageSize', 15, 'intval');
		$pageCurrent = I('pageCurrent', 1, 'intval');

		$count = M('members') -> field('password', true) -> where($where) -> count();
		$startCount = ($pageCurrent - 1) * $pageSize;
		$page = new \Think\Page($count, $pageSize);
		$limit = $startCount . ',' . $pageSize;
		$user = M('members') -> field('password', true) -> where($where) -> limit($limit) -> select();

		//view
		if ($user) {
			foreach ($user as $k => $v) {
				$user[$k]['role'] = D('GroupMemberView') -> where(array('mid' => $v['Id'])) -> select();

			}
		}

		// pp($user);die;
		$this -> keyword = $keyword;
		$this -> page = $page -> show();
		$this -> totalRows = $page -> totalRows;
		$this -> nowPage = $page -> nowPage;
		$this -> user = $user;
		$this -> display();
	}

	//添加/编辑用户
	public function addUser() {

		$uid = I('uid', 0, 'intval');

		$user = M('members') -> find($uid);
		if ($user) {
			$user['password'] = '';
		}
		$userRote = M('auth_group_access') -> where(array('uid' => $uid)) -> find();

		$this -> uid = $uid;
		$this -> user = $user;
		$this -> userRote = $userRote;
		$this -> role = M('auth_group') -> select();
		$this -> display('addUser');
	}

	//添加用户处理
	public function addUserHandle() {
		if (!IS_POST) {

			exit();
		}
		//M验证
		$validate = array( array('username', 'require', '用户名必须填写！'), array('username', '', '用户名已经存在！', 0, 'unique', 1), );
		$data = M('members');
		if (!$data -> validate($validate) -> create()) {
			error($data -> getError());
		}

		$passwordinfo = I('password', '', 'get_password');
		$userData = array('username' => I('username', '', 'trim'), 'password' => $passwordinfo['password'], 'encrypt' => $passwordinfo['encrypt'], 'lasttime' => time(), 'lastip' => get_client_ip(), 'status' => I('islock', 0, 'intval'), 'pretime' => time(), 'preip' => get_client_ip());

		if ($uid = M('members') -> add($userData)) {

			$group_id = I('group_id');
			$accessData = array('uid' => $uid, 'group_id' => $group_id);

			M('auth_group_access') -> add($accessData);
			//$this->success('添加成功', U('Auth/index'));
			yes('修改成功！', true, 'Auth-index');
		} else {

			error('添加失败');
		}

	}

	//修改用户处理
	public function editUser() {

		if (!IS_POST) {
			error('参数错误!');
		}
		//M验证
		$password = trim($_POST['password']);
		$username = I('username', '', 'trim');
		$uid = I('uid', 0, 'intval');
		if (empty($username)) {
			error('用户名必须填写！');
		}

		if ( M('members') -> where(array('username' => $username, 'Id' => array('neq', $uid))) -> find()) {
			error('用户名已经存在！');
		}

		$data = array('username' => $username, 'status' => I('islock', 0, 'intval'));

		//如果密码不为空，即是修改
		if (!$password == '') {
			$passwordinfo = I('password', '', 'get_password');
			$data['password'] = $passwordinfo['password'];
			$data['encrypt'] = $passwordinfo['encrypt'];
		}

		if (false !== M('members') -> where(array('Id' => $uid)) -> save($data)) {

			M('auth_group_access') -> where(array('uid' => $uid)) -> save(array('group_id' => I('group_id')));

			//$this->success('修改成功', U('Auth/index'));
			yes('修改成功！', true, 'Auth-index');
		} else {

			error('修改失败');
		}

	}

	//删除用户处理
	public function delUser() {

		$uid = I('uid', 0, 'intval');
		$batchFlag = intval($_GET['batchFlag']);
		//批量删除
		if ($batchFlag) {
			$this -> delUserAll();
			return;
		}

		if ( M('members') -> where(array('Id' => $uid)) -> delete()) {

			M('auth_group_access') -> where(array('uid' => $uid)) -> delete();
			//$this->success('删除成功', U('Auth/index'));
			yes('删除成功！');
		} else {

			error('删除失败');
		}

	}

	//指量删除用户处理
	public function delUserAll() {

		$id = I('get.id');
		$idArr = explode(',', $id);

		if (empty($idArr) || $idArr[0] == 0) {
			error('请选择要删除的列');
		}

		if ( M('members') -> where(array('Id' => array('in', $idArr))) -> delete()) {
			M('auth_group_access') -> where(array('uid' => array('in', $idArr))) -> delete();
			yes('删除成功！');
		} else {
			error('删除成功');
		}

	}

	//角色权限设置
	public function accessSet() {

		//角色id
		$where['id'] = I('id');
		/*//角色名称
		 $group['name']=I('name');*/
		$m = M('auth_group');
		//获取所有规则id
		$ruleID = $m -> field('rules') -> where($where) -> select();
		$rules = explode(',', $ruleID[0]['rules']);

		$rule = D("RuleView");
		$mid = $rule -> field('mid,moduleName') -> group('mid') -> select();
		foreach ($mid as $v) {
			$map['mid'] = array('in', $v['mid']);
			$map['status'] = '1';
			//只查询开启的
			$result[$v['moduleName']] = $rule -> where($map) -> select();
		}

		/* print_r('<pre>');
		 print_r($rules);
		 print_r('</pre>');
		 die;*/
		// $this->group=$group;
		$this -> result = $result;
		$this -> ruleID = $rules;
		$this -> id = I('id');
		$this -> display('accessSet');

		/*$rid = I('id', 0, 'intval');
		 $access = M('access')->where(array('role_id' => $rid))->getField('node_id' ,true);
		 $where = array('status' => 1);
		 $node = M('node')->where($where)->order('sort')->select();
		 $this->node = nodeForLayer($node, $access);

		 $this->rid =$rid;

		 $this->display();
		 */
	}

	//修改用户组权限
	public function access() {
		$where = array('id' => I('id'));
		$arr = I('rule');
		$data['rules'] = implode(',', $arr);
		$m = M('auth_group');
		$num = $m -> where($where) -> save($data);
		if ($num) {
			yes('权限更新成功!', true);
		} else {
			error('权限更新失败!');
		}
	}

	//权限列表
	public function ruleIndex() {
		$keyword = I('keyword', '', 'trim');
		$group=I('models',0,'intval');
		
		$where = array();
		if (empty($keyword)) {
			$where['id'] = array('GT', '0');
		} else {
			$where['name'] = array('like', "%{$keyword}%");
		}
		if($group){
			$where['mid']=array('eq',$group);
		}
		$m = M('auth_rule');
		$count = $m -> where($where) -> count();
		$pageSize = I('pageSize', 15, 'intval');
		$pageCurrent = I('pageCurrent', 1, 'intval');
		$startCount = ($pageCurrent - 1) * $pageSize;
		$page = new \Think\Page($count, $pageSize);
		$limit = $startCount . ',' . $pageSize;
		$data = $m -> where($where) -> limit($limit) -> order('id desc') -> select();
		$this -> assign('page', $page -> show());
		$this -> assign('role', $data);
		$this -> totalRows = $page -> totalRows;
		$this->models=M('auth_modules')->select();
		$this->mid=$group;
		$this -> keyword = $keyword;
		$this -> nowPage = $page -> nowPage;
		$this -> display('ruleIndex');
	}

	//添加权限
	public function addHandle() {

		$m = M("auth_rule");
		$data['name'] = I('ruleName');
		$data['title'] = I('ruleTitle');
		$data['mid'] = I('mid');
		//过滤方法必须为空,否则验证时会出错
		$data['condition'] = I('post.condition', '', '');
		$data['status'] = I('status');
		$data['type'] = 1;
		//固定，暂时不予处理
		//print_r($data);
		if ($m -> add($data)) {
			yes('添加成功!', true, 'Auth-ruleIndex');
		} else {
			error('添加失败');
		}
	}

	//添加权限页面
	public function ruleShow() {

		$modules = M('auth_modules') -> select();
		$this -> modules = $modules;
		$this -> display('ruleShow');
	}

	//修改权限
	public function editRuleView() {
		$id = I('id', 0, 'intval');

		$rule = M('auth_rule') -> where(array('id' => $id)) -> find();
		$modules = M('auth_modules') -> select();
		$this -> modules = $modules;
		$this -> role = $rule;
		$this -> id = $id;

		$this -> display('eidtRule');
	}

	//处理修改权限
	public function runEditRule() {

		$data = array('title' => I('ruleTitle'), 'name' => I('ruleName'), 'type' => 1, 'status' => I('status'), 'condition' => I('condition'), 'mid' => I('mid'));
		if ( M('auth_rule') -> where(array('id' => I('id'))) -> save($data)) {
			yes('修改成功!', true, 'Auth-ruleIndex');
		} else {
			error('修改失败！');
		}

	}

	//删除权限
	public function delRule() {
		$id = I('id', 0, 'intval');
		$batchFlag = intval($_GET['batchFlag']);
		//批量删除
		if ($batchFlag) {
			$this -> delRuleAll();
			return;
		}
		if ( M('auth_rule') -> where(array('id' => $id)) -> delete()) {
			yes('删除权限成功!');
		} else {
			error('删除权限失败');
		}
	}

	//指量删除用户处理
	public function delRuleAll() {
		$id = I('get.id');
		$idArr = explode(',', $id);
		if (empty($idArr) || $idArr[0] == 0) {
			error('请选择要删除的行');
		}
		if ( M('auth_rule') -> where(array('id' => array('in', $idArr))) -> delete()) {
			yes('删除权限成功!');
		} else {
			error('删除权限失败');
		}
	}

	//角色组列表
	public function groupIndex() {
		$keyword = I('keyword', '', 'trim');
		$where = '';
		if (empty($keyword)) {
			$where = array('id' => array('GT', '0'));
		} else {
			$where = array('title' => array('like', "%$keyword%"));
		}
		$m = M('auth_group');
		$pageSize = I('pageSize', 15, 'intval');
		$pageCurrent = I('pageCurrent', 1, 'intval');
		$count = $m -> where($where) -> count();
		$startCount = ($pageCurrent - 1) * $pageSize;
		$page = new \Think\Page($count, $pageSize);
		$limit = $startCount . ',' . $pageSize;
		$data = $m -> where($where) -> limit($limit) -> order('id desc') -> select();
		$this -> assign('page', $show);
		$this -> assign('role', $data);
		$this -> totalRows = $page -> totalRows;
		$this -> nowPage = $page -> nowPage;
		$this -> display('groupIndex');
	}

	//显示添加角色页面
	public function groupShow() {

		$this -> display('groupShow');

	}

	//添加角色
	public function groupAddHandle() {
		$data['title'] = I('name');
		//角色名称
		$data['describe'] = I('remark');
		//角色描述
		$data['status'] = I('status');
		//角色状态
		$m = M("auth_group");
		if ($m -> add($data)) {
			yes('添加成功', true, 'Auth-groupIndex');
		} else {
			error('添加失败');
		}
	}

	//显示修改角色页面
	public function editGroupView() {
		$id = I('id', 0, intval);
		$group = M('auth_group') -> where(array('id' => $id)) -> find();
		$this -> id = $id;
		$this -> group = $group;

		$this -> display('editGroup');

	}

	//修改角色
	public function editGroupHandle() {
		$id = I('id', 0, intval);
		$data['title'] = I('name');
		//角色名称
		$data['describe'] = I('remark');
		//角色描述
		$data['status'] = I('status');
		//角色状态
		$m = M("auth_group");
		if ($m -> where(array('id' => $id)) -> save($data)) {
			yes('修改成功', true, 'Auth-groupIndex');
		} else {
			error('修改失败');
		}
	}

	//删除角色
	public function delGroupHandle() {
		$id = I('id', 0, intval);
		$num = M('auth_group_access') -> where(array('group_id' => $id)) -> find();
		if ($num) {
			error('该用户组里面有会员，请先处理该用户组下面的会员再操作！');
		}
		$m = M("auth_group");
		$batchFlag = isset($_GET['batchFlag']) ? intval($_GET['batchFlag']) : 0;
		//批量删除
		if ($batchFlag) {
			$this -> delBatch();
			return;
		}
		if ($m -> where(array('id' => $id)) -> delete()) {
			yes('删除成功');
		} else {
			error('删除失败');
		}
	}

	//批量删除角色
	public function delBatch() {
		$id = I('get.id');
		$idArr = explode(',', $id);
		if (empty($idArr) || $idArr[0] == 0) {
			error('请选择要删除的行');
		}
		$where = array('id' => array('in', $idArr));
		$num = M('auth_group_access') -> where(array('group_id' => array('in', $idArr))) -> find();
		if ($num) {
			error('该用户组里面有会员，请先处理该用户组下面的会员再操作！');
		}
		if ( M('auth_group') -> where($where) -> delete()) {
			yes('删除成功');
		} else {
			error('删除失败');
		}
	}

	public function module() {
		$modules = M('auth_modules') -> select();
		$this -> modules = $modules;
		$this -> display();
	}

	public function moduleView() {
		$id = I('id');
		$action = I('action');
		$module = M('auth_modules') -> where(array('id' => $id)) -> find();
		$this -> module = $module;
		$this -> action = $action;
		$this -> display('moduleview');
	}

	public function moduleHandle() {
		$action = I('action');
		$name = I('name');
		if (empty($name)) {
			error('组名不能为空！');
		}
		if ($action == "edit") {
			$id = I('id');
			$data['moduleName'] = $name;
			if ( M('auth_modules') -> where(array('id' => $id)) -> save($data)) {
				yes('修改成功！', true, 'Auth-module');
			} else {
				error('修改失败！');
			}

		} else {

			$data['moduleName'] = $name;
			if ( M('auth_modules') -> add($data)) {
				yes('添加成功！', true, 'Auth-module');
			} else {
				error('添加失败！');
			}

		}
	}

	public function moduleDel() {
		$id = I('id');
		if (!$id) {
			error('参数错误！');
		}
		$isrules = M('auth_rule') -> where(array('mid' => $id)) -> select();
		if ($isrules) {
			error('改组下面存在规则！');
		}

		if ( M('auth_modules') -> where(array('id' => $id)) -> delete()) {
			yes('删除成功！');
		} else {
			error('删除失败！');
		}
	}

	public function accessCate() {
		if (IS_POST) {
			$cids = I('post.id');
			$gid = I('post.group_id');
			if (addToExtend($gid, $cids, 1)) {
				yes('添加成功！');
			} else {
				error('添加失败！');
			}
		} else {
			$gid = I('get.id');
			//p($gid);
			//$auth_group= M('auth_group')->where(array('status'=>array('egt','0'),'module'=>'admin','type'=>1))->getfield('id,title,rules');
			$authed_group = getCategoryOfGroup($gid, 1);
			//p($auth_group[$gid]);
			$cate = D('CategoryView') -> where(array('type' => 0)) -> order('category.sort') -> select();
			$Category = new \Think\Category();
			$this -> cate = $Category::unlimitedForLevel($cate, '&nbsp;&nbsp;&nbsp;&nbsp;', 0);
			//$this->assign('authed_group',   implode(',',(array)$authed_group));
			$this -> authed_group = $authed_group;
			//$this->assign('auth_group',     $auth_group);
			//$this->assign('this_group',     $auth_group[(int)$_GET['group_id']]);
			$this -> id = $gid;
			$this -> display('accessCate');
		}
	}

	//JS检测是否已存在用户组名称
	public function checkRoleName() {
		$name = I('name', '', 'trim');
		$id = I('id', 0, 'intval');
		if (empty($name)) {
			exit(0);
		}
		$data = M('auth_group') -> where(array('title' => $name, 'id' => array('neq', $id))) -> find();
		if ($data) {
			echo 1;
		} else {
			echo 0;
		}
	}

	public function checkAuthName() {
		$name = I('name', '', 'trim');
		$condition = I('condition', '', 'trim');
		$id = I('id', 0, 'intval');
		if (empty($name)) {
			exit(0);
		}
		$data = M('auth_rule') -> where(array('name' => $name, 'id' => array('neq', $id), 'condition' => $condition)) -> find();
		if ($data) {
			echo 1;
		} else {
			echo 0;
		}
	}

	//js 用户名
	public function checkusername() {
		$username = I('username', '', 'trim');
		if (empty($username)) {
			exit(0);
		}
		$user = M('members') -> where(array('username' => $username)) -> find();
		if ($user) {
			echo 1;
		} else {
			echo 0;
		}
	}

}
