<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class SiteController extends CommonController {

    public function setSite(){
        $this->styleDirList = getFileFolderList(APP_PATH.C('DEFAULT_MODULE').'/View/' , 1);

        $this->display('index');
    }

    public function siteInfo(){
        if (IS_POST) {

            if (writeArr($_POST,CONF_PATH.'config.site.php')) {
                yes();
            }else{
                $this->error('修改失败！',U('setSite'));
            }
        }else{
            exit;
        }
    }

    public function pay(){        
        if (IS_POST) {
            if (writeArr($_POST,CONF_PATH.'config.pay.php')) {
                //$this->success('修改成功！',U('Site/pay'));
                yes();
            }else{
                $this->error('修改失败！',U('Site/pay'));
            }
        }else{
            $this->display();
        }
    }


    //添加修改幻灯片视图
    public function showPic(){
        $this->display('isding_show');
    }

    public function sitePics(){
        if (IS_POST) {
            if (writeArr($_POST,CONF_PATH.'pics.php')) {
                yes();
            }else{
                error();
            }
        }else{
            exit;
        }
    }

    public function showFlink(){

        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count = M('flinks')->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $list = M('flinks')->order('id desc')->limit($limit)->select();

        $this->page = $page->show();
        $this->vlist = $list;
        $this->type = '友情连接列表';
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->display('isding_flink');
    }

    public function addlinkShow(){
        $actionName = strtolower($Think . CONTROLLER_NAME);
        
        $this->display('link_add');
    }

    public function addHandle() {

        if (!IS_POST) {
            exit();
        }
        $name = I('title', '', 'trim');
        $url = I('url', '', 'trim');
        $pic = I('logo', '', 'trim');
        if (empty($name) || empty($url)) {
            error('网站名称或网址不能为空');
        }
        $data = array(
            'title'      => $name,
            'url'       => $url,
            'logo'      => $pic,
            'description' => I('description', ''),
            'ischeck'   => I('ischeck', 0, 'intval'),
            'sort'      => I('sort', 0, 'intval'),
            'posttime'  => time(),
        );

        if($id = M('flinks')->add($data)) {
            //更新上传附件表
            if (!empty($pic)) {
                $attid = M('attachment')->where(array('filepath' => $pic))->getField('id');
                if($attid){
                    M('attachmentindex')->add(array('attid' => $attid,'arcid' => $id, 'modelid' => 0, 'desc' => 'link'));
                }
            }
            yes('添加成功！',true,'Site-showFlink');
        }else {
            error('添加失败!');
        }
    }


    //编辑文章
    public function linkEdit() {
        //当前控制器名称
        $id = I('id', 0, 'intval');
        $this->vo = M('flinks')->find($id);
        $this->display('link_edit');
    }

    //修改友情链接
    public function editHandle() {
        if (!IS_POST) {
            exit();
        }
        $title = I('title', '', 'trim');
        $url = I('url', '', 'trim');
        $pic = I('logo', '', 'trim');
        $id = I('id', 0, 'intval');
        if (empty($title) || empty($url)) {
            $this->error('网站名称或网址不能为空');
        }
        //print_r($_POST);die;
        if (false !== M('flinks')->save($_POST)) {
            M('attachmentindex')->where(array('arcid' => $id, 'modelid' => 0, 'desc' => 'link'))->delete();
            //更新上传附件表
            if (!empty($pic)) {
                $attid = M('attachment')->where(array('filepath' => $pic))->getField('id');
                if($attid){
                    M('attachmentindex')->add(array('attid' => $attid,'arcid' => $id, 'modelid' => 0, 'desc' => 'link'));
                }
            }
            yes('修改成功！',true,'Site-showFlink');
        }else {
            error('修改失败');
        }
        
    }

    //删除友情链接
    public function del() {

        $id = I('id',0 , 'intval');
        $batchFlag = isset($_GET['batchFlag'])? intval($_GET['batchFlag']) : 0;
        //批量删除
        if ($batchFlag) {
            $this->delBatch();
            return;
        }
        if (M('flinks')->delete($id)) {           
            M('attachmentindex')->where(array('arcid' => $id, 'modelid' => 0, 'desc' => 'link'))->delete();
            yes('彻底删除成功');
        }else {
            error('彻底删除失败');
        }
    }


    //批量删除友情链接
    public function delBatch() {

        $idArr = I('key',0 , 'intval');     

        if (!is_array($idArr)) {
            $this->error('请选择要彻底删除的项');
        }
        $where = array('Id' => array('in', $idArr));

        if (M('flinks')->where($where)->delete()) {
            M('attachmentindex')->where(array('arcid' => array('in', $idArr), 'modelid' => 0, 'desc' => 'link'))->delete();
            $this->success('彻底删除成功', U('Site/showFlink'));
        }else {
            $this->error('彻底删除失败');
        }
    }



    /*public function addFlink(){
        if (IS_POST) {
            if (M('flinks')->data($_POST)->add()) {
                $this->success('添加成功！');
            }else{
                $this->error('添加失败！');
            }
        }else{
            exit;
        }
    }

    public function runFlink(){
        if (IS_POST) {
            $data = array(
                'title' => I('post.title'),
                'url' => I('post.url'), 
                'sort' => I('post.sort'),  
                );

            if (M('flinks')->where(array('Id'=>I('post.Id')))->save($_POST)) {
                $this->success('修改成功！');
            }else{
                $this->error('修改失败！');
            }
        }else{
            exit;
        }
    }

    public function delFlink(){
        if(M('flinks')->where(array('Id'=>I('get.Id')))->delete()){
            $this->success('删除成功！');
        }else{
            $this->error('删除失败！');
        }
    }
*/
    /*
    上传图片
     */
    public function upload() {
        //3.2上传
        if (IS_POST) {
            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize = 3145728 ;// 设置附件上传大小
            $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
            //$upload->rootPath = './'.C('ROOTPATH'); // 设置附件上传根目录
            $upload->savePath = 'image/'; // 设置附件上传目录
            // 上传文件
            $info = $upload->upload();
            if(!$info) {// 上传错误提示错误信息
                $this->error($upload->getError());
                exit;
            }else{// 上传成功 获取上传文件信息
                $db=M('attachment');
                foreach($info as $file){
                        //入库附件表格
                        $savepath = $file['savepath'];                        
                        $data['filepath'] = ltrim(C('ROOTPATH'), ".").$savepath.$file['savename'];  
                        $data['title'] = $file['name'];  
                        $data['haslitpic'] = empty($file['haslitpic']) ? 0 : 1;
                        $filetype =1;
                        //后缀
                        switch ($file['ext']) {
                            case 'gif':
                                $filetype =1;
                                break;
                            case 'jpg':
                                $filetype =1;
                                break;
                            case 'png':
                                $filetype =1;
                                break;
                            case 'bmp':
                                $filetype =1;
                                break;
                            case 'swf'://flash
                                $filetype =2;
                                break;
                            case 'mp3'://音乐
                                $filetype =3;
                                break;
                            case 'wav':
                                $filetype =3;
                                break;
                            case 'rm'://电影
                                $filetype =4;
                                break;

                            case 'doc'://
                                $filetype =5;
                                break;
                            case 'docx'://
                                $filetype =5;
                                break;
                            case 'xls'://
                                $filetype =5;
                                break;
                            case 'ppt'://
                                $filetype =5;
                                break;
                            case 'zip'://
                                $filetype =6;
                                break;
                            case 'rar'://
                                $filetype =6;
                                break;
                            case '7z'://
                                $filetype =6;
                                break;
                            
                            default://其他
                                $filetype = 0;
                                break;
                           }   
                        $data['filetype'] = $filetype;
                        $data['filesize'] = $file['size'];
                        $data['uploadtime'] = time();
                        $data['aid'] = $_SESSION['aid'];//管理员ID                        
                        $db->add($data);
                        //入库附件表结束
                    
                    echo json_encode(array('status'=>1,'path'=>ltrim(C('ROOTPATH'), ".").$file['savepath'].$file['savename'],'title'=>$file['name'],'size'=>$file['size']));
                    
                }
                exit;
            }
        }else{
            exit;
        }
    }

    /*
    上传文件
     */
    public function uploadDocs() {
        //3.2上传
        if (IS_POST) {
            $dir = I('get.dir');
            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize = 0 ;// 设置附件上传大小
            $upload->exts = array('doc', 'docx', 'xls', 'ppt', 'zip', 'rar', '7z');// 设置附件上传类型
            //$upload->rootPath = C('DOC_SAVE_PATH'); // 设置附件上传根目录
            $upload->savePath = $dir . '/'; // 设置附件上传目录

            // 上传文件
            $info = $upload->upload();
            if(!$info) {// 上传错误提示错误信息
                $this->error($upload->getError());
                exit;
            }else{// 上传成功 获取上传文件信息
                $db=M('attachment');
                foreach($info as $file){
                        //入库附件表格
                        $savepath = $file['savepath'];                        
                        $data['filepath'] = ltrim(C('DOC_SAVE_PATH'), ".").$savepath.$file['savename'];  
                        $data['title'] = $file['name'];  
                        $data['haslitpic'] = empty($file['haslitpic']) ? 0 : 1;
                        $filetype =1;
                        //后缀
                        switch ($file['ext']) {
                            case 'gif':
                                $filetype =1;
                                break;
                            case 'jpg':
                                $filetype =1;
                                break;
                            case 'png':
                                $filetype =1;
                                break;
                            case 'bmp':
                                $filetype =1;
                                break;
                            case 'swf'://flash
                                $filetype =2;
                                break;
                            case 'mp3'://音乐
                                $filetype =3;
                                break;
                            case 'wav':
                                $filetype =3;
                                break;
                            case 'rm'://电影
                                $filetype =4;
                                break;

                            case 'doc'://
                                $filetype =5;
                                break;
                            case 'docx'://
                                $filetype =5;
                                break;
                            case 'xls'://
                                $filetype =5;
                                break;
                            case 'ppt'://
                                $filetype =5;
                                break;
                            case 'zip'://
                                $filetype =6;
                                break;
                            case 'rar'://
                                $filetype =6;
                                break;
                            case '7z'://
                                $filetype =6;
                                break;
                            
                            default://其他
                                $filetype = 0;
                                break;
                           }   
                        $data['filetype'] = $filetype;
                        $data['filesize'] = $file['size'];
                        $data['uploadtime'] = time();
                        $data['aid'] = $_SESSION['aid'];//管理员ID                        
                        $db->add($data);
                        //入库附件表结束
                    
                    echo json_encode(array('status'=>1,'path'=>C('ROOTPATH').$file['savepath'].$file['savename'],'title'=>$file['name'],'size'=>round($file['size']/1024,2)));
                    
                }
                exit;
            }
        }else{
            exit;
        }
    }

    /*
    首页显示
     */
    public function main(){
        $mysql_ver = M()->query('SELECT VERSION();');
        if(is_array($mysql_ver)) {
            $this->mysql_ver = $mysql_ver[0]['VERSION()'];
        }else {
            $this->mysql_ver = '';
        }
        $this->environment_upload = ini_get('file_uploads') ? ini_get('upload_max_filesize') : '不支持';
        $this->idebug = APP_DEBUG ? '是':'否';
        $this->icgi = IS_CGI ? '是':'否';
        $this->display();
    }





      
}