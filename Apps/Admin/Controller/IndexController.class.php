<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class IndexController extends Controller {

    //显示登录页面视图
    public function index(){
         if (!empty($_SESSION['aid'])) {
            redirect(U('Admin/Main/index'));
        }
        $this->display();
    }

    //处理登录
    public function login(){
        if (IS_POST) {
            $username = trim(I('post.username'));
            $password = trim(I('post.passwordhash'));
            $verify = I('post.j_captcha');
            if (empty($username)|| empty($password)) {
                $this->error('亲，用户名或密码都不能为空滴！');
            }
            $where = array('username' =>$username);
            $db=M('members');
            $user=$db->where($where)->find();
            if (get_password($password, $user['encrypt']) != $user['password'] || empty($user)) 
            {                
                $this->error('用户名或密码错误！');             
            }
            if(!check_verify($verify,1)){
                $this->error('验证码错误！');
            }
            if($user['status']){
                $this->error('帐号被禁止！'); 
            }
            $data = array(
                'loginnum' => $user['loginnum']+1, 
                'lasttime'=>time(),
                'lastip'=>get_client_ip()
                );
            $db->where(array('Id'=>$user['Id']))->save($data);
            session('aid',$user['Id']);
            session('aname',$user['username']);
            //------登录成功发送通知邮件
            if (C('cfg_login_auto')) {
            $email=C('cfg_email_get');
            $subject = "WOWOCMS后台登录通知";
            $login_ip=get_client_ip();
            $login_time=date('Y年m月d日 H:i:s', time());
$message = <<<str
<p>后台用户{$user['username']}于{$login_time} IP：{$login_ip}登录，如非本人登录，敬请做好安全防范！</p>
str;
if (SendMail($email, $subject , $message) == true){

}
}
            //------登录成功发送通知邮件
            $this->success('欢迎回来！',U('Admin/Main/index'));        
        }
    }

    //处理注销
    public function loginout(){
         $db=M('members');
         $where = array('Id'=>$_SESSION['aid']);
         $user=$db->where($where)->find();
         $data = array(
                    'pretime'=>$user['lasttime'],
                    'preip'=>$user['lastip']
                    );
        $db->where($where)->save($data);

        session('aid',NULL);
        session('aname',NULL);
        session(NULL);
        $this->redirect('Index/index');

    }

    //显示验证码
    public function get_verify(){
        $Verify = new \Think\Verify();
        //$Verify->codeSet = '0123456789'; 
        $Verify->length   = 5;
        $Verify->entry(1);
    }
}