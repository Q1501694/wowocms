<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
class MemberController extends CommonController {

    public function index(){
        $keyword = I('keyword', '', 'htmlspecialchars,trim');
        if (!empty($keyword)) {
            $where['member.nickname'] = array('LIKE', "%{$keyword}%");
        }
        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count = D('MemberView') -> where($where) -> count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $members = D('MemberView') -> where($where) -> order('member.id DESC') -> limit($limit) -> select();        
        $this -> keyword = $keyword;
        $this -> page = $page -> show();
        $this -> vlist = $members;
        $this -> type = '会员列表';
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this -> display();
    }

    public function add(){
        $where['status']=array('neq',0);
        $membergroup=M('membergroup')->where($where)->order('sort')->select();
        $this->membergroup=$membergroup;
        $this->display();
    }

    public function addHandle(){
        if (!IS_POST) {
            exit;
        }
        $email=I('post.email','');
        $password=I('post.pwd','');
        $rpassword=I('post.rpwd','');
        $qq=I('post.qq','');
        $phone=I('post.phone','');
        $realname=I('post.realname','');
        $nickname=I('post.nickname','');
        $groupid=I('post.membergroup','');
        $sex=I('post.sex','');

        if (empty($email) || empty($password) || empty($rpassword) || empty($nickname)) {
            error('邮箱，密码，确认密码，昵称均不能为空！');
        }

        if (strlen($password)<6) {
            error('密码不能小于6位！');
        }

        if ($password!=$rpassword) {
            error('密码和确认密码不一致！');
        }

        $db=M('member');
        $id=$db->where(array('email'=>$email))->getField('id');
        if ($id) {
            error('此邮箱已被占用！');
        }
        $notallowname = explode(',', C('cfg_member_notallow'));
        if (in_array($nickname, $notallowname)) {
            error('此昵称系统禁用，请重新更换一个！');
        }
        $data['groupid']=$groupid;
        $data['email'] = $email;
        $data['nickname'] = $nickname;
        $data['status']=1;
        $data['islock']=0;
        $data['regtime'] = time();
        $passwordinfo = I('pwd', '','get_password');
        $data['password'] = $passwordinfo['password'];
        $data['encrypt'] = $passwordinfo['encrypt'];
        if ($uid=$db->add($data)) {
            $details['userid']=$uid;
            $details['realname']=$realname;
            $details['qq']=$qq;
            $details['phone']=$phone;
            $details['sex']=$sex;
            $details['birthday']=time();
            if (M('memberdetail')->add($details)) {
                yes('添加会员成功！',true,'Member-index');
            }
        }else{  
            error('添加会员失败！');
        }
    }

    public function edit(){

        $uid=I('id','','intval');

        if (empty($uid)) {
            $this->error('非法操作！');
        }

        $user=D('MemberView')->where(array('id'=>$uid))->find();

        
        $this->membergroup=M('membergroup')->select();
        $this->user=$user;

        $this->display();
    }

    public function editHandle(){

        if (!IS_POST) {
            exit;
        }
        $uid=I('id','','intval');
        if (!$uid) {
            error('非法提交！');
        }
        $nickname=I('post.nickname','');
        $password=I('post.pwd','');

        $notallowname = explode(',', C('cfg_member_notallow'));
        if (in_array($nickname, $notallowname)) {
            error('此昵称系统禁用，请重新更换一个！');
        }
        if (!empty($password)) {
            $passwordinfo = I('pwd', '','get_password');
            $data['password'] = $passwordinfo['password'];
            $data['encrypt'] = $passwordinfo['encrypt'];           
        }
        $data['nickname']=I('nickname');
        $data['amount']=I('amount');
        $data['score']=I('score');        
        $data['groupid']=I('membergroup');
        $data['status']=I('status');
        $data['islock']=I('islock');
        $data['face']=I('face');

        $detail['qq']=I('qq');
        $detail['phone']=I('phone');
        $detail['sex']=I('sex');
        $detail['realname']=I('post.realname');

        $num=M('member')->where(array('id'=>$uid))->save($data);
        $mm=M('memberdetail')->where(array('userid'=>$uid))->save($detail);

        if ( $num || $mm) {
            yes('修改成功！',true,'Member-index');
        }else{
            error('修改失败！');
        }



    }

    public function del(){
        $uid=I('id',0,'intval');
        if ($uid==0) {
            error('参数错误！');
        }
        $num=M('member')->where(array('id'=>$uid))->delete();
        $mm=M('memberdetail')->where(array('userid'=>$uid))->delete();
        if ($num || $mm) {
            yes('删除成功！');
        }else{
            error('删除失败！');
        }
    }

    public function group(){

        $keyword=I('keyword','','htmlspecialchars,trim');
        if (!empty($keyword)) {
            $where['name'] = array('LIKE', "%{$keyword}%");
        }
        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count=M('membergroup')->where($where)->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $groups=M('membergroup')->where($where)->order('sort')->limit($limit)->select();

        $this->groups=$groups;
        $this -> keyword = $keyword;
        $this -> page = $page -> show();
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->display();
    }

    public function groupHandle(){
        
        $action = I('action');
        if ($action =="edit") {
            $this->action="edit";
            $this->group=M('membergroup')->where(array('id'=>I('id')))->find();
        }
        if (IS_POST) {            
            if ($action == "edit") {

                $id=I('id');
                $data['name']=I('name');
                $data['description']=I('description');
                $data['status']=I('status');
                $data['sort']=I('sort');

                $num=M('membergroup')->where(array('id'=>$id))->save($data);
                if ($num) {
                    yes('修改成功！',true,'Member-group');
                }else{
                    error('修改失败！');
                }

            }else{
                $data['name']=I('name');
                $data['description']=I('description');
                $data['status']=I('status');
                $data['sort']=I('sort');

                $nnum=M('membergroup')->add($data);
                if ($nnum) {
                    yes('添加成功！',true,'Member-group');
                }else{
                    error('添加失败！');
                }

            }
        }
        //print_r($action);
        $this->display('handle');
    }

    public function accessSet(){
        echo "完善中。。。。。";
    }

    public function groupDel(){
        $gid=I('id','','intval');

        $users=M('member')->where(array('groupid'=>$gid))->field('id')->select();
        if (!empty($users)) {
            error('该会员组有会员！');
        }
        $num=M('membergroup')->where(array('id'=>$gid))->delete();
        if ($num) {
            yes('删除成功！');
        }else{
            error('删除失败！');
        }
    }

    public function comment(){
        $pageSize = I('pageSize', 15, 'intval');
        $pageCurrent = I('pageCurrent', 1, 'intval');
        $count=M('comment')->count();
        $startCount = ($pageCurrent - 1) * $pageSize;
        $page = new \Think\Page($count, $pageSize);
        $limit = $startCount . ',' . $pageSize;
        $comments = M('comment') -> order('posttime DESC') -> limit($limit) -> select();        
        
        $this -> page = $page -> show();
        $this -> vlist = $comments;
        $this -> type = '评论列表';
        $this->totalRows = $page->totalRows;
        $this->nowPage = $page->nowPage;
        $this->display();


    }

    public function lookComment(){
        $id=I('id');
        $comment=M('comment')->where(array('id'=>$id))->field('content,status')->find();
        
        $this->id=$id;
        $this->comment=$comment;
        $this->display('lookcomment');

    }

    public function commentHandle(){
        if (!IS_POST) {
            exit;
        }
        $id=I('id');
        $data['status']=I('status');
        $num=M('comment')->where(array('id'=>$id))->save($data);
        if ($num) {
            //$this->success('修改成功！');
            yes('修改成功！',true,'Member-comment');
        }else{
            error('修改失败！');
        }
    }

    public function commentDel(){
        $id=I('id');       
        $batchFlag = I('get.batchFlag', 0, 'intval');
        //批量删除
        if ($batchFlag) {
            $this -> delBatch();
            return;
        }
        $num=M('comment')->where(array('id'=>$id))->delete();
        if ($num) {
            yes('删除成功！');
        }else{
            error('删除失败！');
        }
    }

    //批量删除到回收站
    public function delBatch() {
        $id = I('get.id');
        $idArr = explode(',', $id);
        if (empty($idArr)||$idArr[0]==0) {
            error('请选择要删除的行');
        }
        if (M('comment') -> where(array('id' => array('in', $idArr))) -> delete()){
                yes('删除成功！');
        }else{
            error('删除失败！');
        }   
    }

    public function checkUserEmail(){
        $db=M('member');
        $email=I('post.email');
        $count=$db->where(array('email'=>$email))->count();
        if (!$count) {
            echo json_encode(array('ok'=>'OK'));
        }else{
            echo json_encode(array('error'=>'邮箱已被占用'));
        }
        exit;
    }
}
