<?php
return array(
    //关闭静态缓存
    'HTML_CACHE_ON' => false,
    'DEFAULT_THEME' => 'UI',
    'LOAD_EXT_CONFIG' => 'config.wechat,config.wechatauth',
    'DEFAULT_FILTER' => 'htmlspecialchars,trim',
    'USER_SQL_FILESIZE' => 5242880,
    'DATA_BACKUP_PATH'=>'./Data/backupdata/',
    'DATA_BACKUP_PART_SIZE'=>20971520,
    'DATA_BACKUP_COMPRESS'=>1,
    'DATA_BACKUP_COMPRESS_LEVEL'=>9,
    'AUTH_ISALLOW_URL'=>array(
		'admin/main/index',
		'admin/category/getparentcate',
		'admin/site/main',
		'admin/person/pwd',
		'admin/index/index',
		'admin/category/getparentcate',
		'admin/attachment/uploadplus',
		'admin/attachment/filemanager',
		'admin/attachment/uploads'
	),










);