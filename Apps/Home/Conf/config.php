<?php
return array(
	//'配置项'=>'配置值'
   // 预先加载的标签库
    'TAGLIB_BUILD_IN' => 'Cx,Wowocms',
    'TAGLIB_PRE_LOAD' => 'wowocms',
    'DEFAULT_FILTER' => 'htmlspecialchars,strip_tags,stripslashes,trim',
    'DEFAULT_THEME' => C('cfg_theme'),
        //开启静态缓存
    'HTML_CACHE_ON' => C('HTML_CACHE_ON__INDEX'),
    'LOAD_EXT_CONFIG' => 'config.wechat,config.wechatauth',
    //'TMPL_DETECT_THEME'=>true,
    //'THEME_LIST'=>'blue004,chaye003,wuliu009,tuliao008,chaye007,zhuangshi005,tongxin006',
);