<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Home\Model;
use Think\Model\ViewModel;
//视图模型
class MapViewModel extends ViewModel {


     public $viewFields = array(
     'Article'=>array('id','title','publishtime'),
     'Category'=>array('id','name'=>'category_name','ename', '_on'=>'Article.cid=Category.id'),
     'User'=>array('name'=>'username', '_on'=>'Blog.user_id=User.id'),
   );

    '_table'=>"__Article__";




}

?>