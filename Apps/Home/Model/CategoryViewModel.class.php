<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Home\Model;
use Think\Model\ViewModel;
//视图模型
class CategoryViewModel extends ViewModel {
    
    protected $viewFields = array(
        'category' => array('id', 'name', 'ename', 'pid', 'type', 'seotitle', 'keywords', 'description', 'modelid','nav',
        'template_category', 'template_list', 'template_show', 'status', 'sort',
        '_type' => 'LEFT'
        ),
        'model' => array(
        'name' => 'modelname',//显示字段name as model
        'tablename' => 'tablename',//显示字段name as model
        '_on' => 'category.modelid = model.id',//_on 对应上面LEFT关联条件
        ),

    );
}

?>