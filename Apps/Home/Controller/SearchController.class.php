<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com><http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Home\Controller;
use Think\Controller;
class SearchController extends Controller {
    public function index(){
        $mid = I('mid', 23,'intval'); //模型ID
        $keyword = I('keyword', '', 'htmlspecialchars,trim,strip_tags');//关键字
        if($keyword == '请输入关键词') $keyword = '';
        if (!empty($keyword)) {
            $cids=M('category')->where(array('modelid'=>$mid,'type'=>0))->field('id')->select();
            $cids=arrToStr($cids);
            $where = array(
                'title' => array('LIKE', '%'.$keyword.'%'),
                'cid'=>array('IN',$cids)
            );

            //通过模型ID获取模型表
            $model_table=M('model')->where(array('id'=>$mid))->getField('tablename');
            $count=M($model_table)->where($where)->count();
            $page = new \Think\Page($count, 10);
            $limit = $page -> firstRow . ',' . $page -> listRows;
            $vlist=M($model_table)->where($where)->limit($limit) -> select();
        }else {
            //$page = '';
            //$vlist = array();
            //直接返回，减少流程
            $this->error('请输入关键字');
            
        }
        if (empty($vlist)) {
            $vlist = array();
        }
        foreach ($vlist as $k => $v) {//处理跳转链接
            if (isset($v['flag'])) {
                $_jumpflag = ($v['flag'] & B_JUMP) == B_JUMP? true : false;
                $_jumpurl = $v['jumpurl'];
            }else {
                $_jumpflag = false;
                $_jumpurl = '';
            }           
            $ename=getCateNameAndEnameById($v['cid']);
            $v['ename']=$ename['ename'];
            $vlist[$k]['url'] = getContentUrl($v['id'], $v['cid'], $v['ename'], $_jumpflag, $_jumpurl);
        }
        if (empty($keyword)) {
            $title = '搜索中心';    
        }else {         
            $title = $keyword.'_搜索中心';  
        }
        //p($vlist);
        $this->title = $title;
        $this->keyword = $keyword;
        $this->searchurl = U('Search/index');
        $this->vlist = $vlist;  
        $this->page = $page->show();    
        $this->modelid = $mid;  
        $this->display();
    }
}
?>