<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function  index(){
            goMobile();
            $this->display();
        
       
    }
    //显示验证码
    public function get_verify(){
        $Verify = new \Think\Verify();
        $Verify->codeSet = '0123456789'; 
        $Verify->length   = 3;
        $Verify->entry(1);
    }
}