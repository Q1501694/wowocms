<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Home\Controller;
use Think\Controller;
class GoController extends Controller {

	public function index() {

		$url = I('url', 0, '');
		if (!empty($url)) {
			redirect($url);
		}

	}

	public function link() {

		$url = I('url', 0, '');
		if (!empty($url)) {
			$url = base64_decode($url);
			redirect($url);
		}

	}

}
?>