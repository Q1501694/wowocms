<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace Home\Controller;
use Think\Controller;
class ListController extends Controller {

    public function index(){
        $cid = I('cid', 0,'intval');
        $ename = I('e', '', 'htmlspecialchars,trim');
        $cate = getCategory(1);
        $Category = new \Think\Category();
        if (!empty($ename)) {//ename不为空
            $self = $Category::getSelfByEName($cate, $ename);//当前栏目信息
        }else {//$cid来判断
            $self = $Category::getSelf($cate, $cid);//当前栏目信息
        }       
        if(empty($self)) {
            $this->error('栏目不存在');
        }
        $cid = $self['id'];//当使用ename获取的时候，就要重新给$cid赋值，不然0
        $_GET['cid'] = $cid;//栏目ID
        $self['url'] = getUrl($self);
        $this->cate = $self;
        $this->flag_son = $Category::hasChild($cate, $cid);//是否包含子类
        $this->title = empty($self['seotitle']) ? $self['name'] : $self['seotitle'];
        $this->keywords = $self['keywords'];
        $this->description = $self['description'];
        $this->cid = $cid;
        $patterns = array('/.html$/');
        $replacements = array('', '');
        $template_list = preg_replace($patterns, $replacements, $self['template_list']);
        if (empty($template_list)) {
            $this->error('模板不存在');
        }
        //print_r($template_list);
        switch ($self['tablename']) { 
            case 'page':
                {
                    $cate = M('Category')->Field('content')->find($cid);
                    $self['content'] = $cate['content'];
                    $this->cate = $self;
                    $this->display($template_list);
                }
                return;
                break;     
            default:
                $this->display($template_list);
                return;
                break;
        }
        $this->display();
    }
}