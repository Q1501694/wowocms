<?php
function getPageDes($typeid, $len = 0) {
	$des = M('category') -> where(array('modelid' => 4, 'id' => $typeid)) -> getField('description');
	if ($len != 0) {
		$des = str2sub($des, $len);
	}
	return $des;
}

function changeData($data) {
	$contents = array();
	foreach ($data as $key => $value) {
		foreach ($value as $k => $v) {
			if ($k == 'content') {
				$k = 1;
			}
			if ($k == 'url') {
				$k = 2;
			}
			if ($k == 'picurl') {
				$k = 3;
			}
			if ($k == 'title') {
				$k = 0;
			}
			if ($k == 3) {
				$v = C('cfg_weburl') . $v;
			}
			$contents[$key][$k] = $v;
		}
	}
	return $contents;
}

function ishaveson($cid) {
	$cate = getCategory(1);
	$Category = new \Think\Category();
	$flag_son = $Category::hasChild($cate, $cid);
	return $flag_son;
}

/**
 * 获取父类ID用于高显
 */
function get_parentid($id) {
	$cate = getCategory(1);
	$category = new \Think\Category();
	$ids = $category::getParents($cate, $id);
	return $ids[0]['id'];
}
?>