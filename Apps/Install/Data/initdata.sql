/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;








INSERT INTO `wowocms_attachment` VALUES (1,'201583121910953_2105456649.png','/Uploads/image/2015-09-01/55e5230c5acd3.png',1,21036,0,1441080076,1,0);


INSERT INTO `wowocms_auth_extend` VALUES (4,73,1);
INSERT INTO `wowocms_auth_extend` VALUES (4,72,1);
INSERT INTO `wowocms_auth_extend` VALUES (4,64,1);
INSERT INTO `wowocms_auth_extend` VALUES (4,74,1);
INSERT INTO `wowocms_auth_extend` VALUES (4,75,1);
INSERT INTO `wowocms_auth_extend` VALUES (4,2,1);
INSERT INTO `wowocms_auth_extend` VALUES (4,7,1);
INSERT INTO `wowocms_auth_extend` VALUES (10,64,1);
INSERT INTO `wowocms_auth_extend` VALUES (10,72,1);
INSERT INTO `wowocms_auth_extend` VALUES (10,73,1);
INSERT INTO `wowocms_auth_extend` VALUES (10,74,1);
INSERT INTO `wowocms_auth_extend` VALUES (10,75,1);
INSERT INTO `wowocms_auth_extend` VALUES (10,2,1);
INSERT INTO `wowocms_auth_extend` VALUES (10,7,1);

INSERT INTO `wowocms_auth_group` VALUES (1,'超级管理员',1,'12,13,78,79,80,81,82,3,4,5,6,9,20,21,22,23,28,34,55,56,57,58,59,60,61,62,63,64,50,51,52,53,54,83,84,85,86,87,88,89,90,91,7,8,10,11,17,18,19,24,25,26,27,65,66,67,68,69,70,71,72,73,74,75,76,77,1,40,41,42,43,44,45,46,47,48,49,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,35,36,37,38,39,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,14,15,16,29,30,31,32,33,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151','拥有全部权限',1,'admin');
INSERT INTO `wowocms_auth_group` VALUES (3,'后台基本用户组',1,'6,4,5','后台基本用户组',1,'admin');
INSERT INTO `wowocms_auth_group` VALUES (4,'centos',1,'12,13,78,79,80,81,82,50,51,52,53,54,60,61,62,63,64,83,84,85,86,87,88,89,90,91,7,8,9,10,11,17,18,19,20,21,22,23,24,25,26,27,34,65,66,67,68,69,70,71,72,73,74,75,76,77,161,162,163,164,1,40,41,42,43,44,45,46,47,48,49,92,93,94,95,97,98,99,100,101,102,103,104,105,106,165,166,35,36,37,38,39,96,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,14,15,16,28,29,30,31,32,33,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151','centos',1,'admin');
INSERT INTO `wowocms_auth_group` VALUES (5,'演示用户组',1,'12,13,79,4,5,6,50,51,52,54,90,7,8,10,11,17,18,66,71,1,40,41,43,44,45,46,47,48,103,104,36,96,107,108,110,114,115,119,120,121,124,126,14,15,16,30,31,130,132,134,144,146','演示',1,'admin');
INSERT INTO `wowocms_auth_group` VALUES (10,'基础分组',1,'12,13,78,79,80,81,82,50,60','基础分组',1,'admin');


INSERT INTO `wowocms_auth_modules` VALUES (1,'栏目管理',2);
INSERT INTO `wowocms_auth_modules` VALUES (3,'内容管理',3);
INSERT INTO `wowocms_auth_modules` VALUES (4,'站点管理',1);
INSERT INTO `wowocms_auth_modules` VALUES (5,'权限管理',5);
INSERT INTO `wowocms_auth_modules` VALUES (6,'生成管理',4);
INSERT INTO `wowocms_auth_modules` VALUES (7,'会员管理',6);
INSERT INTO `wowocms_auth_modules` VALUES (8,'订单管理',7);
INSERT INTO `wowocms_auth_modules` VALUES (9,'其他操作',8);
INSERT INTO `wowocms_auth_modules` VALUES (10,'微信管理',9);

INSERT INTO `wowocms_auth_rule` VALUES (1,'Admin/Auth/groupIndex','用户组设置',2,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (7,'Admin/Site/setSite','网站设置',2,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (8,'Admin/Model/index','模型设置',2,1,'',4,4);
INSERT INTO `wowocms_auth_rule` VALUES (9,'Admin/Items/index','联动信息列表',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (10,'Admin/Site/showPic','幻灯片设置',2,1,'',4,6);
INSERT INTO `wowocms_auth_rule` VALUES (11,'Admin/Database/index','数据库管理',2,1,'',4,7);
INSERT INTO `wowocms_auth_rule` VALUES (12,'Admin/Category/index','栏目管理',2,1,'',1,1);
INSERT INTO `wowocms_auth_rule` VALUES (13,'Admin/Category/add','栏目添加页面',1,1,'',1,1);
INSERT INTO `wowocms_auth_rule` VALUES (14,'Admin/Site/showFlink','友情链接',2,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (15,'Admin/Site/addlinkShow','友情链接添加页面',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (16,'Admin/Site/linkEdit','友情链接编辑页面',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (17,'Admin/Model/add','模型添加页面',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (18,'Admin/Model/edit','模型修改页面',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (19,'Admin/Model/del','模型删除处理',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (20,'Admin/Items/add','联动信息添加',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (21,'Admin/Items/edit','联动信息修改',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (22,'Admin/Items/del','联动信息删除',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (23,'Admin/Items/delBatch','联动信息批量删除',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (24,'Admin/Database/export','备份数据库',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (25,'Admin/Database/import','还原数据库',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (26,'Admin/Database/optimize','优化数据表',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (27,'Admin/Database/repair','修复数据表',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (28,'Admin/Site/del','友情链接删除处理',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (29,'Admin/Site/delBatch','友情链接批量删除处理',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (30,'Admin/Guestbook/index','留言本管理',2,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (31,'Admin/Guestbook/look','留言本查看内容页面',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (32,'Admin/Guestbook/del','留言板留言删除处理',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (33,'Admin/Guestbook/delBatch','留言板留言批量删除处理',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (34,'Admin/System/url','伪静态设置',2,1,'',4,5);
INSERT INTO `wowocms_auth_rule` VALUES (35,'Admin/System/clearCache','清除缓存',2,1,'',6,1);
INSERT INTO `wowocms_auth_rule` VALUES (36,'Admin/Clearhtml/all','更新全站',2,1,'',6,1);
INSERT INTO `wowocms_auth_rule` VALUES (37,'Admin/Clearhtml/home','更新首页',2,1,'',6,1);
INSERT INTO `wowocms_auth_rule` VALUES (38,'Admin/Clearhtml/lists','更新栏目',2,1,'',6,1);
INSERT INTO `wowocms_auth_rule` VALUES (39,'Admin/Clearhtml/shows','更新文档',2,1,'',6,1);
INSERT INTO `wowocms_auth_rule` VALUES (40,'Admin/Auth/index','管理员设置',2,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (41,'Admin/Auth/addUser','系统管理员添加页面',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (42,'Admin/Auth/delUser','系统管理员删除处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (43,'Admin/Auth/groupShow','用户组添加页面',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (44,'Admin/Auth/accessSet','用户组权限设置页面',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (45,'Admin/Auth/editGroupView','用户组修改页面',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (46,'Admin/Auth/ruleIndex','权限列表',2,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (47,'Admin/Auth/ruleShow','权限添加页面',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (48,'Admin/Auth/editRuleView','权限修改页面',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (49,'Admin/Auth/delRule','权限删除处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (50,'Admin/Article/index','内容管理',2,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (51,'Admin/Article/add','文章添加页面',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (52,'Admin/Article/edit','文章修改页面',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (53,'Admin/Article/del','文章删除到回收站',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (54,'Admin/Article/trach','文章回收站页面',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (60,'Admin/Download/index','文档列表',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (61,'Admin/Download/add','文档添加',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (62,'Admin/Download/edit','文档编辑',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (63,'Admin/Download/del','文档删除',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (64,'Admin/Download/trach','文档回收站',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (65,'Admin/Site/siteInfo','网站设置保存',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (66,'Admin/Site/pay','支付设置',2,1,'',4,2);
INSERT INTO `wowocms_auth_rule` VALUES (67,'Admin/Site/payHandle','支付设置保存',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (68,'Admin/Model/addHandle','模型添加保存',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (69,'Admin/Model/editHandle','模型修改保存',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (70,'Admin/Model/sort','模型排序处理',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (71,'Admin/Itemgroup/index','联动设置',2,1,'',4,3);
INSERT INTO `wowocms_auth_rule` VALUES (72,'Admin/Itemgroup/add','联动添加保存',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (73,'Admin/Itemgroup/edit','联动编辑保存',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (74,'Admin/Itemgroup/del','联动删除处理',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (75,'Admin/Itemgroup/delBatch','联动批量删除',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (76,'Admin/Site/sitePics','幻灯片修改保存',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (77,'Admin/Database/del','数据库备份删除',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (78,'Admin/Category/addHandle','栏目添加保存',1,1,'',1,1);
INSERT INTO `wowocms_auth_rule` VALUES (79,'Admin/Category/edit','栏目修改页面',1,1,'',1,1);
INSERT INTO `wowocms_auth_rule` VALUES (80,'Admin/Category/editHandle','栏目修改保存',1,1,'',1,1);
INSERT INTO `wowocms_auth_rule` VALUES (81,'Admin/Category/del','栏目删除处理',1,1,'',1,1);
INSERT INTO `wowocms_auth_rule` VALUES (82,'Admin/Category/sort','栏目排序处理',1,1,'',1,1);
INSERT INTO `wowocms_auth_rule` VALUES (83,'Admin/Article/addPost','文章添加保存',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (84,'Admin/Article/editPost','文章修改保存',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (85,'Admin/Article/delBatch','文章批量删除到回收站',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (86,'Admin/Article/restore','文章还原处理',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (87,'Admin/Article/restoreBatch','文章批量还原处理',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (88,'Admin/Article/clear','文章彻底删除处理',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (89,'Admin/Article/clearBatch','文章批量彻底删除处理',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (90,'Admin/Page/index','单页页面',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (91,'Admin/Page/indexHandle','单页修改保存处理',1,1,'',3,1);
INSERT INTO `wowocms_auth_rule` VALUES (92,'Admin/Auth/addUserHandle','系统管理员添加处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (93,'Admin/Auth/editUser','系统管理员修改保存',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (94,'Admin/Auth/delUserAll','系统管理员批量删除处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (95,'Admin/Auth/access','用户组权限设置处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (96,'Admin/Member/index','会员管理',2,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (97,'Admin/Auth/runEditRule','权限修改处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (98,'Admin/Auth/delRuleAll','权限批量删除处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (99,'Admin/Auth/groupAddHandle','用户组添加处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (100,'Admin/Auth/editGroupHandle','用户组修改处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (101,'Admin/Auth/delGroupHandle','用户组删除处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (102,'Admin/Auth/delBatch','用户组批量删除处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (103,'Admin/Auth/module','权限分组',2,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (104,'Admin/Auth/moduleView','权限分组添加/修改页面',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (105,'Admin/Auth/moduleHandle','权限分组添加/修改处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (106,'Admin/Auth/moduleDel','权限分组删除处理',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (107,'Admin/Member/group','组员管理',2,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (108,'Admin/Member/add','会员添加页面',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (109,'Admin/Member/addHandle','会员添加处理',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (110,'Admin/Member/edit','会员修改页面',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (111,'Admin/Member/editHandle','会员修改处理',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (112,'Admin/Member/groupHandle','会员组添加/修改处理',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (113,'Admin/Member/groupDel','会员组删除处理',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (114,'Admin/Member/comment','评论管理',2,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (115,'Admin/Member/lookComment','评论查看页面',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (116,'Admin/Member/commentHandle','评论回复处理',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (117,'Admin/Member/commentDel','评论删除处理',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (118,'Admin/Member/delBatch','评论批量删除',1,1,'',7,1);
INSERT INTO `wowocms_auth_rule` VALUES (119,'Admin/Order/index','订单管理',2,1,'',8,1);
INSERT INTO `wowocms_auth_rule` VALUES (120,'Admin/Order/look','订单收货信息查看页面',1,1,'',8,1);
INSERT INTO `wowocms_auth_rule` VALUES (121,'Admin/Order/edit','订单编辑页面',1,1,'',8,1);
INSERT INTO `wowocms_auth_rule` VALUES (122,'Admin/Order/editHandle','订单修改处理',1,1,'',8,1);
INSERT INTO `wowocms_auth_rule` VALUES (123,'Admin/Order/del','订单删除处理',1,1,'',8,1);
INSERT INTO `wowocms_auth_rule` VALUES (124,'Admin/Order/cart','购物车管理',2,1,'',8,1);
INSERT INTO `wowocms_auth_rule` VALUES (125,'Admin/Order/cartDel','购物车删除处理',1,1,'',8,1);
INSERT INTO `wowocms_auth_rule` VALUES (126,'Admin/Order/amlog','财务日志',2,1,'',8,1);
INSERT INTO `wowocms_auth_rule` VALUES (127,'Admin/Site/addHandle','友情链接添加处理',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (128,'Admin/Site/editHandle','友情链接编辑处理',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (129,'Admin/Guestbook/reply','留言板留言回复处理',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (130,'Admin/Sitemap/index','网站地图',2,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (131,'Admin/Sitemap/makeMap','网站地图生成处理',1,1,'',9,1);
INSERT INTO `wowocms_auth_rule` VALUES (132,'Admin/Wechat/index','信息配置',2,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (133,'Admin/Wechat/editHuiType','微信信息类型处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (134,'Admin/Wechat/words','文字信息',2,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (135,'Admin/Wechat/awords','微信添加关键字页面',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (136,'Admin/Wechat/awordsHandle','微信添加关键字处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (137,'Admin/Wechat/awordsEdit','微信关键字修改处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (138,'Admin/Wechat/atuwen','微信添加/修改图文素材页面',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (139,'Admin/Wechat/atuwenHandle','微信图文素材添加处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (140,'Admin/Wechat/atuwenEdit','微信图文素材修改处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (141,'Admin/Wechat/del','微信回复信息删除处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (142,'Admin/Wechat/batchFlag','微信回复信息批量删除处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (143,'Admin/Wechat/sort','微信信息排序处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (144,'Admin/Wechatauth/index','菜单管理',2,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (145,'Admin/Wechatauth/editHuiType','微信菜单接口配置保存',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (146,'Admin/Wechatauth/addMenu','微信菜单添加/修改页面',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (147,'Admin/Wechatauth/addMenuHandler','微信菜单添加处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (148,'Admin/Wechatauth/addChildHandler','微信菜单添加子菜单处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (149,'Admin/Wechatauth/editMenuHandler','微信菜单编辑处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (150,'Admin/Wechatauth/del','微信菜单删除处理',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (151,'Admin/Wechatauth/reloadMenu','微信发布菜单到微信',1,1,'',10,1);
INSERT INTO `wowocms_auth_rule` VALUES (161,'Admin/Model/fieldset','模型字段管理',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (162,'Admin/Model/addfieldset','模型添加字段',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (163,'Admin/Model/editfieldset','模型修改字段',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (164,'Admin/Model/delfieldset','模型删除字段',1,1,'',4,1);
INSERT INTO `wowocms_auth_rule` VALUES (165,'Admin/Auth/addHandle','添加权限操作',1,1,'',5,1);
INSERT INTO `wowocms_auth_rule` VALUES (166,'Admin/Auth/accessCate','访问栏目授权页面',1,1,'',5,1);






INSERT INTO `wowocms_flinks` VALUES (1,'wowocms','http://www.wowocms.com',1,'/Uploads/image/2015-09-01/55e522e77654d.png',1441080043,0,'');


INSERT INTO `wowocms_itemgroup` VALUES (1,'flagtype','文档属性',0);
INSERT INTO `wowocms_itemgroup` VALUES (3,'softtype','软件类型',0);
INSERT INTO `wowocms_itemgroup` VALUES (4,'softlanguage','软件语言',0);

INSERT INTO `wowocms_iteminfo` VALUES (1,'头条','flagtype',5,1);
INSERT INTO `wowocms_iteminfo` VALUES (3,'推荐','flagtype',3,1);
INSERT INTO `wowocms_iteminfo` VALUES (8,'跳转','flagtype',32,2);
INSERT INTO `wowocms_iteminfo` VALUES (9,'图文','flagtype',1,0);
INSERT INTO `wowocms_iteminfo` VALUES (10,'特荐','flagtype',4,0);
INSERT INTO `wowocms_iteminfo` VALUES (17,'中文','softlanguage',1,0);
INSERT INTO `wowocms_iteminfo` VALUES (18,'英文','softlanguage',2,0);

INSERT INTO `wowocms_member` VALUES (2,'admin@qq.com','990e2eada8513f467433a577eb842130','QyMdcB','煮酒节',9272,1140,'/Uploads/pics/2014-09-09/540e758f2fb77.JPG',1409858064,1429111290,'127.0.0.1',57,1,NULL,1,0);

INSERT INTO `wowocms_memberdetail` VALUES (2,'习近平',0,1307462400,707421609,'18587736605');

INSERT INTO `wowocms_membergroup` VALUES (1,'初级会员','初级会员',1,1);
INSERT INTO `wowocms_membergroup` VALUES (2,'中级会员','中级会员',1,2);
INSERT INTO `wowocms_membergroup` VALUES (3,'高级会员','高级会员',1,3);


INSERT INTO `wowocms_model` VALUES (4,'单页模型','单页模型','page',1,NULL,'List_Page.html','Show_Page.html',1,NULL);
INSERT INTO `wowocms_model` VALUES (6,'下载模型','下载模型','download',1,NULL,'List_Download.html','Show_Download.html',1,NULL);
INSERT INTO `wowocms_model` VALUES (23,'文章模型','啊','article',1,'','List_Article.html','Show_Article.html',1,NULL);
INSERT INTO `wowocms_model` VALUES (25,'产品模型','产品模型','product',1,'','List_Page.html','Show_Product.html',1,NULL);

INSERT INTO `wowocms_model_field` VALUES (24,23,'content','内容','','htmltext',1);
INSERT INTO `wowocms_model_field` VALUES (25,25,'pictureurls','产品图片','','imgfile',1);
INSERT INTO `wowocms_model_field` VALUES (26,25,'price','价格','','float',1);
INSERT INTO `wowocms_model_field` VALUES (27,25,'brand','品牌','','text',1);
INSERT INTO `wowocms_model_field` VALUES (28,25,'units','单位','','text',1);
INSERT INTO `wowocms_model_field` VALUES (29,25,'norms','规格','','text',1);
INSERT INTO `wowocms_model_field` VALUES (30,25,'content','详细介绍','','htmltext',1);




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
