/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;

CREATE TABLE `wowocms_active` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` varchar(11) NOT NULL,
  `expire` int(11) unsigned NOT NULL,
  `email` varchar(50) NOT NULL,
  `updatetime` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_addonarticle` (
  `aid` int(11) NOT NULL DEFAULT '0',
  `content` mediumtext,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_addonproduct` (
  `aid` int(11) NOT NULL DEFAULT '0',
  `pictureurls` text,
  `price` float NOT NULL DEFAULT '0',
  `brand` varchar(250) NOT NULL DEFAULT '',
  `units` varchar(250) NOT NULL DEFAULT '',
  `norms` varchar(250) NOT NULL DEFAULT '',
  `content` mediumtext,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_amlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(50) NOT NULL DEFAULT '',
  `orderfee` double NOT NULL DEFAULT '0' COMMENT '金额',
  `creattime` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `dtype` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1为收入 0为支出',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='现金明细';
CREATE TABLE `wowocms_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `cid` int(11) unsigned NOT NULL DEFAULT '0',
  `click` int(10) unsigned NOT NULL DEFAULT '0',
  `shorttitle` varchar(60) NOT NULL DEFAULT '',
  `litpic` varchar(150) NOT NULL DEFAULT '',
  `color` char(10) NOT NULL DEFAULT '',
  `copyfrom` varchar(45) NOT NULL DEFAULT '',
  `author` varchar(45) NOT NULL DEFAULT '',
  `keywords` varchar(60) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `publishtime` int(10) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `commentflag` tinyint(1) NOT NULL DEFAULT '1',
  `flag` varchar(50) NOT NULL DEFAULT '',
  `jumpurl` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `aid` int(11) unsigned NOT NULL DEFAULT '0',
  `addtable` varchar(60) NOT NULL DEFAULT 'addonarticle',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL COMMENT '源文件名称',
  `filepath` varchar(255) DEFAULT NULL COMMENT '文件存储路径',
  `filetype` smallint(6) DEFAULT '1' COMMENT '文件类型',
  `filesize` mediumint(8) unsigned DEFAULT '0' COMMENT '文件大小',
  `haslitpic` tinyint(1) unsigned DEFAULT '0' COMMENT '是否含有缩略图0否1是',
  `uploadtime` int(10) unsigned DEFAULT NULL COMMENT '上传时间',
  `aid` int(10) unsigned DEFAULT '0' COMMENT '管理员ID',
  `userid` int(10) unsigned DEFAULT '0' COMMENT '会员ID',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_attachmentindex` (
  `attid` int(10) unsigned NOT NULL COMMENT '附件ID',
  `arcid` int(10) unsigned NOT NULL COMMENT '所属文章ID',
  `modelid` int(10) unsigned NOT NULL COMMENT '所属模型ID',
  `desc` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_auth_extend` (
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `extend_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1',
  KEY `group_id` (`group_id`),
  KEY `extend_id` (`extend_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text NOT NULL,
  `describe` char(50) DEFAULT NULL,
  `type` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `module` varchar(50) NOT NULL DEFAULT 'admin',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_auth_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moduleName` varchar(50) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则简述',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `mid` tinyint(3) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '栏目名称',
  `ename` varchar(45) DEFAULT NULL COMMENT '栏目别名',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级ID',
  `modelid` int(10) NOT NULL COMMENT '模型ID',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是外链0否1是',
  `seotitle` varchar(50) DEFAULT NULL COMMENT 'SEO标题',
  `keywords` varchar(100) DEFAULT NULL COMMENT '栏目关键字',
  `description` varchar(255) DEFAULT NULL COMMENT '栏目描述',
  `template_category` varchar(45) DEFAULT NULL COMMENT '栏目封面',
  `template_list` varchar(45) DEFAULT NULL COMMENT '列表模版',
  `template_show` varchar(45) DEFAULT NULL COMMENT '内容模版',
  `content` mediumtext COMMENT '单页内容',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否隐藏1显示0隐藏',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `nav` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否显示导航0否1是',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_chatmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `url` varchar(225) NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0链接;1点击;2栏目',
  `key` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信自定义菜单';
CREATE TABLE `wowocms_chatwords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(225) NOT NULL,
  `content` text NOT NULL,
  `publictime` int(11) NOT NULL,
  `aid` int(11) NOT NULL,
  `url` varchar(255) NOT NULL COMMENT '地址URL',
  `picurl` varchar(255) NOT NULL COMMENT '缩略图URL',
  `isnews` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否图文信息',
  `iszidong` tinyint(3) NOT NULL DEFAULT '0' COMMENT '自动回复的信息',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `keywords` varchar(50) NOT NULL DEFAULT '' COMMENT '触发关键字',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `postid` int(11) unsigned NOT NULL DEFAULT '1',
  `modelid` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `posttime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `cid` int(11) unsigned NOT NULL DEFAULT '0',
  `ename` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `color` char(25) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `litpic` varchar(255) DEFAULT NULL,
  `content` text,
  `downlink` text,
  `description` varchar(255) DEFAULT NULL,
  `filesize` varchar(10) DEFAULT NULL,
  `publishtime` int(11) unsigned DEFAULT NULL,
  `updatetime` int(11) unsigned DEFAULT NULL,
  `click` int(11) unsigned DEFAULT NULL,
  `cid` int(11) unsigned DEFAULT NULL,
  `commentflag` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `flag` varchar(50) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned DEFAULT NULL,
  `aid` int(11) unsigned NOT NULL DEFAULT '0',
  `jumpurl` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_flinks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `url` varchar(45) NOT NULL,
  `sort` int(10) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `posttime` int(11) DEFAULT NULL,
  `ischeck` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1内页 0 首页',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='友情链接';
CREATE TABLE `wowocms_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `tel` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `ip` varchar(20) DEFAULT NULL COMMENT '留言者IP',
  `posttime` int(10) unsigned NOT NULL COMMENT '留言时间',
  `content` text NOT NULL COMMENT '留言内容',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '前台是否显示',
  `replytime` int(10) DEFAULT NULL,
  `reply` text,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_itemgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `remark` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_iteminfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '属性名称',
  `group` varchar(45) NOT NULL COMMENT '所属分组',
  `value` int(10) NOT NULL COMMENT '枚举值',
  `sort` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(34) NOT NULL,
  `encrypt` varchar(20) NOT NULL,
  `nickname` varchar(25) DEFAULT NULL,
  `amount` double(10,2) DEFAULT '0.00',
  `score` int(10) NOT NULL DEFAULT '0',
  `face` varchar(50) DEFAULT NULL,
  `regtime` int(10) NOT NULL,
  `logintime` int(10) NOT NULL,
  `loginip` varchar(20) NOT NULL,
  `loginnum` int(10) NOT NULL,
  `groupid` int(10) NOT NULL,
  `message` varchar(225) DEFAULT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `islock` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='会员表';
CREATE TABLE `wowocms_memberdetail` (
  `userid` int(10) unsigned NOT NULL,
  `realname` varchar(20) NOT NULL,
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `birthday` int(10) NOT NULL,
  `qq` int(20) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_membergroup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_members` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL COMMENT '用户名',
  `password` varchar(45) DEFAULT NULL COMMENT '密码',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否被禁止0否1是',
  `loginnum` int(11) DEFAULT NULL COMMENT '登录次数',
  `lasttime` int(11) DEFAULT NULL COMMENT '最后登录时间',
  `lastip` varchar(20) DEFAULT NULL COMMENT '最后登录IP',
  `pretime` int(11) DEFAULT NULL COMMENT '上次登录时间',
  `preip` varchar(20) DEFAULT NULL COMMENT '上次登录IP',
  `encrypt` varchar(8) DEFAULT 'sdfdasa',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '模型名称',
  `description` varchar(255) DEFAULT NULL COMMENT '模型描述',
  `tablename` varchar(45) NOT NULL COMMENT '模型表',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '是否启动0否 1是',
  `template_category` varchar(45) DEFAULT NULL,
  `template_list` varchar(45) DEFAULT NULL,
  `template_show` varchar(45) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `fieldset` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_model_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) unsigned NOT NULL DEFAULT '0',
  `field` varchar(50) NOT NULL DEFAULT '',
  `itemname` varchar(60) NOT NULL DEFAULT '',
  `default_value` varchar(255) DEFAULT NULL,
  `type` varchar(50) NOT NULL DEFAULT '',
  `sort` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_orderlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `ordid` varchar(255) DEFAULT NULL,
  `ordtime` int(11) DEFAULT NULL,
  `productid` int(11) DEFAULT NULL,
  `ordtitle` varchar(255) DEFAULT NULL,
  `ordbuynum` int(11) DEFAULT '0',
  `ordprice` float(10,2) DEFAULT '0.00',
  `ordfee` float(10,2) DEFAULT '0.00',
  `ordstatus` int(11) DEFAULT '0',
  `payment_type` varchar(255) DEFAULT NULL,
  `payment_trade_no` varchar(255) DEFAULT NULL,
  `payment_trade_status` varchar(255) DEFAULT NULL,
  `payment_notify_id` varchar(255) DEFAULT NULL,
  `payment_notify_time` varchar(255) DEFAULT NULL,
  `payment_buyer_email` varchar(255) DEFAULT NULL,
  `getname` varchar(50) NOT NULL DEFAULT '' COMMENT '收货人',
  `shouhuo` varchar(255) NOT NULL DEFAULT '' COMMENT '收货地址',
  `youbian` varchar(20) NOT NULL DEFAULT '' COMMENT '邮政编码',
  `phone` varchar(50) NOT NULL DEFAULT '' COMMENT '联系电话',
  `beizhu` varchar(255) DEFAULT '' COMMENT '订单备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `cid` int(11) unsigned NOT NULL DEFAULT '0',
  `click` int(10) unsigned NOT NULL DEFAULT '0',
  `shorttitle` varchar(60) NOT NULL DEFAULT '',
  `litpic` varchar(150) NOT NULL DEFAULT '',
  `color` char(10) NOT NULL DEFAULT '',
  `copyfrom` varchar(45) NOT NULL DEFAULT '',
  `author` varchar(45) NOT NULL DEFAULT '',
  `keywords` varchar(60) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `publishtime` int(10) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `commentflag` tinyint(1) NOT NULL DEFAULT '1',
  `flag` varchar(50) NOT NULL DEFAULT '',
  `jumpurl` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `aid` int(11) unsigned NOT NULL DEFAULT '0',
  `addtable` varchar(60) NOT NULL DEFAULT 'addonproduct',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `wowocms_shopcart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `product_title` varchar(255) NOT NULL DEFAULT '',
  `product_price` double NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `istolist` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否被生成订单0：否，1：是，2：已付款',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
