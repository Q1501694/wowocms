<?php
    function SendDataByCurl($data=array()){
        $url="http://service.0771mc.com/user/api/index";
        $vilidate=array(
            'Host'=>C('cfg_weburl'),
            'Key'=>KEY,
            'session_id'=>session_id(),
            'uploads'=>U('Admin/Attachment/uploads'),
            'uploadPlus_image'=>U('Admin/Attachment/uploadPlus',array('dir'=>'image')),
            'uploadPlus_file'=>U('Admin/Attachment/uploadPlus',array('dir'=>'file')),
            'uploadPlus_media'=>U('Admin/Attachment/uploadPlus',array('dir'=>'media')),
            'filemanager'=>U('Admin/Attachment/filemanager'),
            'delimg'=>U('Admin/Attachment/delimg'),
            );
        //p($data);
        $data=array_merge($vilidate,$data);
        $data=serialize($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_TIMEOUT,30);  //定义超时3秒钟  
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);
        if(0 !== $errorCode) {
            return false;
        }
        return $output;
    }

    function SendDataByCurls($fields=array(),$data=array()){
        $url="http://service.0771mc.com/user/api/edit";
        $vilidate=array(
            'Host'=>C('cfg_weburl'),
            'Key'=>KEY,
            'session_id'=>session_id(),
            'uploads'=>U('Admin/Attachment/uploads'),
            'uploadPlus_image'=>U('Admin/Attachment/uploadPlus',array('dir'=>'image')),
            'uploadPlus_file'=>U('Admin/Attachment/uploadPlus',array('dir'=>'file')),
            'uploadPlus_media'=>U('Admin/Attachment/uploadPlus',array('dir'=>'media')),
            'filemanager'=>U('Admin/Attachment/filemanager'),
            'delimg'=>U('Admin/Attachment/delimg'),
        );
        $datas[0]=$vilidate;
        $datas[1]=$fields;
        $datas[2]=$data;
        $datas=serialize($datas);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_TIMEOUT,30);  //定义超时3秒钟  
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
        $output = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);
        if(0 !== $errorCode) {
            return false;
        }
        return $output;
    }
    function getMenus($menus=array()){
        $url="http://service.0771mc.com/user/api/wechatMenu";
        $vilidate=array(
            'Host'=>C('cfg_weburl'),
            'Key'=>KEY,
        );
        $datas=array_merge($vilidate,$menus);
        $datas=serialize($datas);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_TIMEOUT,30);  //定义超时3秒钟  
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
        $output = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);
        if(0 !== $errorCode) {
            return false;
        }
        return $output;
    }
    function p($arr){
        header("Content-Type:text/html; charset=utf-8");
        print_r('<pre>');
        print_r($arr);
        print_r('</pre>');
        exit;
    }
    function change2array($array){
        $rs=array();
        foreach ($array as $key => $value) {
            foreach ($value as $k => $v) {
                $rs[]=$v;
            }
        }
        $rs=array_unique($rs);
        $rs_string=implode(',',$rs);
        return $rs_string;
    }
    /**
     * 根据PHP各种类型变量生成唯一标识号
     * @param mixed $mix 变量
     * @return string
     */
    function biuld_object_id($mix) {
        if (is_object($mix)) {
            return spl_object_hash($mix);
        } elseif (is_resource($mix)) {
            $mix = get_resource_type($mix) . strval($mix);
        } else {
            $mix = serialize($mix);
        }
        return md5($mix);
    }
    function getCateNameAndEnameById($id){
        return M('category')->where(array('id'=>$id))->field('name,ename')->find();
    }

    function build_order_no()
    {
        mt_srand((double) microtime() * 1000000);
        return 'sp'.date('Ymd') . str_pad(mt_rand(1, 999999), 6, '0', STR_PAD_LEFT);
    }

    function build_cz_order_no()
    {
        mt_srand((double) microtime() * 1000000);
        return 'cz'.date('Ymd') . str_pad(mt_rand(1, 999999), 6, '0', STR_PAD_LEFT);
    }

    function default_pic($pic){
        if (empty($pic)) {
            return "/Public/images/nopic.jpg";
        }else{
            if (file_exists(".".$pic)) {
                return $pic;
            }else{
                return "/Public/images/nopic.jpg";
            }
        }
    }

    function set_cookie($args , $key = '') {
        $key = empty($key)? C('cfg_cookie_encode') : $key;
        $name = $args['name'];
        $expire = isset($args['expire']) ? $args['expire'] : null;
        $path = isset($args['path']) ? $args['path'] : '/';
        $domain = isset($args['domain']) ? $args['domain'] : null;
        $secure = isset($args['secure']) ? $args['secure'] : 0;    
        $value = serialize($args['value']);
        $key = md5($key);
        $sc = new \Think\SysCrypt($key);
        $value =$sc->php_encrypt($value);
        return setcookie($name, $value, $expire, $path, $domain, $secure);//失效时间   0关闭浏览器即失效
    }

    function get_cookie($name, $key = '') {
        if (!isset($_COOKIE[$name])) {
            return null;
        }
        $key = empty($key)? C('cfg_cookie_encode') : $key;
        $value = $_COOKIE[$name];
        $key=md5($key);
        $sc = new \Think\SysCrypt($key);
        $value=$sc->php_decrypt($value);
        return unserialize($value);
    }

    function del_cookie($args){
        $name = $args['name'];
        $domain = isset($args['domain']) ? $args['domain'] : null;
        return isset($_COOKIE[$name]) ? setcookie($name, '', time() - 86400, '/', $domain) : true;
    }

    function exp_flags($flag){
        return explode(',', $flag);
    }

    function goLinkEncode($weburl = 'http://www.mcly.cc/') {
        return U(C('DEFAULT_MODULE'). '/Go/link',array('url' => base64_encode($weburl)));
    }

    function get_nickname($uid){
        $nickname=M('member')->where(array('id'=>$uid))->getField('nickname');
        return $nickname;
    }

    function get_pinyin($str, $ishead=0, $isclose=1)
    {
        $pinyins = array();
        $restr = '';
        $str = trim($str);
        $slen = strlen($str);
        if($slen < 2)
        {
            return $str;
        }
        if(count($pinyins) == 0)
        {
            $fp = fopen('./Data/pinyin.dat', 'r');
            if (false == $fp) {
                return '';
            }
            while(!feof($fp))
            {
                $line = trim(fgets($fp));
                $pinyins[$line[0].$line[1]] = substr($line, 3, strlen($line)-3);
            }
            fclose($fp);
        }
        
        for($i=0; $i<$slen; $i++)
        {
            if(ord($str[$i])>0x80)
            {
                $c = $str[$i].$str[$i+1];
                $i++;
                if(isset($pinyins[$c]))
                {
                    if($ishead==0)
                    {
                        $restr .= $pinyins[$c];
                    }
                    else
                    {
                        $restr .= $pinyins[$c][0];
                    }
                }else
                {
                    $restr .= "_";
                }
            }else if( preg_match("/[a-z0-9]/i", $str[$i]) )
            {
                $restr .= $str[$i];
            }
            else
            {
                $restr .= "_";
            }
        }
        if($isclose==0)
        {
            unset($pinyins);
        }
        return $restr;
    }

    function SendMail($address, $title, $message, $attachment = null)
    {
        vendor('PHPMailer.class#phpmailer');
        $mail = new PHPMailer;
        //$mail->Priority = 3;
        // 设置PHPMailer使用SMTP服务器发送Email
        $mail->IsSMTP();
        // 设置邮件的字符编码，若不指定，则为'UTF-8'
        $mail->CharSet='UTF-8';
        $mail->SMTPDebug  = 0; // 关闭SMTP调试功能
        $mail->SMTPAuth   = true; // 启用 SMTP 验证功能
       // $mail->SMTPSecure = 'ssl';  // 使用安全协议
        $mail->IsHTML(true);//body is html

        // 设置SMTP服务器。
        $mail->Host=C('cfg_email_host');
        $mail->Port = C('cfg_email_port') ? C('cfg_email_port') : 25 ;  // SMTP服务器的端口号

        // 设置用户名和密码。
        $mail->Username =C('cfg_email_loginname');
        $mail->Password = C('cfg_email_password');

        // 设置邮件头的From字段
        $mail->From=C('cfg_email_from');
        // 设置发件人名字
        $mail->FromName = C('cfg_email_from_name');

        // 设置邮件标题
        $mail->Subject=$title;
        // 添加收件人地址，可以多次使用来添加多个收件人
        $mail->AddAddress($address);
        // 设置邮件正文
        $mail->Body=$message;
        // 添加附件
        if(is_array($attachment)){ 
            foreach ($attachment as $file){
                is_file($file) && $mail->AddAttachment($file);
            }
        }
        // 发送邮件。
        //return($mail->Send());
        return $mail->Send() ? true : $mail->ErrorInfo;
    }

    function get_verify() {
        $Verify = new \Think\Verify();
        $Verify -> codeSet = '0123456789';
        $Verify -> length = 3;
        $Verify -> entry(1);
    }

    function check_verify($code, $id = '') {
        $verify = new \Think\Verify();
        return $verify -> check($code, $id);
    }

    /*function get_iparea($ip){

        $Ip = new \Org\Util\IpLocation('UTFWry.dat'); // 实例化类 参数表示IP地址库文件
        $area = $Ip->getlocation("'".$ip."'"); // 获取某个IP地址所在的位置
        $res=$area['country'].$area['area'];
        if ($res=="IANA保留地址CZ88.NET") {
            return $ip;
        }else{
            return $res;
        }
    }*/
    function getClick($id, $tablename, $flag = 0) {
            $id = intval($id);
            if (empty($id) || empty($tablename)) {
                return '--';
            }
            $num = M($tablename)->where(array('id' => $id))->getField('click');
            M($tablename)->where(array('id' => $id))->setInc('click');
            return "$num";
    }

    /*
     * 判断是否为WAP端访问
     */
    function ismobile() {
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
        if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
            return true;
         
        //此条摘自TPM智能切换模板引擎，适合TPM开发
        if(isset ($_SERVER['HTTP_CLIENT']) &&'PhoneClient'==$_SERVER['HTTP_CLIENT'])
            return true;
        //如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
        if (isset ($_SERVER['HTTP_VIA']))
            //找不到为flase,否则为true
            return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
        //判断手机发送的客户端标志,兼容性有待提高
        if (isset ($_SERVER['HTTP_USER_AGENT'])) {
            $clientkeywords = array(
                'nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile'
            );
            //从HTTP_USER_AGENT中查找手机浏览器的关键字
            if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                return true;
            }
        }
        //协议法，因为有可能不准确，放到最后判断
        if (isset ($_SERVER['HTTP_ACCEPT'])) {
            // 如果只支持wml并且不支持html那一定是移动设备
            // 如果支持wml和html但是wml在html之前则是移动设备
            if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                return true;
            }
        }
        return false;
     }

    /**goto mobile*/
    function goMobile() {
        $mobileAuto = C('cfg_mobile_auto');
        if ($mobileAuto == 1) {
            $wap2web = I('wap2web', 0, 'intval');//手机访问电脑版
            $agent = $_SERVER['HTTP_USER_AGENT'];   
            if ($wap2web != 1) {
                if(strpos($agent,"comFront") || strpos($agent,"iPhone") || strpos($agent,"MIDP-2.0") || strpos($agent,"Opera Mini") || strpos($agent,"UCWEB") || strpos($agent,"Android") || strpos($agent,"Windows Phone") || strpos($agent,"Windows CE") || strpos($agent,"SymbianOS"))
                {
                    header('Location:'.U('M/Index/index').'');
                }
            }   
        }
        
    }

    /*
    *getTpl 获取模板地址
    *
    */
    function get_tpl($tpl = '', $style = '') {
        $groupName = ucfirst(strtolower(GROUP_NAME));
        $tplPath = APP_PATH.MODULE_NAME.'/View/default/';
        if ($groupName  != 'Index') {
             $tplPath .= $groupName. '/';
        }
        if (trim($tpl) == '') {
           $tplPath .= MODULE_NAME. C('TMPL_FILE_DEPR'). ACTION_NAME. C('TMPL_TEMPLATE_SUFFIX');
        }elseif (strpos($tpl, '.') > 0) {
            $ext = explode('.', $tpl);
            $ext = end($ext);
            $tplPath .= str_replace('.'.$ext, '', $tpl). C('TMPL_TEMPLATE_SUFFIX');
        }elseif (strpos($tpl, ':') > 0){
            $tpl_array = explode(':', $tpl);
            $tplPath .= $tpl_array[0]. C('TMPL_FILE_DEPR'). $tpl_array[1]. C('TMPL_TEMPLATE_SUFFIX');
        }else{
            
            $tplPath .= MODULE_NAME. C('TMPL_FILE_DEPR').$tpl. C('TMPL_TEMPLATE_SUFFIX');
        }
        return $tplPath;
    }

    /**
     * 对用户的密码进行加密
     * @param $password
     * @param $encrypt //传入加密串，在修改密码时做认证
     * @return array/password
     */
    function get_password($password, $encrypt='') {
        $pwd = array();
        $pwd['encrypt'] =  $encrypt ? $encrypt : get_randomstr();
        $pwd['password'] = md5(md5(trim($password)).$pwd['encrypt']);
        return $encrypt ? $pwd['password'] : $pwd;
    }
    function writeArr($arr, $filename) {
        return file_put_contents($filename, "<?php\r\nreturn " . var_export($arr, true) . ";");
    }

    /**
     * getFileFolderList
     *@fileFlag 0 所有文件列表,1只读文件夹,2是只读文件(不包含文件夹)
     */
    //获取文件目录列表,该方法返回数组
    function getFileFolderList($pathname, $fileFlag = 0, $pattern = '*') {
        $fileArray = array();
        $pathname = rtrim($pathname, '/') . '/';
        $list = glob($pathname . $pattern);
        foreach ($list as $i => $file) {
            switch ($fileFlag) {
                case 0 :
                    $fileArray[] = basename($file);
                    break;
                case 1 :
                    if (is_dir($file)) {
                        $fileArray[] = basename($file);
                    }
                    break;
                case 2 :
                    if (is_file($file)) {
                        $fileArray[] = basename($file);
                    }
                    break;
                default :
                    break;
            }
        }
        if (empty($fileArray))
            $fileArray = NULL;
        return $fileArray;
    }
    //$ismobile是否是Mobile,$typeid栏目id,
    function getPosition($typeid = 0, $sname = '', $surl = '', $ismobile = false, $delimiter = '&gt;&gt;') {
        if ($delimiter == '' ) {
            $delimiter = ' &gt; ';
        }
        $url = $ismobile ? U('Index/index/') : C('cfg_weburl');
        $position = '<a href="'. $url .'">首页</a>';

        //Parents of Category
        if (!empty($typeid)) {
            $cate = getCategory(0);//ALL
            $Category = new \Think\Category(); 
            $getParents = $Category::getParents($cate, $typeid);
            if (is_array($getParents)) {            
                foreach ($getParents as $v) {
                    $position .= $delimiter. '<a href="' . getUrl($v) .'">'.$v['name']. '</a>'; 
                }
            }
        }

        if (!empty($sname)) {
            if (empty($surl)) {
                $position .= $delimiter. $sname; 
            }else {
                $position .= $delimiter. '<a href="' . $surl .'">'.$sname. '</a>'; 
            }
        }
        return $position;
    }
    /**
     *取出所有分类
     *
     *@param string $status 显示部份(0|1|2)， 0显示全部(默认),1显示不隐藏的,2显示type为0(类型为内部模型非外链)全部
     *@param string $update 更新缓存(0|1,false|true)， 默认0不更新
     */
    function getCategory($status = 0, $update = 0) {//
        $cate_sname = 'fCategery_' . $status;
        $cate_arr = F($cate_sname);

        if ($update || !$cate_arr) {

            switch ($status) {

                case 1://查询出显示的栏目（剔除隐藏的栏目）
                    $cate_arr = D('CategoryView') -> where(array('category.status' => 1)) -> order('category.sort') -> select();
                    break;
                case 2://查询出内部栏目（剔除外联栏目）
                    $cate_arr = D('CategoryView') -> where(array('category.type' => 0)) -> order('category.sort') -> select();
                    break;
                case 3://查询出显示的栏目及显示为导航的栏目
                    $cate_arr = D('CategoryView') -> where(array('category.status' => 1,'category.nav'=> 1)) -> order('category.sort') -> select();
                        break;
                default:
                    $cate_arr = D('CategoryView') -> order('category.sort') -> select();
                    break;
            }
            if (!isset($cate_arr)) {
                $cate_arr = array();
            }
            F($cate_sname, $cate_arr);
        }
        return $cate_arr;
    }

    /*
     获取文章属性
     @update 0不更新  1 更新缓存()
     */
    function getArrayOfItem($group = 'animal', $update = 0) {
        $itme_arr = S('sItem_' . $group);
        if ($update || !$itme_arr) {
            $itme_arr = array();
            $temp = M('iteminfo') -> where(array('group' => $group)) -> order('sort,id') -> select();
            foreach ($temp as $key => $v) {
                $itme_arr[$v['value']] = $v['name'];
            }
            //S(缓存名称,缓存值,缓存有效时间[秒]);
            S('sItem_' . $group, $itme_arr, 48 * 60 * 60);
        }
        return $itme_arr;
    }

    //删除静态缓存文件
    //$isdir 是否是目录
    //$rules 规则名称
    function delCacheHtml($str, $isdir = false, $rules = '') {
        $delflag = true;
        if (empty($str) && !$isdir) {
            return;
        }
        $str_array = array();
        $html_cache_rules = C('HTML_CACHE_RULES');
        if (C('HTML_CACHE_ON__INDEX')) {
            $str_array[] = HTML_PATH . 'Home/' . $str;
        }
        if (C('HTML_CACHE_ON__NOBILE')) {
            $str_array[] = HTML_PATH . 'M/' . $str;
        }
        if (!empty($rules) && !isset($html_cache_rules[$rules])) {
            $delflag = false;
        } else {
            $delflag = true;
        }
        if ($delflag) {
            foreach ($str_array as $v) {
                if ($isdir && is_dir($v)) {
                    delDirAndFile($v, false);
                } else {
                    $list = glob($v . '*');
                    for ($i = 0; $i < count($list); $i++) {
                        if (is_file($list[$i])) {
                            unlink($list[$i]);
                        }
                    }
                }
            }
        }
    }

    //flag相加,返回数值，用于查询
    function flag2sum($str, $delimiter = ',') {
        if (empty($str)) {
            return 0;
        }
        $tmp_arr = array_filter(explode($delimiter, $str));//去除空数组'',0,再使用sort()重建索引
        if (empty($tmp_arr)) {
            return 0;
        }

        $arr = array('a' => B_PIC, 'b' => B_TOP, 'c' => B_REC, 'd' => B_SREC, 'e' => B_SLIDE, 'f' => B_JUMP, 'g' => B_OTHER);
        $sum = 0;
        foreach ($arr as $k => $v) {
            if (in_array($k, $tmp_arr)) {
                $sum += $v;
            }
        }
        return $sum;
    }
    function string2filter($str, $delimiter = ',', $flag = false) {
        if (empty($str)) {
            return '';
        }
        $tmp_arr = array_filter(explode($delimiter, $str));//去除空数组'',0,再使用sort()重建索引
        $tmp_arr2 = array();

        //检验是不是数字
        if ($flag) {
            foreach ($tmp_arr as $v) {
                if (is_numeric($v)) {
                    $tmp_arr2[] = $v;
                }        
            }
        } else {
            $tmp_arr2 = $tmp_arr;
        }
        return implode($delimiter, $tmp_arr2);
    }



/*
截图字符
 */
function str2sub($str, $num, $flag = 0, $sp = '...') {
    if ($str == '' || $num <= 0) {
        return $str;
    }
    $strlen = mb_strlen($str, 'utf-8');
    $newstr ='';
    $newstr .= mb_substr($str, 0, $num, 'utf-8');
    if ($num < $strlen && $flag) {
        $newstr .= $sp;
    }
    return $newstr;
}

/**
 * 功能：计算文件大小
 * @param int $bytes
 * @return string 转换后的字符串
 */
function get_byte($bytes) {
    if (empty($bytes)) {
        return '--';
    }
    $sizetext = array(" B", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
    return round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), 2) . $sizetext[$i];
}


function arrToStr ($array)
{
    static $r_arr = array();
    if (is_array($array)) {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                arrToStr($value);
            } else {
                $r_arr[] = $value;
            }
        }
    } else if (is_string($array)) {
            $r_arr[] = $array;
    }
    $r_arr = array_unique($r_arr);
    $string = implode(",", $r_arr);
    return $string;
}

function get_random($length, $chars = '0123456789') {
    $hash = '';
    $max = strlen($chars) - 1;
    for($i = 0; $i < $length; $i++) {
        $hash .= $chars[mt_rand(0, $max)];
    }
    return $hash;
}

function get_randomstr($lenth = 6) {
    return get_random($lenth, '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ');
}

function getContentUrl($id, $cid, $ename, $jumpflag = false, $jumpurl = '') {
    $url = '';
	$model_name=strtolower(MODULE_NAME);
    if ($jumpflag && !empty($jumpurl)) {
        return $jumpurl;
    }
    if (empty($id) || empty($cid) || empty($ename)) {
        return $url;
    }
    if(C('URL_ROUTER_ON') == true) {
    	if($model_name=='m'){
    		$url = $id > 0 ? U(''.$ename.'/'.$id,'') : U('/'.$model_name.'/'.$ename,'', '');
    	}else{
    		$url = $id > 0 ? U(''.$ename.'/'.$id,'') : U('/'.$ename,'', '');
    	}
    }else {
        $url  = U('Show/index', array('cid' => $cid, 'id'=> $id));
    }
    return strtolower($url);//全部转小写
}

function getUrl($cate, $id = 0, $jumpflag = false, $jumpurl = '') {
    $url = '';
    if ($jumpflag && !empty($jumpurl)) {
        return $jumpurl;
    }
    if (empty($cate)) {
        return $url;
    }
	$model_name=strtolower(MODULE_NAME);
	//P($model_name);
    $ename = $cate['ename'];
    if ($cate['type'] == 1) {
        $firstChar = substr($ename, 0, 1);
        if ($firstChar == '@') {
            $ename = ucfirst(substr($ename, 1));//
            if(C('URL_ROUTER_ON') == true) {
            	if($model_name=='m'){
            		$url = $id > 0 ? U(''.$ename.'/'.$id,'') : U('/'.$model_name.'/'.$ename,'');
            	}else{
            		$url = $id > 0 ? U(''.$ename.'/'.$id,'') : U('/'.$ename,'');
            	}
            }else {
                $url  = U(''.$ename.'');
                if ($id > 0) {
                    $url  = U($ename.'/shows', array('e' => $cate['tablename'], 'id'=> $cate['id']));
                }
            }
        }else {
            $url = $ename;
        }
    }else {
        if(C('URL_ROUTER_ON') == true) {
            if($model_name=='m'){
        		$url = $id > 0 ? U(''.$ename.'/'.$id,'') : U('/'.$model_name.'/'.$ename,'','');
        	}else{
        		$url = $id > 0 ? U(''.$ename.'/'.$id,'') : U('/'.$ename,'','');
        	}
        }else {
            $url  = U('List/index', array('cid'=> $cate['id']));
            if ($id > 0) {
                $url  = U('Show/index', array('cid' => $cate['cid'], 'id'=> $cate['id']));
            }
        }
    }
    return strtolower($url);//全部转小写
}