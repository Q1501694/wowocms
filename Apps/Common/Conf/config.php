<?php
return array(
   'VAR_SESSION_ID' => 'session_id',
   'LOAD_EXT_CONFIG' => 'config.db,config.site,config.pay,pics,config.url,stat',
   'URL_CASE_INSENSITIVE' =>true, //不区分URL大小写
   /** 上传路径  */
   'ROOTPATH'=>'/Uploads/',
   'DOC_SAVE_PATH'=>'./Uploads/docs/',

   
    //'SHOW_ERROR_MSG' =>  false,    // 显示错误信息
    //'ERROR_MESSAGE'  =>    '发生错误！',
    //'SHOW_PAGE_TRACE' =>true,
    'ERROR_PAGE' =>'./Public/UI/error.html',

    'TMPL_ACTION_ERROR'     =>  './Public/UI/jump.php', // error tpl
    'TMPL_ACTION_SUCCESS'   => './Public/UI/jump.php', //  success tpl
    'TMPL_EXCEPTION_FILE'   => './Public/UI/exception.php', //exception tpl



   
);