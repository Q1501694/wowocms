<?php
return array(
	//'配置项'=>'配置值'
   // 预先加载的标签库
    'HTML_CACHE_ON' => false,
    'TAGLIB_BUILD_IN' => 'Cx,Wowocms',
    'TAGLIB_PRE_LOAD' => 'wowocms',
    'DEFAULT_FILTER' => 'strip_tags,htmlspecialchars,trim',
    'DEFAULT_THEME' => 'default',
    'LOAD_EXT_CONFIG' => 'alipay',
);