<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace User\Widget;
use Think\Controller;
class InfoWidget extends Controller{

        public function infos(){
            $info = M('member')->field('nickname,amount,score,face')->where(array('id'=>get_cookie('uid')))->find();
            $this->assign('info',$info);
            $this->display('Public:info');
        }

}