<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace User\Controller;
use Think\Controller;
class CommentController extends CommonController {

    public function index(){
        header("Content-Type:text/html; charset=utf-8");
        $uid=get_cookie('uid');
        if (!$uid) {
            $this->error('非法操作！');
        }
        $count=$comments=M('comment')->where(array('userid'=>$uid))->count();
        $page = new \Think\Page($count, 15);
        $page->setConfig('header','共 %totalRow% 条记录');
        $page->setConfig('theme','%upPage% %linkPage% %downPage% %nowPage%/%totalPage%页 %header% ');
        $limit = $page -> firstRow . ',' . $page -> listRows;


        $comments=M('comment')->field('id,postid,title,posttime,status,cid,ename')->where(array('userid'=>$uid))->limit($limit)->select();

        $this -> page = $page -> show();
        $this->comments=$comments;
        $this->display();
    }

    public function del(){
        $uid=get_cookie('uid');
        if (!$uid) {
            $this->error('非法操作！');
        }
        $id=I('get.id','','intval');
        $num=M('comment')->where(array('id'=>$id,'userid'=>$uid))->delete();
        if ($num) {
            $this->success('删除成功！');
        }else{
            $this->error('非法操作！');
        }
    }

    public function look(){
        $uid=get_cookie('uid');
        if (!$uid) {
            $this->error('非法操作！');
        }
        $id=I('get.id','','intval');
        $content=M('comment')->where(array('id'=>$id,'userid'=>$uid))->getField('content');
        $this->content=$content;
        $this->display();
    }

}