<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace User\Controller;

use Think\Controller;

class AlipayController extends Controller
{
    public function _initialize()
    {
        vendor('Alipay.Corefunction');
        vendor('Alipay.Md5function');
        vendor('Alipay.Notify');
        vendor('Alipay.Submit');
    }

    //doalipay方法
    public function doalipay()
    {

        header("Content-type:text/html;charset=utf-8");

        $alipay_config = C('alipay_config');

        //商户订单号
        //$out_trade_no = $_POST['WIDout_trade_no'];
        $out_trade_no = I('WIDout_trade_no', '');
        //商户网站订单系统中唯一订单号，必填
        //付款金额
        //$total_fee = $_POST['WIDtotal_fee'];
        $total_fee = I('WIDtotal_fee', '');

        //print_r($alipay_config);die;
        //如直接从购物车进来，保存好收货信息
        $isnew = I('post.isnew', '', 'intval');


        $datas['getname'] = I('post.get_name', '');
        $datas['shouhuo'] = I('post.get_shouhuo', '');
        $datas['youbian'] = I('post.get_youbian', '');
        $datas['phone'] = I('post.get_phone', '');
        $datas['beizhu'] = I('post.beizhu', '');

        //print_r($datas);die;

        $ischongzhi = I('post.ischongzhi', '', 'intval');
        //如果是会员余额充值
        if ($ischongzhi) {
            $data['userid'] = get_cookie('uid');
            $data['username'] = get_cookie('nickname');
            $data['ordid'] = I('WIDout_trade_no', '');
            $data['ordtime'] = time();
            $data['ordtitle'] = I('WIDsubject', '');
            $data['ordfee'] = I('WIDtotal_fee', '');

            $cz = M('orderlist')->where(array('userid' => get_cookie('uid')))->add($data);
            if (!$cz) {
                $this->error('充值信息写入失败！');
            }


        } else {
            if (empty($datas['getname']) || empty($datas['shouhuo']) || empty($datas['youbian']) || empty($datas['phone'])) {
                $this->error('收货信息必须填完整！');
            }
        }
        //保存收货信息
        if ($isnew) {
            //print_r($isnew);die;
            $num = M('orderlist')->where(array('ordid' => $out_trade_no, 'userid' => get_cookie('uid')))->save($datas);
            //print_r($num);die;

        }

        //支付类型
        $payment_type = "1";
        //必填，不能修改
        //服务器异步通知页面路径
        $notify_url = C('alipay.notify_url');
        //需http://格式的完整路径，不能加?id=123这类自定义参数

        //页面跳转同步通知页面路径
        $return_url = C('alipay.return_url');
        //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

        //卖家支付宝帐户
        $seller_email = C('alipay.seller_email');;
        //必填


        $order = M('orderlist')->field('id,ordfee,ordstatus,ordtime')->where(array('ordid' => $out_trade_no, 'userid' => get_cookie('uid')))->find();
        if (!$order['id']) {
            $this->error('不存在此订单！');
        }
        if ($order['ordstatus']) {
            $this->error('此订单已付款！');
        }
        if ($order['ordfee'] != $total_fee) {
            $this->error('付款金额出错！！！');
        }

        //必填

        /**
         * 再次处理商品订单
         */
        //付款金额


        $amount = M('member')->where(array('id' => get_cookie('uid')))->getField('amount');

        //直接从余额中扣除
        if ($amount >= $order['ordfee'] & substr($out_trade_no, 0, 2) != "cz") {

            $mdata['amount'] = $amount - $order['ordfee'];
            if (M('member')->where(array('id' => get_cookie('uid')))->save($mdata)) {
                $updata['ordstatus'] = 1;
                $up = M('orderlist')->where(array('ordid' => $out_trade_no, 'userid' => get_cookie('uid')))->save($updata);
                if ($up) {

                    $ld['orderid'] = $out_trade_no;
                    $ld['orderfee'] = $order['ordfee'];
                    $ld['creattime'] = $order['ordtime'];
                    $ld['userid'] = get_cookie('uid');
                    $ld['username'] = get_cookie('nickname');
                    $ld['dtype'] = 0;
                    $log = M('amlog')->add($ld);

                    if ($log) {
                        $this->success('成功从余额支付！', U('Order/payed'));
                    }


                } else {
                    $this->error('从余额支付失败！', U('Order/unpay'));
                }
            } else {
                $this->error('从余额扣除失败！');
            }

            //跑到支付宝支付
        } else {


            //订单名称
            //$subject = $_POST['WIDsubject'];
            $subject = I('WIDsubject', '');
            //必填


            //订单描述

            //$body = $_POST['WIDbody'];
            $body = I('WIDbody', '');
            //商品展示地址
            //$show_url = $_POST['WIDshow_url'];
            $show_url = I('WIDshow_url', '');
            //需以http://开头的完整路径，例如：http://www.xxx.com/myorder.html

            //防钓鱼时间戳
            $anti_phishing_key = "";
            //若要使用请调用类文件submit中的query_timestamp函数

            //客户端的IP地址
            $exter_invoke_ip = get_client_ip();
            //非局域网的外网IP地址，如：221.0.0.1
            //


            /************************************************************/

            //构造要请求的参数数组，无需改动
            $parameter = array(
                "service" => "create_direct_pay_by_user",
                "partner" => trim($alipay_config['partner']),
                "payment_type" => $payment_type,
                "notify_url" => $notify_url,
                "return_url" => $return_url,
                "seller_email" => $seller_email,
                "out_trade_no" => $out_trade_no,
                "subject" => $subject,
                "total_fee" => $total_fee,
                "body" => $body,
                "show_url" => $show_url,
                "anti_phishing_key" => $anti_phishing_key,
                "exter_invoke_ip" => $exter_invoke_ip,
                "_input_charset" => trim(strtolower($alipay_config['input_charset']))
            );

            //建立请求
            $alipaySubmit = new \AlipaySubmit($alipay_config);
            $html_text = $alipaySubmit->buildRequestForm($parameter, "post", "确认");
            //$html_text = $alipaySubmit->buildRequestForm($parameter,"post", "确认");
            echo $html_text;
        }

    }

    //服务器异步通知页面方法
    public function notifyurl()
    {
        //header("Content-type:text/html;charset=utf-8");
        $alipay_config = C('alipay_config');
        //计算得出通知验证结果
        $alipayNotify = new \AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();

        if ($verify_result) {


            //验证成功
            //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
            $out_trade_no = $_POST['out_trade_no']; //商户订单号
            $trade_no = $_POST['trade_no']; //支付宝交易号
            $trade_status = $_POST['trade_status']; //交易状态
            $total_fee = $_POST['total_fee']; //交易金额
            $notify_id = $_POST['notify_id']; //通知校验ID。
            $notify_time = $_POST['notify_time']; //通知的发送时间。格式为yyyy-MM-dd HH:mm:ss。
            $buyer_email = $_POST['buyer_email']; //买家支付宝帐号；
            $parameter = array(
                "out_trade_no" => $out_trade_no, //商户订单编号；
                "trade_no" => $trade_no, //支付宝交易号；
                "total_fee" => $total_fee, //交易金额；
                "trade_status" => $trade_status, //交易状态
                "notify_id" => $notify_id, //通知校验ID。
                "notify_time" => $notify_time, //通知的发送时间。
                "buyer_email" => $buyer_email, //买家支付宝帐号；
            );


            if ($_POST['trade_status'] == 'TRADE_FINISHED') {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //该种交易状态只在两种情况下出现
                //1、开通了普通即时到账，买家付款成功后。
                //2、开通了高级即时到账，从该笔交易成功时间算起，过了签约时的可退款时限（如：三个月以内可退款、一年以内可退款等）后。

                //调试用，写文本函数记录程序运行情况是否正常
                //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //该种交易状态只在一种情况下出现——开通了高级即时到账，买家付款成功后。

                //调试用，写文本函数记录程序运行情况是否正常
                //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
                if (!checkorderstatus($out_trade_no)) {
                    orderhandle($parameter);
                    //进行订单处理，并传送从支付宝返回的参数；
                }
            }

            //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

            echo "success"; //请不要修改或删除

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        } else {
            //验证失败
            echo "fail";

            //调试用，写文本函数记录程序运行情况是否正常
            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
        }

    }


    //页面跳转处理方法；
    public function returnurl()
    {
        header("Content-type:text/html;charset=utf-8");
        $alipay_config = C('alipay_config');
        $alipayNotify = new \AlipayNotify($alipay_config); //计算得出通知验证结果
        $verify_result = $alipayNotify->verifyReturn();
        if ($verify_result) {
            //验证成功
            //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表
            $out_trade_no = $_GET['out_trade_no']; //商户订单号
            $trade_no = $_GET['trade_no']; //支付宝交易号
            $trade_status = $_GET['trade_status']; //交易状态
            $total_fee = $_GET['total_fee']; //交易金额
            $notify_id = $_GET['notify_id']; //通知校验ID。
            $notify_time = $_GET['notify_time']; //通知的发送时间。
            $buyer_email = $_GET['buyer_email']; //买家支付宝帐号；

            $parameter = array(
                "out_trade_no" => $out_trade_no, //商户订单编号；
                "trade_no" => $trade_no, //支付宝交易号；
                "total_fee" => $total_fee, //交易金额；
                "trade_status" => $trade_status, //交易状态
                "notify_id" => $notify_id, //通知校验ID。
                "notify_time" => $notify_time, //通知的发送时间。
                "buyer_email" => $buyer_email, //买家支付宝帐号
            );

            if ($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
                if (!checkorderstatus($out_trade_no)) {
                    orderhandle($parameter); //进行订单处理，并传送从支付宝返回的参数；
                }
                $this->redirect(C('alipay.successpage')); //跳转到配置项中配置的支付成功页面；
            } else {
                echo "trade_status=" . $_GET['trade_status'];
                $this->redirect(C('alipay.errorpage')); //跳转到配置项中配置的支付失败页面；
            }
        } else {
            //验证失败
            //如要调试，请看alipay_notify.php页面的verifyReturn函数
            echo "支付失败！";
        }
    }

}