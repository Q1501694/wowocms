<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace User\Controller;

use Think\Controller;

class CenterController extends CommonController
{

    public function index()
    {
        $uid = get_cookie('uid');
        $user = D('MemberView')->find($uid);
        if (!$user) {
            del_cookie(array('name' => 'uid'));
            del_cookie(array('name' => 'email'));
            del_cookie(array('name' => 'nickname'));
            del_cookie(array('name' => 'logintime'));
            del_cookie(array('name' => 'loginip'));
            del_cookie(array('name' => 'status'));
            $this->error('请重新登录', U('User/Index/index'));
        }
        $user['detail'] = M('memberdetail')->where(array('userid' => $uid))->find();
        if (empty($user['detail'])) {
            $user['detail'] = array(
                'realname' => '还没设置',
                'sex' => '保密',
                'birthday' => '0000-00-00',

            );
        } else {
            $user['detail']['sex'] = $user['detail']['sex'] ? '女士' : '男士';
        }


        $this->user = $user;
        $this->display();
    }

    public function detail()
    {
        $uid = get_cookie('uid');
        if (!$uid) {
            $this->error('非法操作！');
        }
        $new = I('new', '', 'intval');
        $userdetail = M('memberdetail')->where(array('userid' => $uid))->find();
        if (!$userdetail) {
            $userdetail['birthday'] = time();
            $userdetail['new'] = 1;
        } else {
            $userdetail['new'] = 0;
        }

        if (IS_POST) {
            $new = I('post.new');
            $nickname = I('post.nickname', '');
            $realname = I('post.realname', '');
            $sex = I('post.sex', '', 'intval');
            $birthday = I('post.birthday', '');
            $qq = I('post.qq', '');
            $phone = I('post.phone', '');
            $birthday = strtotime($birthday);
            //print_r($birthday);die;
            $data = array(
                'userid' => $uid,
                'realname' => $realname,
                'sex' => $sex,
                'birthday' => $birthday,
                'qq' => $qq,
                'phone' => $phone
            );
            if ($new) {
                $num = M('member')->where(array('id' => $uid))->save(array('nickname' => $nickname));
                $numm = M('memberdetail')->where(array('userid' => $uid))->add($data);
                if ($num || $numm) {
                    del_cookie(array('name' => 'nickname'));
                    set_cookie(array('name' => 'nickname', 'value' => $nickname));
                    $this->success('修改成功！');
                } else {
                    $this->error('修改失败！');
                }
            } else {
                $num = M('member')->where(array('id' => $uid))->save(array('nickname' => $nickname));
                $numm = M('memberdetail')->where(array('userid' => $uid))->save($data);
                if ($num || $numm) {
                    del_cookie(array('name' => 'nickname'));
                    set_cookie(array('name' => 'nickname', 'value' => $nickname));
                    $this->success('修改成功！');
                } else {
                    $this->error('修改失败！');
                }
            }

        }
        $this->user = $userdetail;
        $this->display();
    }

    public function face()
    {
        $uid = get_cookie('uid');
        if (IS_POST) {

            $face = I('post.face', '');
            $num = M('member')->where(array('id' => $uid))->save(array('face' => $face));
            if ($num) {
                del_cookie(array('name' => 'face'));
                set_cookie(array('name' => 'face', 'value' => $face));
                $this->success('修改成功！');
            } else {
                $this->error('修改失败！');
            }
        }
        $face = M('member')->where(array('id' => $uid))->getField('face');

        $this->face = $face;
        $this->display();
    }

    public function pwd()
    {
        $uid = get_cookie('uid');
        if (IS_POST) {
            $oldpwd = I('post.oldpwd');
            $newpwd = I('post.newpwd');
            $rnewpwd = I('post.rnewpwd');

            if (empty($oldpwd) || empty($newpwd) || empty($rnewpwd)) {
                $this->error('三个输入框都必须填写内容！');
            }
            if (strlen($newpwd) < 6) {
                $this->error('密码必须6位数以上！');
            }
            if ($newpwd != $rnewpwd) {
                $this->error('两次密码不一样，请重新填写！');
            }
            $self = M('member')->field(array('email', 'password', 'encrypt'))->where(array('id' => $uid))->find();
            if (!self) {
                $this->error('用户不存在，请重新登录');
            }

            if (get_password($oldpwd, $self['encrypt']) != $self['password']) {
                $this->error('旧密码错误');
            }
            $passwordinfo = get_password($newpwd);
            $data = array(
                'id' => $uid,
                'password' => $passwordinfo['password'],
                'encrypt' => $passwordinfo['encrypt']
            );
            if (false !== M('member')->save($data)) {
                R('User/Index/logout');
                $this->success('修改密码成功!请重新登录！', U('User/Index/index'));
            } else {
                $this->error('修改密码失败');
            }
        }
        $this->display();
    }

    public function amount()
    {
        $amount = M('member')->where(array('id' => get_cookie('uid')))->getField('amount');
        $count = M('amlog')->where(array('userid' => get_cookie('uid')))->count();
        $page = new \Think\Page($count, 15);
        $page->setConfig('header', '共 %totalRow% 条记录');
        $page->setConfig('theme', '%upPage% %linkPage% %downPage% %nowPage%/%totalPage%页 %header% ');
        $limit = $page->firstRow . ',' . $page->listRows;
        $logs = M('amlog')->where(array('userid' => get_cookie('uid')))->order('creattime desc')->limit($limit)->select();
        $this->page = $page->show();
        $this->logs = $logs;
        $this->amount = $amount;
        $this->display();
    }

    public function score()
    {
        if (IS_POST) {
            $this->scoreHandle();
        }
        $score = M('member')->where(array('id' => get_cookie('uid')))->getField('score');
        $this->score = $score;
        $this->display();
    }

    public function scoreHandle()
    {
        $score = I('post.score', '', 'intval');
        if ($score == 0 || $score % 10 != 0 || $score < 10) {
            $this->error('充值积分必须不能为0，且只能以10为单位！');
        }
        //print_r($score/10);die;
        $db = M('member');
        $amount = $db->where(array('id' => get_cookie('uid')))->getField('amount');
        $uscore = $db->where(array('id' => get_cookie('uid')))->getField('score');
        if ($amount - $score % 10 < 0) {
            $this->error('余额不足，请先充值账户余额！');
        }
        $data['score'] = $uscore + $score;
        $num = $db->where(array('id' => get_cookie('uid')))->save($data);
        if ($num) {
            $da['amount'] = $amount - $score / 10;
            $mm = $db->where(array('id' => get_cookie('uid')))->save($da);
            if ($mm) {
                $l['orderid'] = "充值积分订单";
                $l['orderfee'] = $score / 10;
                $l['creattime'] = time();
                $l['userid'] = get_cookie('uid');
                $l['username'] = get_cookie('nickname');
                $l['dtype'] = 0;
                $logs = M('amlog')->add($l);
                if ($logs) {
                    $this->success('充值积分成功！');
                } else {
                    $this->error('充值失败！');
                }
            } else {
                $this->error('充值失败！');
            }
        } else {
            $this->error('_充值失败！');
        }
    }


}