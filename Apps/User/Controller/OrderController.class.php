<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace User\Controller;
use Think\Controller;
class OrderController extends CommonController {

    public function index(){

        
        $id=I('get.id','','intval');
        if (!$id) {
            $this->error('参数错误！');
        }
        $shop=M('shopcart')->where(array('id'=>$id,'user_id'=>get_cookie('uid'),'istolist'=>1))->find();
        //print_r($shop);die;
        if ($shop) {
            
            /*$isinorder=M('orderlist')->where(array('productid'=>$shop['product_id'],'userid'=>get_cookie('uid')))->getField('id');
            if ($isinorder) {
                $this->order=M('orderlist')->where(array('id'=>$isinorder))->find();
                $this->display();
            }else{*/
                $data['userid']=get_cookie('uid');
                $data['username']=get_cookie('nickname');
                $data['ordid']=build_order_no();
                $data['ordtime']=time();
                $data['productid']=$shop['product_id'];
                $data['ordtitle']=$shop['product_title'];
                $data['ordbuynum']=$shop['num'];
                $data['ordprice']=$shop['product_price'];
                $data['ordfee']=$data['ordbuynum']*$data['ordprice'];
                $data['ordstatus']=0;
                $num=M('orderlist')->add($data);
                if ($num) {
                    //购物车已生成订单
                    M('shopcart')->where(array('id'=>$id,'user_id'=>get_cookie('uid')))->save(array('istolist'=>2));
                    $this->order=M('orderlist')->where(array('id'=>$num))->find();
                    $this->display();

                }else{
                    $this->error('创建订单出错！');
                }
           /* }*/


        }else{
            $this->error('参数错误！');
        }
        

    }

    public function lists(){

        $count=M('orderlist')->where(array('userid'=>get_cookie('uid')))->count();
        $page = new \Think\Page($count, 15);
        $page->setConfig('header','共 %totalRow% 条记录');
        $page->setConfig('theme','%upPage% %linkPage% %downPage% %nowPage%/%totalPage%页 %header% ');
        $limit = $page -> firstRow . ',' . $page -> listRows;

        $list=M('orderlist')->where(array('userid'=>get_cookie('uid')))->order('id desc')->limit($limit)->select();
        $this -> page = $page -> show();
        $this->list=$list;
        $this->display('list');
    }

    //成功支付
    public function payed(){

        $count=M('orderlist')->where(array('userid'=>get_cookie('uid'),'ordstatus'=>1))->count();
        $page = new \Think\Page($count, 15);
        $page->setConfig('header','共 %totalRow% 条记录');
        $page->setConfig('theme','%upPage% %linkPage% %downPage% %nowPage%/%totalPage%页 %header% ');
        $limit = $page -> firstRow . ',' . $page -> listRows;

        $list=M('orderlist')->where(array('userid'=>get_cookie('uid'),'ordstatus'=>1))->order('id desc')->limit($limit)->select();
        $this -> page = $page -> show();
        $this->list=$list;
        $this->display('payed');
    }

    //支付失败
    public function unpay(){
        $count=M('orderlist')->where(array('userid'=>get_cookie('uid'),'ordstatus'=>0))->count();
        $page = new \Think\Page($count, 15);
        $page->setConfig('header','共 %totalRow% 条记录');
        $page->setConfig('theme','%upPage% %linkPage% %downPage% %nowPage%/%totalPage%页 %header% ');
        $limit = $page -> firstRow . ',' . $page -> listRows;
        $list=M('orderlist')->where(array('userid'=>get_cookie('uid'),'ordstatus'=>0))->order('id desc')->limit($limit)->select();
        $this -> page = $page -> show();
        $this->list=$list;
        $this->display('unpay');
    }

    //订单列表支付
    public function pay(){
        $id=I('get.id','','intval');
        if (!$id) {
            $this->error('参数错误！');
        }
        $order=M('orderlist')->field('ordid,ordtitle,ordprice,ordbuynum,ordfee,getname,shouhuo,youbian,phone,beizhu,ordstatus')->where(array('id'=>$id,'userid'=>get_cookie('uid')))->find();
        if (!$order['ordid']) {
            $this->error('无此订单！请重新下单！');
        }
        if ($order['ordstatus']) {
            $this->error('此订单已付款，毋须再次付款！');
        }
        $this->order=$order;
        $this->display();
    }

    public function del(){
        $id=I('get.id','','intval');
        if (!$id) {
            $this->error('参数错误！');
        }
        $num=M('orderlist')->where(array('userid'=>get_cookie('uid'),'id'=>$id))->delete();
        if ($num) {
            $this->success('删除成功！');
        }else{
            $this->error('删除失败！');
        }
    }

}