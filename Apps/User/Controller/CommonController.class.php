<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace User\Controller;
use Think\Controller;
class CommonController extends Controller {

    public function _initialize() {
    	if(C('cfg_user_auto')!=1){
    		$this->error('会员中心暂时关闭');
    	}
        $uid = intval(get_cookie('uid'));
        if (empty($uid)) {
            //$this->redirect('User/Index/index');
            $this->error('请先登录！',U('User/Index/index'));
        }
    }

}