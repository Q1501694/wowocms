<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace User\Controller;

use Think\Controller;

class CartController extends CommonController
{

    public function index()
    {
        $carts = M('shopcart')->where(array('user_id' => get_cookie('uid'), 'istolist' => 1))->order('id desc')->select();
        //print_r($carts);die;
        $this->carts = $carts;
        $this->display();
    }

    public function add()
    {

        if (!IS_POST) {
            exit;
        }
        $product_id = I('post.product_id', '', 'intval');
        if (!$product_id) {
            $this->error('非法传参！');
        }
        $isproduct = M('product')->where(array('id' => $product_id))->getField('id');
        if (!isproduct) {
            $this->error('非法传参！');
        }
        $uid = get_cookie('uid');
        if (!$uid) {
            $this->error('非法传参！');
        }
        $isin = M('shopcart')->field('id,num')->where(array('product_id' => $product_id, 'user_id' => $uid))->find();

        if ($isin['id'] & $isin['istolist'] != 0) {
            $isin['num'] = $isin['num'] + 1;
            $num = M('shopcart')->where(array('product_id' => $product_id, 'user_id' => $uid))->save($isin);
            if ($num) {
                $this->success('成功添加到购物车！');
            } else {
                $this->error('添加到购物车失败！');
            }
        } else {
            $data['product_id'] = $product_id;
            $data['product_title'] = I('post.product_title', '');
            $data['product_price'] = I('post.product_price', '');
            $data['num'] = 1;
            $data['user_id'] = $uid;
            $data['istolist'] = 1;
            $num = M('shopcart')->add($data);
            if ($num) {
                $this->success('成功添加到购物车！');
            } else {
                $this->error('添加到购物车失败！');
            }
        }
    }

    public function updateCart()
    {
        $id = I('post.id', '', 'intval');
        $num = I('post.num', '', 'intval');

        $data['num'] = $num;

        $mm = M('shopcart')->where(array('id' => $id, 'user_id' => get_cookie('uid')))->save($data);
        if ($mm) {
            $this->success('成功更新购物车！');
        } else {
            $this->error('更新失败！');
        }
    }

    public function del()
    {
        $id = I('get.id', '', 'intval');
        $data['istolist'] = 0;
        $num = M('shopcart')->where(array('id' => $id, 'user_id' => get_cookie('uid')))->save($data);
        if ($num) {
            $this->success('成功删除！');
        } else {
            $this->error('删除失败！');
        }
    }

}