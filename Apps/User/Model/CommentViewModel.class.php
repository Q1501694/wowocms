<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace User\Model;
use Think\Model\ViewModel;
//视图模型
class CommentViewModel extends ViewModel {

    protected $viewFields = array(
        'comment' => array('id', 'postid', 'modelid', 'title', 'username', 'email', 'ip',
        'agent', 'content', 'posttime', 'status', 'pid', 'userid','cid','ename',
        '_type' => 'LEFT'
        ),
        'model' => array(
        'name' => 'modelname',
        'tablename' => 'tablename',
        '_on' => 'comment.modelid = model.id',//_on 对应上面LEFT关联条件
        '_type' => 'LEFT'
        ),      
        'member' => array(
        'face' => 'face',//显示字段name as model
        'nickname' => 'nickname',//显示字段name as model
        '_on' => 'comment.userid = member.id',//_on 对应上面LEFT关联条件
        ),


    );
}