<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace User\Model;
use Think\Model;
class MemberModel extends Model{

   protected $_validate = array(
            array('uname','require','电子邮箱必须填写！'),
           // array('uname','email','邮箱格式不符合要求。'), 
            array('upwd','require','密码必须填写！'), 
            array('rupwd','require','确认密码必须填写！'), 
            array('upwd','rupwd','两次密码不一致',0,'confirm'),
            array('email','unique','邮箱已经存在！',0,'unique'), //使用这个是否存在，auto就不能自动完成
    );

}