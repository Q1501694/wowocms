<?php
//在线交易订单支付处理函数
//函数功能：根据支付接口传回的数据判断该订单是否已经支付成功；
//返回值：如果订单已经成功支付，返回true，否则返回false；
function checkorderstatus($ordid){
    $Ord=M('orderlist');
    $ordstatus=$Ord->where(array('ordid'=>$ordid))->getField('ordstatus');
    if($ordstatus==1){
        return true;
    }else{
        return false;    
    }
}

//处理订单函数
//更新订单状态，写入订单支付后返回的数据
function orderhandle($parameter){
    $ordid=$parameter['out_trade_no'];
    $data['payment_trade_no']      =$parameter['trade_no'];
    $data['payment_trade_status']  =$parameter['trade_status'];
    $data['payment_notify_id']     =$parameter['notify_id'];
    $data['payment_notify_time']   =$parameter['notify_time'];
    $data['payment_buyer_email']   =$parameter['buyer_email'];
    $data['ordstatus']             =1;
    $Ord=M('orderlist');
    $Ord->where(array('ordid'=>$ordid))->save($data);

    $infos=$Ord->field('userid,username')->where(array('ordid'=>$ordid))->find();


    $datas['orderid']=$ordid;
    $datas['orderfee']=$parameter['total_fee'];
    $datas['creattime']=time();
    $datas['userid']=$infos['userid'];
    $datas['username']=$infos['username'];

    //如果是充值
    if (substr($ordid,0,2)=="cz") {        
        $datas['dtype']=1;
        $amount=M('member')->where(array('id'=>$datas['userid']))->getField('amount');
        $mdata['amount']=$amount+$datas['orderfee'];       
        $addam=M('member')->where(array('id'=>$datas['userid']))->save($mdata); 
    }else{
        $datas['dtype']=0;  
    }
    $log=M('amlog')->add($datas);
}






?>