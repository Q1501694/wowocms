<?php
return array(
    'TAGLIB_BUILD_IN' => 'Cx,Wowocms',
    'TAGLIB_PRE_LOAD' => 'wowocms',
    'DEFAULT_FILTER' => 'strip_tags,stripslashes',
    'DEFAULT_THEME' => 'wap002',
    'URL_ROUTER_ON' => true,
    'HTML_CACHE_ON' => C('HTML_CACHE_ON__NOBILE'),
    'TMPL_PARSE_STRING' => array(
        '__MOBILE__' => __ROOT__. '/moblie',
        //'__MSTYLE__' => __ROOT__. '/moblie',
     ),
    
);