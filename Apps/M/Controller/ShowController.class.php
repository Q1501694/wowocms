<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace M\Controller;
use Think\Controller;
class ShowController extends Controller {
    public function index(){
        $id = I('id', 0, 'intval');
        $cid = I('cid', 0,'intval');
        $ename = I('e', '', 'htmlspecialchars,trim');
        if ($id == 0) {
            $this->error('参数错误');
        }
        $cate = getCategory(1);
        $Category = new \Think\Category();
        if (!empty($ename)) {//ename不为空
            $self = $Category::getSelfByEName($cate, $ename);//当前栏目信息
        }else {//$cid来判断
            $self = $Category::getSelf($cate, $cid);//当前栏目信息
        }       
        if(empty($self)) {
            $this->error('栏目不存在');
        }
        $cid = $self['id'];//当使用ename获取的时候，就要重新给$cid赋值，不然0
        $_GET['cid'] = $cid;//栏目ID
        $self['url'] = getUrl($self);

        $patterns = array('/.html$/');
        $replacements = array('', '');
        $template_show = preg_replace($patterns, $replacements, $self['template_show']);
        if (empty($template_show)) {
            $this->error('模板不存在');
        }

        $content = M($self['tablename'])->where(array('status' => 0, 'id' => $id))->find();
        $addon_content=M("addon".$self['tablename'])->where(array('aid'=>$id))->find();
        unset($addon_content['aid']);//剔除aid，无用
        
        //检查是否有html编辑框的字段，因为需要解码
        $ml=M('model_field');
        $html_builder=$ml->field('field')->where("model_id =%d  and type='%s' or type='%s'",$self['modelid'],'multitext','htmltext')->group('FIELD')->select();
        for ($i=0; $i <count($html_builder) ; $i++) { 
            $key=$html_builder[$i]['field'];
            if (array_key_exists($key,$addon_content)) {
                $addon_content[$key]=htmlspecialchars_decode($addon_content[$key]);
            }
        }
        //p($addon_content);
        //检查是否是多图上传字段，反序列化之后返回数据
        $imgfiles=$ml->field('field')->where(array('type'=>'imgfile'))->group('FIELD')->select();
        for ($i=0; $i <count($imgfiles) ; $i++) { 
            $key=$imgfiles[$i]['field'];
            if (array_key_exists($key,$addon_content)) {
                $pictureurls_arr = empty($addon_content[$key]) ? array() : explode('|||',$addon_content[$key]);
                $pictureurls  = array();
                    foreach ($pictureurls_arr as $v) {
                        $temp_arr = explode('$$$', $v);
                        if (!empty($temp_arr[0])) {
                            $pictureurls[] = array(
                                'url' => $temp_arr[0],
                                'alt' => $temp_arr[1]
                            );
                        }               
                    }
                $addon_content[$key] = $pictureurls;
            }
        }
        if ($self['tablename']!='download') {
            $content=array_merge($content,$addon_content);
        }
        // p($content);

        if (empty($content)) {
            $this->error('内容不存在');
        }
        //当前url
        $_jumpflag = ($content['flag'] & B_JUMP) == B_JUMP? true : false;
        $content['url'] = getContentUrl($content['id'], $content['cid'], $self['ename'], $_jumpflag, $content['jumpurl']);

        switch ($self['tablename']) {
            case 'download':
                //下载地址:
                $downlink_arr = empty($content['downlink']) ? array() : explode('|||', $content['downlink']);       
                $downlink  = array();
                foreach ($downlink_arr as $v) {
                    $temp_arr = explode('$$$', $v);
                    if (!empty($temp_arr[1])) {
                        $downlink[] = array(
                            'url' => $temp_arr[1],
                            'title' => $temp_arr[0]
                        );
                    }               
                }
                $content['downlink'] = $downlink;           
                break;          
            default:
                break;
        }
        $this->cate = $self;
        $this->title = $content['title']."-".$self['name'];
        $this->keywords = $content['keywords'];
        $this->description = empty($content['description'])? $content['title']: $content['description'];
        $this->commentflag = $content['commentflag'];//是否允许评论,debug,以后加上个全局评价 $content['commentflag'] && CFG_Comment
        $this->content = $content;
        $this->tablename = $self['tablename'];
        $this->id = $id;
        $this->cid=$content['cid'];
        $this->display($template_show); 
    }


    //增加点击数
    public function click(){    
        $id = I('id', 0, 'intval');
        $tablename = I('tn', '');
        if (C('HTML_CACHE_ON') == true) {
            echo 'document.write('. getClick($id, $tablename) .')';
        }
        else {
            echo getClick($id, $tablename);
        }
        
    }

        //click +1
    public function clicknum(){
        $id = I('id', 0, 'intval');
        $tablename = I('tablename', '');
        if (empty($id) || empty($cid)) {
            exit();
        }

        $Category = new \Think\Category();
        $self = $Category::getSelf(getCategory(1), $cid);//当前栏目信息
        
        if(empty($self)) {
            //$this->error('栏目不存在');
            exit();
        }
        $num = M($self['tablename'])->where(array('id' => $id))->getField('click');
        M($self['tablename'])->where(array('id' => $id))->setInc('click');
        echo $num;
    }
}