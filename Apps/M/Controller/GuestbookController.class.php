<?php
// +----------------------------------------------------------------------
// | WWW.0771MC.COM 广西南宁市铭成龙毅网络科技有限公司 出品
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://WWW.0771MC.COM All rights reserved.
// +----------------------------------------------------------------------
// | Author: 铭成龙毅 <service@0771mc.com> <http://www.0771MC.com>
// +----------------------------------------------------------------------
namespace M\Controller;
use Think\Controller;
class GuestbookController extends Controller {

    public function index(){
        $this->display();
    }

    public function add(){

        
            $username=I('post.title','','strip_tags');
            $tel=I('post.tel',0,'intval');
            $content=I('post.content','','strip_tags');
            
            if(empty($username)){
                $this->error('用户名不能为空！');
            }
            if(empty($content)){
                $this->error('留言内容不能为空！');
            }
            
            

                $arr = array(
                'username' => $username, 
                'tel' => $tel, 
                'ip' => get_client_ip(), 
                'posttime' => time(), 
                'content' => $content, 
                'status' => 0, 
                );
                $num=M('guestbook')->add($arr);

                if ($num) {    
                    //$this->success('留言成功！我们会尽快和您取得联系！');        
                    //$data=1;
                    //------留言成功发送通知邮件
                if (C('cfg_book_auto')) {
                $email=C('cfg_email_get');
                $subject = "铭成龙毅企业管理系统留言信息通知";
                $login_ip=get_client_ip();
                $login_time=date('Y-m-d H:i:s', time());

$message = <<<str
<p>网站留言信息通知！</p>
<p>姓名：{$username}</p>
<p>电话：{$tel}</p>
<p>时间：{$login_time}</p>
<p>留言内容：{$content}</p>
str;
if (SendMail($email, $subject , $message) == true){

}
}
              //------登录成功发送通知邮件 
                  echo json_encode(array('success'=>1,'msg'=>"留言成功！我们会尽快和您取得联系！"));
                  //$this->ajaxReturn(array('success'=>1),"留言成功！我们会尽快和您取得联系！",1,1);
                }else{
                    $data=0;
                   // $this->error('留言失败！请通过网站其他联系方式和我们取得联系！');
                    echo json_encode(array('success'=>0,'msg'=>"留言失败！请通过网站其他联系方式和我们取得联系！"));
                }
                // echo $data;

       
        
    }

}
?>