<?php
namespace Think\Template\TagLib;
use Think\Template\TagLib;
class Mc extends TagLib{

        protected $tags = array(
        //自定义标签
            'flink' => array(
                'attr'  => 'orderby,limit',//attr 属性列表
                'close' => 1,
            ),
            //通用列表  设置参数arcid 必须同时设置typeid
            'list'  => array(
                'attr'  => 'flag,typeid,arcid,titlelen,infolen,model,orderby,keyword,limit,pagesize,pageroll,pagetheme,moreinfo,notintype,cache',
                'close' => 1,
            ),
            'sql' =>array(
                'attr' =>'sql,return,cache,num',
                'close'=>1,
            ),  
            //软件列表分页
            'download'   => array(
                'attr'  => 'flag,typeid,arcid,titlelen,infolen,orderby,keyword,limit,pagesize,pageroll,pagetheme',
                'close' => 1,
            ),
            //栏目
            'catlist'   => array(
                'attr'  => 'typeid,type,orderby,limit,flag',//flag为是否全部显示
                'close' => 1,
            ),
            //导航
            'navlist'   => array(
                'attr'  => 'typeid',
                'close' => 1,
            ),

            //类名和链接
            'type'  => array(
                'attr'  => 'typeid',
                'close' => 1,
            ),
            'position'  => array(
                'attr'  => 'typeid,ismobile,sname,surl,delimiter',
                'close' => 0,
            ),
            'prev'  => array(
            'attr'  => 'titlelen',//attr 属性列表
            'close' => 0
            ),
            'next'  => array(           
                'attr'  => 'titlelen',//attr 属性列表
                'close' => 0
            ),
            'click' => array('close' => 0),
            'keywords'  => array('close' => 0),
            'description'   => array('close' => 0),
            'webtitle'  => array('close' => 0),
            'tel'  => array('close' => 0),
            'power'  => array('close' => 0),
            'address'  => array('close' => 0),
            'beian'  => array('close' => 0),
            'qq'  => array('close' => 0),
            'tongji'  => array('close' => 0),
            'weburl'  => array('close' => 0),
            'webname'  => array('close' => 0),
            'email'  => array('close' => 0),
            
    );
    public function _email($attr, $content) {
        return C('cfg_email');
    }
    public function _webname($attr, $content) {
        return C('cfg_webname');
    }
    public function _weburl($attr, $content) {
        return C('cfg_weburl');
    }
    public function _tongji($attr, $content) {
        return C('cfg_tongji');
    }
    public function _qq($attr, $content) {
        return C('cfg_qq');
    }
    public function _beian($attr, $content) {
        return C('cfg_beian');
    }
    public function _address($attr, $content) {
        return C('cfg_address');
    }
    public function _power($attr, $content) {
        return C('cfg_powerby');
    }
    public function _tel($attr, $content) {
        return C('cfg_phone');
    }

    public function _keywords($attr, $content) {
        return C('cfg_keywords');
    }
    public function _description($attr, $content) {
        return C('cfg_description');
    }
    public function _webtitle($attr, $content) {
        return C('cfg_webtitle');
    }

    public function _sql($attr, $content){
        $attr['sql'] = $sql = str_replace(array("dm_","@#_"), C("DB_PREFIX"), strtolower($attr['sql']));
        $attr['return'] = $return = empty($attr['return']) ? "data" : $attr['return'];
        $attr['cache'] = $cache = (int) $attr['cache'];
        $attr['num'] = $num = isset($attr['num']) && intval($attr['num']) > 0 ? intval($attr['num']) : 20;
        $parseStr = ' <?php ';
        $parseStr .= ' $cache = ' . $cache . ';';
        $parseStr .= ' $cacheID = biuld_object_id(' . arr_to_html($attr) . ');';
        $parseStr .= ' if(' . $cache . ' && $_return = S( $cacheID ) ){ ';
        $parseStr .= '      $' . $return . '=$_return;';
        $parseStr .= ' }else{ ';
        if (substr(trim($sql), 0, 1) == '$') {
                $parseStr .= ' $_sql = str_replace(array("dm_", "@#_"), C("DB_PREFIX"),' . $sql . ');';
        } else {
            $parseStr .= ' $_sql = "' . str_replace('"', '\"', $sql) . '";';
        }
        $parseStr .= ' $_sql = "' . str_replace('"', '\"', $sql) . '";';
        $parseStr .= ' $get_db = M();';
        $parseStr .= '      $' . $return . '=$get_db->query($_sql." LIMIT ' . $num . ' ");';
        $parseStr .= '      if(' . $cache . '){ S( $cacheID  ,$' . $return . ',$cache); }; ';
        $parseStr .= ' } ';
        $parseStr .= '  ?>';
        $parseStr .= $this->tpl->parse($content);
        return $parseStr;
    }
    //文档下载列表
    public function _download($attr, $content) {
        $attr = $this->parseXmlAttr($attr, 'download');
        $flag = empty($attr['flag'])? '': $attr['flag'];
        $typeid = !isset($attr['typeid']) || $attr['typeid'] == '' ? -1 : trim($attr['typeid']);
        $arcid  = empty($attr['arcid'])? '' : $attr['arcid'];
        $titlelen = empty($attr['titlelen'])? 0 : intval($attr['titlelen']);
        $infolen = empty($attr['infolen'])? 0 : intval($attr['infolen']);       
        $orderby = empty($attr['orderby'])? 'id DESC' : $attr['orderby'];
        $limit = empty($attr['limit'])? '10' : $attr['limit'];
        $pagesize = empty($attr['pagesize'])? '0' : $attr['pagesize'];
        $keyword = empty($attr['keyword'])? '': trim($attr['keyword']);

        //$flag = flag2sum($flag);
        $arcid = string2filter($arcid, ',', true);

        $pageroll = empty($attr['pageroll'])? '5' : $attr['pageroll'];
        $pagetheme = empty($attr['pagetheme'])? ' %upPage% %linkPage% %downPage% 共%totalPage%页' : htmlspecialchars_decode($attr['pagetheme']);
        

        $str = <<<str
<?php
    \$_typeid = $typeid;    
    \$_keyword = "$keyword";
    \$_arcid = "$arcid";
    \$_flag = "$flag";
    if(\$_typeid == -1) \$_typeid = I('cid', 0, 'intval');
    if (\$_typeid>0 || substr(\$_typeid,0,1) == '$') {
        \$Category = new \Think\Category();
        \$ids = \$Category::getChildsId(getCategory(1), \$_typeid, true);
        //p(\$ids);
        \$where = array('download.status' => 0, 'download.cid'=> array('IN',\$ids));
    }else {
        \$where = array('download.status' => 0);
    }

    if (\$_keyword != '') {
        \$where['download.title'] = array('like','%'.\$_keyword.'%');
    }
    if (!empty(\$_arcid)) {
        \$where['download.id'] = array('IN', \$_arcid);
    }

    if(\$_flag != '') {
            \$flags = explode(',', \$_flag);
            for(\$i=0; isset(\$flags[\$i]); \$i++) \$where['_string'] = " FIND_IN_SET('\$flags[\$i]', article.flag)>0 ";
    }

    //分页
    if ($pagesize > 0) {
        
        
        \$count = D('SoftView')->where(\$where)->count();

        \$thisPage = new \Think\Page(\$count, $pagesize);
        
        \$ename = I('e', '', 'htmlspecialchars,trim');
        if (!empty(\$ename) && C('URL_ROUTER_ON') == true) {
            \$thisPage->url = ''.\$ename. '/p';
        }
        //设置显示的页数
        \$thisPage->rollPage = $pageroll;
        \$thisPage->setConfig('theme',"$pagetheme");
        \$limit = \$thisPage->firstRow. ',' .\$thisPage->listRows;  
        \$page = \$thisPage->show();
    }else {
        \$limit = "$limit";
    }
    

    \$_soflist = D('DownloadView')->where(\$where)->order("$orderby")->limit(\$limit)->select();

    if (empty(\$_soflist)) {
        \$_soflist = array();
    }
    

    foreach(\$_soflist as \$autoindex => \$soflist):    
    \$_jumpflag = (\$soflist['flag'] & B_JUMP) == B_JUMP? true : false;
    \$soflist['url'] = getContentUrl(\$soflist['id'], \$soflist['cid'], \$soflist['ename'], \$_jumpflag, \$soflist['jumpurl']);
    if($titlelen) \$soflist['title'] = str2sub(\$soflist['title'], $titlelen, 0);
    if($infolen) \$soflist['description'] = str2sub(\$soflist['description'], $infolen, 0);

?>
str;
    $str .= $content;
    $str .='<?php endforeach;?>';
    return $str;

    }


    //针对内容页
    public function _click($attr, $content) {

        $str =<<<str
<?php

    if (!empty(\$id) && !empty(\$tablename)) {


        //开启静态缓存
        if (C('HTML_CACHE_ON') == true) {
            echo '<script type="text/javascript" src="'.U('Show/click', array('id' => \$id, 'tn' => \$tablename)).'"></script>';
        }
        else {
            echo getClick(\$id, \$tablename);
        }
        
        
    }

?>
str;
        return $str;
    }



public function _prev($attr, $content) {
        $attr = !empty($attr)? $this->parseXmlAttr($attr, 'prev') : null;
        $titlelen = empty($attr['titlelen']) ? 0 : intval($attr['titlelen']);
        $DB_PREFIX=c('DB_PREFIX');
        $str =<<<str
    <?php

    if(empty(\$content['id']) || empty(\$content['cid']) || empty(\$cate['tablename']) ) {
        echo '无记录';
    } else {
        //上一条记录
        \$joins="__CATEGORY__ as Category ON ".$DB_PREFIX.\$cate['tablename'].".cid = Category.id";
        \$field=$DB_PREFIX.\$cate['tablename'].".*,Category.name as catename,Category.ename,Category.modelid";
      
        \$_vo=M(ucfirst(\$cate['tablename']))
        ->join(\$joins,'LEFT')
        ->field(\$field)
        ->where(array($DB_PREFIX.\$cate['tablename'].".status" => 0, 'cid' => \$content['cid'], $DB_PREFIX.\$cate['tablename'].".id" => array('lt',\$content['id'])))
        ->order('id DESC')
        ->find();
        if (\$_vo) {
            \$_jumpflag = (\$_vo['flag'] & B_JUMP) == B_JUMP? true : false;
            \$_vo['url'] = getContentUrl(\$_vo['id'], \$_vo['cid'], \$_vo['ename'], \$_jumpflag, \$_vo['jumpurl']);
            if($titlelen) \$_vo['title'] = str2sub(\$_vo['title'], $titlelen, 0);   
            echo '<a href="'. \$_vo['url'] .'">'. \$_vo['title'] .'</a>';
        } else {
            echo '第一篇';
        }
    }

?>
str;

        return $str;
    }

    public function _next($attr, $content) {
        $attr = !empty($attr)? $this->parseXmlAttr($attr, 'next') : null;
        $titlelen = empty($attr['titlelen']) ? 0 : intval($attr['titlelen']);
        $DB_PREFIX=c('DB_PREFIX');
        $str =<<<str
<?php
    if(empty(\$content['id']) || empty(\$content['cid']) || empty(\$cate['tablename']) ) {
        echo '无记录';
    } else {
        //下一条记录
        \$joins="__CATEGORY__ as Category ON ".$DB_PREFIX.\$cate['tablename'].".cid = Category.id";
        \$field=$DB_PREFIX.\$cate['tablename'].".*,Category.name as catename,Category.ename,Category.modelid";
      
        \$_vo=M(ucfirst(\$cate['tablename']))
        ->join(\$joins,'LEFT')
        ->field(\$field)
        ->where(array($DB_PREFIX.\$cate['tablename'].".status" => 0, 'cid' => \$content['cid'], $DB_PREFIX.\$cate['tablename'].".id" => array('gt',\$content['id'])))
        ->order('id ASC')
        ->find();

        if (\$_vo) {    
            \$_jumpflag = (\$_vo['flag'] & B_JUMP) == B_JUMP? true : false;
            \$_vo['url'] = getContentUrl(\$_vo['id'], \$_vo['cid'], \$_vo['ename'], \$_jumpflag, \$_vo['jumpurl']);             
            if($titlelen) \$_vo['title'] = str2sub(\$_vo['title'], $titlelen, 0);   
            echo '<a href="'. \$_vo['url'] .'">'. \$_vo['title'] .'</a>';
        } else {
            echo '最后一篇';
        }
    }

?>
str;

        return $str;
    }

    /**/
    public function _position($attr, $content) {
        //非debug参数只处理 一次
        $attr = !empty($attr)?$this->parseXmlAttr($attr, 'position') : null;
        $typeid = !isset($attr['typeid']) || $attr['typeid'] == '' ? -1: trim($attr['typeid']);//只接收一个栏目ID
        $ismobile = empty($attr['ismobile'])? 0 : 1;
        $sname = isset($attr['sname'])? trim($attr['sname']) : '';  
        $surl = isset($attr['surl'])? trim($attr['surl']) : ''; 
        $delimiter = isset($attr['delimiter'])? trim($attr['delimiter']) : '';

        $str =<<<str
<?php
        \$_sname = "$sname";
        \$_typeid =$typeid;
        //debug关闭后,typeid值不变
        //没有下面这步，非debug下，会写死了
        if(\$_typeid == -1) \$_typeid = I('cid', 0, 'intval');

        if (\$_typeid == 0 &&  \$_sname == '') {
            \$_sname = isset(\$title) ? \$title : '';
        }
        echo getPosition(\$_typeid, \$_sname, "$surl", $ismobile, "$delimiter");

?>
str;

        return $str;
    }



        //当前栏目名称
    public function _type($attr, $content) {
        //$attr = $this->parseXmlAttr($attr, 'artlist');
        $typeid = empty($attr['typeid'])? 0 : intval($attr['typeid']);
        if ($typeid == 0) {
            $typeid = $attr['typeid'];
        }
        $str = <<<str
<?php

    \$Category = new \Think\Category();
    \$type = \$Category::getSelf(getCategory(0), $typeid);        
    \$type['url'] = getUrl(\$type);
    

?>
str;
        $str .= $content;
        return $str;

    }

    //导航
    public function _navlist($attr, $content) {
        //$attr = $this->parseXmlAttr($attr, 'navlist');
        //$attr = !empty($attr)? $this->parseXmlAttr($attr, 'navlist') : null;
        $typeid = $attr['typeid'] == '' ? -1 : intval($attr['typeid']);//不能用empty,0,'','0',会认为true
       
        //$currentStyle=$attr['currentstyle'];

        //$cc="";

        $currentCid = I('cid', 0, 'intval');//自动获取当前栏目ID,自动获取当前内容页所属的栏目ID
        //$contentCid=I('contentCid',0, 'intval');//自动获取当前内容页所属的栏目ID

        //print_r($currentCid);
       

        $str = <<<str
<?php
    \$_typeid = $typeid;
    if(\$_typeid == -1) \$_typeid = I('cid', 0, 'intval');
    \$_navlist = getCategory(3);
    \$Category = new \Think\Category();
    if(\$_typeid == 0) {
        \$_navlist  = \$Category::unlimitedForLayer(\$_navlist);      

    }else {
        \$_navlist  = \$Category::unlimitedForLayer(\$_navlist, 'child', \$_typeid);
    }

    foreach(\$_navlist as \$autoindex => \$navlist):
        \$navlist['url'] = getUrl(\$navlist);   
        
?>
str;

    $str .= $content;
    $str .='<?php endforeach;?>';
    return $str;

    }


        //栏目
    public function _catlist($attr, $content) {
        //$attr = $this->parseXmlAttr($attr, 'catlist');
        $typeid = !isset($attr['typeid']) || $attr['typeid'] == '' ? -1 : trim($attr['typeid']);//只接收一个栏目ID
        $type = empty($attr['type'])? 'son' : $attr['type'];//son表示下级栏目,self表示同级栏目,top顶级栏目(top忽略typeid)
        $flag = empty($attr['flag']) ? 0: intval($attr['flag']);//0(不显示链接和单页),1(全部显示),2
        //$orderby = empty($attr['orderby'])? 'id DESC' : $attr['orderby'];
        $limit = empty($attr['limit'])? '10' : $attr['limit'];
        $str = <<<str
<?php
    \$_typeid = intval($typeid);
    \$type = "$type";
    \$_temp = explode(',', "$limit");
    \$_temp[0] = \$_temp[0] > 0? \$_temp[0] : 10;
    if (isset(\$_temp[1]) && intval(\$_temp[1]) > 0) {
        \$_limit[0] = \$_temp[0];
        \$_limit[1] = intval(\$_temp[1]);
    }else {
        \$_limit[0] = 0;
        \$_limit[1] = \$_temp[0];
    }
    if(\$_typeid == -1) \$_typeid = I('cid', 0, 'intval');
    \$__catlist = getCategory(1);

    \$Category = new \Think\Category();
    if ($flag == 0) {
        //where array('status' => 1, 'type' => 0 , 'modelid' => array('neq',4));//4是单页模型
        \$__catlist = \$Category::clearPageAndLink(\$__catlist);
    }
    
    //\$type为top,忽略$typeid
    if(\$_typeid == 0 || \$type == 'top') {
        \$_catlist  = \$Category::unlimitedForLayer(\$__catlist);
    }else {
        //同级分类
        if (\$type == 'self') {
            \$_typeinfo  = \$Category::getSelf(\$__catlist, \$_typeid );
            //if (\$_typeinfo['pid'] != 0) {
                \$_catlist  = \$Category::unlimitedForLayer(\$__catlist, 'child', \$_typeinfo['pid']);
            //}
        }else {
            //son，子类列表
            \$_catlist  = \$Category::unlimitedForLayer(\$__catlist, 'child', \$_typeid);
        }
    }

    foreach(\$_catlist as \$autoindex => \$catlist):
    if(\$autoindex < \$_limit[0]) continue;
    if(\$autoindex >= (\$_limit[1]+\$_limit[0])) break;
    \$catlist['url'] = getUrl(\$catlist);
?>
str;

    $str .= $content;
    $str .='<?php endforeach;?>';
    return $str;

    }

    //文章列表
    public function _list($attr, $content) {
        //$attr = $this->parseXmlAttr($attr,'list');
        $DB_PREFIX=C('DB_PREFIX');
        $attr['flag']=$flag = empty($attr['flag'])? '': $attr['flag'];
        $attr['typeid']=$typeid = !isset($attr['typeid']) || $attr['typeid'] == '' ? -1 : trim($attr['typeid']);//只接收一个栏目ID
        $arcid  = empty($attr['arcid'])? '' : $attr['arcid'];
        $attr['arcid']=$arcid = string2filter($arcid, ',', true);
        $attr['notintype']=$notintype =!isset($attr['notintype']) || $attr['notintype'] == '' ? -1 : trim($attr['notintype']);//-1后面自动获取
        $attr['moreinfo']=$moreinfo = empty($attr['moreinfo'])? '': $attr['moreinfo'];
        $attr['model']=$model = empty($attr['model'])? '': $attr['model'];//模型
        $attr['cache']=$cache=empty($attr['cache'])? 0: $attr['cache'];//默认缓存1秒钟
        $attr['titlelen']=$titlelen = empty($attr['titlelen'])? 0 : intval($attr['titlelen']);
        $attr['infolen']=$infolen = empty($attr['infolen'])? 0 : intval($attr['infolen']);       
        $attr['orderby']=$orderby = empty($attr['orderby'])? 'id DESC' : $attr['orderby'];
        $attr['limit']=$limit = empty($attr['limit'])? '10' : $attr['limit'];
        $attr['pagesize']=$pagesize = empty($attr['pagesize'])? '0' : $attr['pagesize'];
        $attr['keyword']=$keyword = empty($attr['keyword'])? '': trim($attr['keyword']);

        $attr['pageroll']=$pageroll = empty($attr['pageroll'])? '5' : $attr['pageroll'];
        $attr['pagetheme']=$pagetheme = empty($attr['pagetheme'])? ' %upPage% %linkPage% %downPage% 共%totalPage%页' : htmlspecialchars_decode($attr['pagetheme']);
        $cacheID=biuld_object_id(arr_to_html($attr));
        //echo $cacheID."      ";
        $str = <<<str
<?php
    \$_typeid = "$typeid";  
    \$_arcid = "$arcid";  
    \$_keyword = "$keyword";
    \$_flag = "$flag";
    \$_model = "$model";
    \$_notintype = "$notintype";  
    \$_moreinfo = "$moreinfo";
    \$_DB_PREFIX = "$DB_PREFIX";
    if(\$_typeid == -1) \$_typeid = I('get.cid');
    \$_cacheID="$cacheID";
    \$_cache=$cache;
   if (empty(\$_model)) {
       \$Category = new \Think\Category();
       \$_selfcate = \$Category::getSelf(getCategory(), \$_typeid);
       \$_tablename = strtolower(\$_selfcate['tablename']);
   }else{
        \$_tablename=\$_model;
   }
   \$idss=array();
   //notintype为空或者不设置的情况
    if(\$_notintype == -1){
        if (intval(\$_typeid)>0 || !empty(\$_typeid)) {
            \$Category = new \Think\Category();
            \$typeids=explode(',',\$_typeid);
            for (\$i=0; \$i <count(\$typeids) ; \$i++) { 
                \$idss[] = \$Category::getChildsId(getCategory(1), \$typeids[\$i], true);
            }
            \$ids=change2array(\$idss);
            \$where = array(\$_DB_PREFIX.\$_tablename.'.status' => 0, \$_DB_PREFIX.\$_tablename .'.cid'=> array('IN',\$ids));
        }else {
            \$where = array(\$_DB_PREFIX.\$_tablename.'.status' => 0);
        }
    }else{
        if (\$_notintype>0 || !empty(\$_notintype)) {
        \$Category = new \Think\Category();
        \$notintypes=explode(',',\$_notintype);
        for (\$i=0; \$i <count(\$notintypes) ; \$i++) { 
            \$idss[] = \$Category::getChildsId(getCategory(1), \$notintypes[\$i], true);
        }
        \$nids=change2array(\$idss);
        \$where = array(\$_DB_PREFIX.\$_tablename.'.status' => 0, \$_DB_PREFIX.\$_tablename .'.cid'=> array('not in',\$nids));
        }else {
            \$where = array(\$_DB_PREFIX.\$_tablename.'.status' => 0);
        }
    }

    if (\$_keyword != '') {
        \$where[\$_DB_PREFIX.\$_tablename.'.title'] = array('like','%'.\$_keyword.'%');
    }

    if (\$_arcid != '') {
        \$where[\$_DB_PREFIX.\$_tablename.'.id'] = array('IN', \$_arcid);
        //print_r(\$where);
    }
    if(\$_flag != '') {
            \$flags = explode(',', \$_flag);
            for(\$i=0; isset(\$flags[\$i]); \$i++) \$where[] = " FIND_IN_SET('\$flags[\$i]', \$_DB_PREFIX\$_tablename.flag)>0 ";
    }
    
    if (\$_cacheID && S(\$_cacheID)) {
        \$_list=S(\$_cacheID);
    }else{
        if (!empty(\$_tablename) && \$_tablename != 'page') {
                if ($pagesize > 0) {
                    \$count = M(ucfirst(\$_tablename))->where(\$where)->count();
                    \$thisPage = new \Think\Page(\$count, $pagesize);
                    \$ename = I('e', '', 'htmlspecialchars,trim');
                    if (!empty(\$ename) && C('URL_ROUTER_ON') == true) {
                    	\$model_name=strtolower(MODULE_NAME);
						if(\$model_name=='m'){
							\$thisPage->url = \$model_name.'/'.\$ename. '/p';
						}else{
							\$thisPage->url = ''.\$ename. '/p';
						}
                        
                    }
                    //设置显示的页数
                    \$thisPage->rollPage = $pageroll;
                    \$thisPage->setConfig('theme',"$pagetheme");
                    \$limit = \$thisPage->firstRow. ',' .\$thisPage->listRows;  
                    \$page = \$thisPage->show();

                }else {
                    \$limit = "$limit";
                }   

                \$Model = M(ucfirst(\$_tablename));
                //print_r(\$_moreinfo);
                if (empty(\$_moreinfo)) {
                    \$field=\$_DB_PREFIX.\$_tablename.".*,Category.name as catename,Category.ename,Category.modelid";
                    \$joins="__CATEGORY__ as Category ON ".\$_DB_PREFIX.\$_tablename.".cid = Category.id";
                    \$_list=\$Model
                    ->join(\$joins,'LEFT')
                    ->field(\$field)
                    ->where(\$where)
                    ->order("$orderby")
                    ->limit(\$limit)
                    ->select();
                }else{
                    \$field=\$_DB_PREFIX.\$_tablename.".*,Category.name as catename,Category.ename,Category.modelid,".\$_moreinfo;
                    \$joins="__CATEGORY__ as Category ON ".\$_DB_PREFIX.\$_tablename.".cid = Category.id";
                    \$join_add=\$_DB_PREFIX."addon".\$_tablename." ON ".\$_DB_PREFIX."addon".\$_tablename.".aid = ".\$_DB_PREFIX.\$_tablename.".id";
                    \$_list=\$Model
                    ->join(\$joins,'LEFT')
                    ->join(\$join_add,'RIGHT')
                    ->field(\$field)
                    ->where(\$where)
                    ->order("$orderby")
                    ->limit(\$limit)
                    ->select();
                }

                if (empty(\$_list)) {
                    \$_list = array();
                }
        }else {
            \$_list = array();
        }
        if (\$_cache) {
            S(\$_cacheID,\$_list,\$_cache);
        }
        
    }

    foreach(\$_list as \$autoindex => \$list):
    \$_jumpflag = (\$list['flag'] & B_JUMP) == B_JUMP? true : false;
    \$list['url'] = getContentUrl(\$list['id'], \$list['cid'], \$list['ename'], \$_jumpflag, \$list['jumpurl']);
    \$list['fulltitle']=\$list['title'];
    if($titlelen) \$list['title'] = str2sub(\$list['title'], $titlelen, 0); 
    if($infolen) \$list['description'] = str2sub(\$list['description'], $infolen, 0);
?>
str;
    $str .= $content;
    $str .='<?php endforeach;?>';
    return $str;

}


    public function _flink($tag, $content) {
        //$attr = $this->parseXmlAttr($attr, 'flink');     
        $orderby = empty($tag['orderby'])? 'sort ASC' : $tag['orderby'];
        $limit = empty($tag['limit']) ? '10' : $tag['limit'];
        //echo "$typeid---------";

        
        $str = <<<str
<?php
    \$_flink = M('flinks')->order("$orderby")->limit("$limit")->select();
    //p(M('flinks')->_sql());
    if (empty(\$_flink)) {
        \$_flink = array();
    }
    foreach(\$_flink as \$autoindex => \$flink):
?>
str;
    $str .= $content;
    $str .='<?php endforeach;?>';
    return $str;
    }
}


    /**
     * 转换数据为HTML代码
     * @param array $data 数组
     */
    function arr_to_html($data) {
        if (is_array($data)) {
            $str = 'array(';
            foreach ($data as $key => $val) {
                if (is_array($val)) {
                    $str .= "'$key'=>" . arr_to_html($val) . ",";
                } else {
                    //如果是变量的情况
                    if (strpos($val, '$') === 0) {
                        $str .= "'$key'=>$val,";
                    } else if (preg_match("/^([a-zA-Z_].*)\(/i", $val, $matches)) {//判断是否使用函数
                        if (function_exists($matches[1])) {
                            $str .= "'$key'=>$val,";
                        } else {
                            $str .= "'$key'=>'" . newAddslashes($val) . "',";
                        }
                    } else {
                        $str .= "'$key'=>'" . newAddslashes($val) . "',";
                    }
                }
            }
            return $str . ')';
        }
        return false;
    }

    /**
     * 返回经addslashes处理过的字符串或数组
     * @param $string 需要处理的字符串或数组
     * @return mixed
     */
    function newAddslashes($string) {
        if (!is_array($string))
            return addslashes($string);
        foreach ($string as $key => $val)
            $string[$key] = $this->newAddslashes($val);
        return $string;
    }


?>