<?php
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');
if (!is_file('./Data/install.lock')) {
	header('Location: ./install.php');
	exit;
}
define('B_JUMP',32);
define('B_PIC',1);
define('KEY','3J4mGj6l69azWHWRjrIENv7CHztbieoI');
define('APP_DEBUG',true);
define('APP_PATH','./Apps/');
define('RUNTIME_PATH','./Runtime/');
require './Core/WOWOCMS.php';
?>