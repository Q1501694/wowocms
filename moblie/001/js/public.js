﻿var p=1,size=10;
var curPage=1;
var anttitle="", antimage="",antdesc="",antlink=window.location.href,isweixin="0";
var PICS={};
var tid = "";
var aid = "";
var cookie = {
	get: function(name){
		if( name=='' ){
			return '';
		}
		var reg = new RegExp(name+'=([^;]*)');
		var res = document.cookie.match(reg);
		return (res && res[1]) || '';
	},
	set: function(name, value){
		var now = new Date();
			now.setDate(now.getDate() + 1);
		var exp = now.toGMTString();
		document.cookie = name + '=' + value + ';expires=' + exp;
		return true;
	}
};
$(function() {
	//右侧菜单显示、隐藏
	var layer1 = $(".layer1"), layer2 = $(".layer2"), layer3 = $(".layer3");
	$(".lst1").bind("click", function(){
		if(layer1.attr("data-s") != 1){
			layer1.attr("data-s", 1);
			layer2.animate({left:"-80%"});
			layer2.css({"height": $(window).height(), "overflow": "hidden"});
			layer1.show();
			layer3.show();
		}else{
			layer1.attr("data-s", 0);
			layer2.animate({left:"0"}, function(){layer1.hide();});
			layer2.css({"height": "auto", "overflow": "auto"});
			layer3.hide();
		}
	});
	
	layer3.bind("click", function(){
		$(".lst1").click();
	});
	$(".prev").click(function(){
		if(window.document.referrer==""||window.document.referrer==window.location.href)  
		{  
			window.location.href="/";  
		}else  
		{  
			window.location.href=window.document.referrer;  
		}  						 
	});
	var ua = navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == "micromessenger") {
		if (document.addEventListener) {
			document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
		} else if (document.attachEvent) {
			document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
			document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
		}
        document.addEventListener('WeixinJSBridgeReady',function onBridgeReady() {
            WeixinJSBridge.call('hideToolbar')
        })
    } 
	function onBridgeReady() 
	{
		var antlink=window.location.href;
		if (anttitle && antlink) {
			WeixinJSBridge.on('menu:share:appmessage', function (argv) {
				WeixinJSBridge.invoke('sendAppMessage', {
					'img_url': antimage ? antimage : undefined,
					'link': antlink,
					'desc': antdesc ? antdesc : undefined,
					'title': anttitle
				}, function (res) {
				});
			});
			WeixinJSBridge.on('menu:share:timeline', function (argv) {
				WeixinJSBridge.invoke('shareTimeline',{
									  "img_url"    : antimage,
									  "img_width"  : "640",
									  "img_height" : "640",
									  "link"       : antlink,
									  "desc"       : antdesc,
									  "title"      : anttitle
				}, function(res) {
				});
			});
			WeixinJSBridge.on('menu:share:weibo', function(argv){
				WeixinJSBridge.invoke('shareWeibo',{
					  "content" : anttitle+"（"+antdesc+"）",
					  "url"     : antlink
					}, function(res) {
				});
			});
		}
    };
	var imgsSrc = [];
		function reviewImage(src,tk) {
			if (typeof window.WeixinJSBridge != 'undefined') {
				WeixinJSBridge.invoke('imagePreview', {
					'current' : src,
					'urls' : imgsSrc
				});
			}
			else
			{
				tk = tk.replace("img_","");
				popUp( parseInt(tk) );	
			}
		}
		function onImgLoad() {
			var imgs = document.getElementById("img-content");
			imgs = imgs ? imgs.getElementsByTagName("img") : [];
			var nk=0
			for( var i=0,l=imgs.length; i<l; i++ ){//忽略第一张图 是提前加载的loading图而已
				var img = imgs.item(i);
				var src = img.getAttribute('data-src') || img.getAttribute('src');
				var oldimage=img;
				if( src ){
					$("#theList").html($("#theList").html()+'<li><div class="photo" style="background:url(/moblie/public/images/loading.gif) no-repeat center center;" id="img_'+i+'"></div></li>');
					img.setAttribute("name", "img_"+i)
					PICS[i] = {
						ts: (i+1)+"/"+imgs.length,
						url: src,
						loaded: false
					};
					nk= i ;
					imgsSrc.push(src);
					(function(src,oldimage){
						if (img.addEventListener){
							img.addEventListener('click', function(){
								reviewImage(src,oldimage.getAttribute('name'));
							});
						}else if(img.attachEvent){
							img.attachEvent('click', function(){
								reviewImage(src,oldimage.getAttribute('name'));
							});
						}
					})(src,oldimage);
				}
			}
		}
		if( window.addEventListener ){
			window.addEventListener('load', onImgLoad, false);
		}else if(window.attachEvent){
			window.attachEvent('load', onImgLoad);
			window.attachEvent('onload', onImgLoad);
		}
});

(function($) {
  $.fn.lazyload = function(threshold, callback) {

    var $w = $(window),
        th = threshold || 0,
        attrib = "data-src",
        images = this,
        loaded;

    this.one("lazyload", function() {
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-src");
      if (source) {
        this.setAttribute("src", source);
        if (typeof callback === "function") callback.call(this);
      }
    });

    function lazyload() {
      var inview = images.filter(function() {
        var $e = $(this);
        // if ($e.is(":hidden")) return;
        var wt = $w.scrollTop(),
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();
        return eb >= wt - th && et <= wb + th;
      });

      loaded = inview.trigger("lazyload");
      images = images.not(loaded);
    }

    $w.scroll(lazyload);
    $w.resize(lazyload);
    lazyload();
    return this;
  };
})(window.jQuery || window.Zepto);

var wapobj={
	getUrlParam:function(name)
	{
		var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
		var r = window.location.search.substr(1).match(reg);  //匹配目标参数
		if (r!=null) return unescape(r[2]); return ''; //返回参数值
	},
	detailsinit:function(){
		var from="";
        var lid=0;
        var key="newss"+wapobj.getUrlParam("id");
        var val = 0;
        var timeout = null;
         if( window.addEventListener ){
            window.addEventListener('load', function(){
                val = cookie.get(key);
                window.scrollTo(0, val);
            }, false);

            window.addEventListener('unload', function(){
                cookie.set(key,val);
            }, false);

            window.addEventListener('scroll', function(){
                clearTimeout(timeout);
                timeout = setTimeout(function(){
                    val = window.pageYOffset;
                },500);
            }, false);

            document.addEventListener('touchmove', function(){
                clearTimeout(timeout);
                timeout = setTimeout(function(){
                    val = window.pageYOffset;
                },500);
            }, false);
        }
		
	},
	listinit:function(){
		window.onscroll = function() {
            if (document.body.scrollTop > 50) {
               $("#go_top").show();
            } else {
                $("#go_top").hide();
            }
        };
		/*$("#page_controller").click(function() {
			p++;
			$(".pagebar").addClass("loadingbar");
			$(".pagebar").html('<span class="ico"></span><span class="myspan">加载中...</span>');
			list();
		});
		var list=function(){
			var temps=$("#template_user_list").html();
			if(temps=="" || temps== null)
				return;
			if(temps.indexOf("#num#")!=-1)
				size=12; //因为有样式，所以要特殊化
			var s=$("#template_user_list").attr("size");
			if(s!="")
				size=s;
			$.getJSON("/public/ajaxpage.aspx?",{"action":"getweblist","p":p,"c":size,"classid":wapobj.getUrlParam("id")},function(data){ 	
				var str="",str1="",str2="",tempstr="";
				var m=0;
				var total=4;
				if(temps.indexOf("#cnum#")!=-1)
					total=6;
				$.each(data.list,function(idx,item){ 
					tempstr = temps.replace("#num#",m);
					
					tempstr = tempstr.replace("#cnum#",m);
					tempstr = tempstr.replace("#newsid#",item.newsid);
					tempstr = tempstr.replace("#newsurl#",item.newsurl);
					tempstr = tempstr.replace("#filepath#",item.newsfilepath);
					tempstr = tempstr.replace("#background#",item.background);
					tempstr = tempstr.replace("#newstitle#",item.newstitle);
					tempstr = tempstr.replace("#newsmark#",item.newsmark);
					tempstr = tempstr.replace("#newsdate#",item.newsdate);
					m++;
					if(m>=total)
					{
						m=0;
					}
					if ( (m%2==1) )
					{
						tempstr = tempstr.replace("#curr#","");
						str1 +=	tempstr;
					}
					else
					{
						tempstr = tempstr.replace("#curr#","curr");
						str2 +=	tempstr;
					}
					str +=tempstr;
				}); 
				if( $("#tpl-detaillist-left").length>0)
				{
					$("#tpl-detaillist-left").html($("#tpl-detaillist-left").html()+str1);
					$("#tpl-detaillist-right").html($("#tpl-detaillist-right").html()+str2);
					$("#tpl-detaillist-left img").lazyload();
					$("#tpl-detaillist-right img").lazyload();
				}
				else
				{
					$("#pro_list").html($("#pro_list").html()+str);
					$("#pro_list img").lazyload();
				}
				$(".pagebar").removeClass("loadingbar");
				$(".pagebar").html('点击查看更多<span class="ico"></span>');
				if (data.TotalPageCount > data.CurrentPageIndex) {
					$("#page_controller").show()
				} else {
					$("#page_controller").hide()
				}
			});		
		};
		list();*/
	}
}

