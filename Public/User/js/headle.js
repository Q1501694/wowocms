    $(function(){
        //入口图标菜单
        $(".entrance").mouseenter(function(){
            var userMenu = $(this).children(".user-menu");
            userMenu.show();
            clearTimeout(userMenu.data("timeout"));
        }).mouseleave(function(){
            var userMenu = $(this).children(".user-menu");
            userMenu.data("timeout") && clearTimeout(userMenu.data("timeout"));
            userMenu.data("timeout", setTimeout(function(){userMenu.hide()}, 100));
        });
        $(".user-menu").mouseover(function(){
            clearTimeout($(this).data("timeout"));
            $(this).removeData("timeout");
        });

        //++++++++++++++ uploadufy配置
        $('#file_upload').uploadify({
                        'fileSizeLimit': '5000KB',
                        'buttonText': '上传图片建议尺寸：360像素 * 200像素',
                        'buttonClass': 'uploadify',
                        'swf': data_path+'/uploadify/uploadify.swf',
                        'uploader': uploader,
                        'width': '360',
                        'fileTypeDesc': 'Image Files',
                        'fileTypeExts': '*.gif; *.jpg; *.bmp; *.jpeg; *.png',
                        'multi': false,
                        'formData': {
                            'PHPSESSID': '<?php echo session_id();?>'
                        },
                        'onUploadSuccess': function(file, data, response) {
                            var msg = $.parseJSON(data);
                            $('#txtimage').attr("src", msg.path);
                            $("#picurl").val(msg.path);
                            $(".upload_preview").show();

                        }
        });
        //+++++++++++++  修改密码
        $('#editpwd').click(function(){
            if ($("#oldpwd").val() == "") {
                popStatus(4, '请输入旧密码！', 1.5);                
                $("#oldpwd").focus();
                return false;
            }else if ($("#newpwd").val() == "") {
                popStatus(4, '请输入新密码！', 1.5);
                $("#newpwd").focus();
                return false;
            }else if ($("#rnewpwd").val() == "") {
                popStatus(4, '请再次输入新密码！', 1.5);
                $("#rnewpwd").focus();
                return false;
            }else{
                $.ajax({
                    url:eidtpwdHandle,
                    type: 'POST',
                    dataType: 'json',
                    //data:"{\"oldpwd\":\""+$("#oldpwd").val()+"\",\"newpwd\":\""+$("#newpwd").val()+"\",\"rnewpwd\":\""+$("#rnewpwd").val()+"\"}",
                    data: "oldpwd=" + $("#oldpwd").val() + "&newpwd=" + $("#newpwd").val() + "&rnewpwd=" + $("#rnewpwd").val(),
                    success:function(data){
                        
                        if (data.status) {
                            popStatus(1, '提交成功！', 1.5);
                        }else{
                            popStatus(2, data.info, 1.5);
                        }
                    }

                });
            }
            return false;
        });
        //++++++修改头像
        $('#editface').click(function(){

            $.ajax({
                    url:editfaceHandle,
                    type: 'POST',
                    dataType: 'json',
                    data: "face=" + $("#picurl").val(),
                    success:function(data){
                        
                        if (data.status) {
                            popStatus(1, '提交成功！', 1.5);
                        }else{
                            popStatus(2, data.info, 1.5);
                        }
                    }
                });


            return false;
        });

        $('#editdetailHandle').click(function(){
            if ($("#nickname").val() == "") {
                popStatus(4, '请输入昵称！', 1.5);                
                $("#nickname").focus();
                return false;
            }else if ($("#realname").val() == "") {
                popStatus(4, '请输入真实姓名！', 1.5);
                $("#realname").focus();
                return false;
            }else if ($("#birthday").val() == "") {
                popStatus(4, '请选择生日日期！', 1.5);
                $("#birthday").focus();
                return false;
            }else if ($("#qq").val() == "") {
                popStatus(4, '请输入QQ号码！', 1.5);
                $("#qq").focus();
                return false;
            }else if ($("#phone").val() == "") {
                popStatus(4, '请输入电话号码！', 1.5);
                $("#phone").focus();
                return false;
            }else{
                $.ajax({
                    url:editdetail,
                    type: 'POST',
                    dataType: 'json',
                    data: "nickname=" + $("#nickname").val() + "&realname=" + $("#realname").val() + "&birthday=" + $("#birthday").val()+ "&sex=" + $("input[name=sex]:checked").val()+ "&new=" + $("#new").val()+ "&qq=" + $("#qq").val()+ "&phone=" + $("#phone").val(),
                    success:function(data){                        
                        if (data.status) {
                            popStatus(1, data.info, 1.5);
                        }else{
                            popStatus(2, data.info, 1.5);
                        }
                    }

                });
            }
            return false;
        });


        /*$.formValidator.initConfig({formID:"form1",theme:"ArrowSolidBox",submitOnce:true,
            onError:function(msg,obj,errorlist){
                $("#errorlist").empty();
                $.map(errorlist,function(msg){
                    $("#errorlist").append("<li>" + msg + "</li>")
                });
                alert(msg);
            },
            ajaxPrompt : '有数据正在异步验证，请稍等...'
        });

        $("#WIDsubject").formValidator({onShow:"请输入密码",onFocus:"至少1个长度",onCorrect:"密码合法"}).inputValidator({min:1,empty:{leftEmpty:false,rightEmpty:false,emptyError:"密码两边不能有空符号"},onError:"密码不能为空,请确认"});*/










    });
        //返回顶部
        function gotoTop(min_height){
            //预定义返回顶部的html代码，它的css样式默认为不显示
            var gotoTop_html = '<div id="gotoTop">返回顶部</div>';
            //将返回顶部的html代码插入页面上id为page的元素的末尾
            $("body").append(gotoTop_html);
            $("#gotoTop").click(//定义返回顶部点击向上滚动的动画
                function(){ $('html,body').animate({scrollTop:0},700);
            }).hover(//为返回顶部增加鼠标进入的反馈效果，用添加删除css类实现
                function(){ $(this).addClass("hover");},
                function(){ $(this).removeClass("hover");
            });
            //获取页面的最小高度，无传入值则默认为600像素
            min_height ? min_height = min_height : min_height = 600;
            //为窗口的scroll事件绑定处理函数
            $(window).scroll(function(){
                //获取窗口的滚动条的垂直位置
                var s = $(window).scrollTop();
                //当窗口的滚动条的垂直位置大于页面的最小高度时，让返回顶部元素渐现，否则渐隐
                if( s > min_height){
                    $("#gotoTop").fadeIn(100);
                }else{
                    $("#gotoTop").fadeOut(200);
                };
            });
        };
        
        gotoTop();
        
        
        