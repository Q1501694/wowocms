<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'/>
    <title>跳转提示</title>

</head>
<body>
    <!--顶部开始-->
    <div>

        <style type="text/css">
    .tip {
width: 485px;
height: 260px;
position: absolute;
top: 10%;
left: 30%;
background: #FCFDFD;
box-shadow: 1px 8px 10px 1px #9B9B9B;
border-radius: 1px;
display: none;
z-index: 111111;
}
.tiptop {
height: 40px;
line-height: 40px;
background: url(/Public/UI/tcbg.gif) repeat-x;
cursor: pointer;
}
.tiptop span {
font-size: 14px;
font-weight: bold;
color: white;
float: left;
text-indent: 20px;
}
.tiptop a {
display: block;
background: url(/Public/UI/close.png) no-repeat;
width: 22px;
height: 22px;
float: right;
margin-right: 7px;
margin-top: 10px;
cursor: pointer;
}
.tipinfo {
padding-top: 30px;
margin-left: 65px;
height: 95px;
font-family: "微软雅黑"
}
.tipinfo span {
width: 60px;
height: 60px;
float: left;
margin-top: 12px;
}
.tipinfo img{
    width: 60px;
    height: 60px;
    margin-right: -8px;
}
.tipright {
float: left;
padding-top: 15px;
}
.success,.error{font-size: 18px; color: #ff3300;font-weight: bold;}
.error a{color: #ff3300;}
.jump{ padding-top: 10px; font-size:14px;padding-left: 10px;line-height: 14px;}
</style>
        <div class="tip" style="display: block; ">
            <div class="tiptop">
                <span>提示信息</span>
                <a></a>
            </div>

            <div class="tipinfo">
                <present name="message">
                <span><img src="/Public/UI/success.png"></span>
                <else/>
                <span><img src="/Public/UI/error.png"></span>
                </present>

                <div class="tipright">
                    <present name="message">
                        <p class="success"><?php echo($message); ?></p>
                        <else/>
                        <p class="error"><?php echo($error); ?></p>
                    </present>
                    <p class="jump">
                        页面自动
                        <a id="href" href="<?php echo($jumpUrl); ?>">跳转</a>
                        等待时间： <b id="wait"><?php echo($waitSecond); ?></b>
                    </p>
                </div>
            </div>
            
            <script type="text/javascript">
            (function(){
            var wait = document.getElementById('wait'),href = document.getElementById('href').href;
            var interval = setInterval(function(){
                var time = --wait.innerHTML;
                if(time <= 0) {
                    location.href = href;
                    clearInterval(interval);
                };
            }, 1000);
            })();
            </script>
           

        </div>

       
</div>
</body>
</html>