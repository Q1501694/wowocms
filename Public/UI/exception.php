<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'/> 
<title>系统发生错误</title>
<style type="text/css">
body{background:#fff;padding:0px;margin: 0px;font-family: 'Microsoft YaHei'; color: #333;}
input,button{ border:solid 1px #dcdcdc;height:30px; line-height:30px; padding:3px 6px;color:#999;background:#fff; vertical-align: middle;}
select{border:solid 1px #ccc;}
img{border:none;}
a{ text-decoration: none;color:#3361AD;}

.title{height: 40px;
line-height: 40px;
background: url(/Public/UI/tcbg.gif) repeat-x;
cursor: pointer;}

.title span {
font-size: 14px;
font-weight: bold;
color: white;
float: left;
text-indent: 20px;
}

.error{ padding: 5px;}
.error .content{ padding-top: 10px}
.error .info{ margin-bottom: 12px; }
.error .info .text{ line-height: 24px;padding: 10px;}
.copyright{ padding: 12px 48px; color: #999; border-top: 1px solid #ccc; }
.copyright a{ color: #666; text-decoration: none; }

</style>
</head>
<body>      
<div class="error">
<div class="content">
    <div class="info">
        <div class="title">
            <span>错误提示</span>
            
        </div>
        <div class="text">
            <p><?php echo strip_tags($e['message']);?></p>
        </div>
    </div>
<?php if(isset($e['file'])) {?>
    <div class="info">
        <div class="title">
            <span>错误位置</span>
            
        </div>
        <div class="text">
            <p>FILE: <?php echo $e['file'] ;?> &#12288;LINE: <?php echo $e['line'];?></p>
        </div>
    </div>
<?php }?>
<?php if(isset($e['trace'])) {?>
    <div class="info">
        <div class="title">
            <span>TRACE</span>
        </div>
        <div class="text">
            <p><?php echo nl2br($e['trace']);?></p>
        </div>
    </div>
<?php }?>
</div>
</div>

<div class="copyright">
<p><a title="官方网站" href="http://www.0771mc.com">Power By 0771mc.com</a> [ <?php echo date('Y-m-d H:i:s')?> ]</p>
</div>

</body>
</html>