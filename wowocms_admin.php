<?php
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');
if (!is_file('./Data/install.lock')) {
	header('Location: ./install.php');
	exit;
}
define ( 'APP_DEBUG', false );
define ( 'BIND_MODULE','Admin');
define ( 'APP_PATH', './Apps/' );
define ( 'RUNTIME_PATH', './Runtime/' );
require './Core/WOWOCMS.php';